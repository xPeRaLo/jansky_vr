﻿using Immersia;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProceduralPlane : MonoBehaviour
{

	MeshFilter filter;
	public Material mat;
	//Outline outline;
	
	private void Awake()
	{
		
		filter = gameObject.GetComponent<MeshFilter>();	

	}

	private void Start()
	{
		CreatePLane(GetComponent<Polygon>().Arr_coordinates, mat, filter.mesh);
		//CreateOutline();
	}

	public void CreatePLane(Vector2[] points, Material mat, Mesh mesh)
	{
		Vector2[] vertices2D = points;

		// Use the triangulator to get indices for creating triangles
		Triangulator tr = new Triangulator(vertices2D);
		int[] indices = tr.Triangulate();

		//Create the Vector3 vertices
		Vector3[] vertices = new Vector3[vertices2D.Length];


		for (int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = new Vector3(vertices2D[i].x, 0, vertices2D[i].y);
		}


		// Create the mesh
		mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = indices;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		// Set up game object with mesh;
		filter.mesh = mesh;

		//Flip normals
		Vector3[] normals = filter.mesh.normals;
		for (int i = 0; i < normals.Length; i++)
			normals[i] = -normals[i];
		filter.mesh.normals = normals;

		for (int m = 0; m < filter.mesh.subMeshCount; m++)
		{
			int[] triangles = filter.mesh.GetTriangles(m);
			for (int i = 0; i < triangles.Length; i += 3)
			{
				int temp = triangles[i + 0];
				triangles[i + 0] = triangles[i + 1];
				triangles[i + 1] = temp;
			}
			filter.mesh.SetTriangles(triangles, m);
		}

		//Set material
		MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
		renderer.material = mat;
		Color color = renderer.material.color;
		color.a = 0.7f;
		renderer.material.color = color;
		
		
	}
	
	/*void CreateOutline() 
	{
		outline = gameObject.AddComponent<Outline>();
		outline.OutlineMode = Outline.Mode.OutlineAll;
		outline.OutlineColor = Color.black;
		outline.OutlineWidth = 10f;
		
		outline.enabled = true;
	}*/
}

