﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Enums;

public class AnimActions : MonoBehaviour
{

	[SerializeField] Transform GeocomunidadesTransform;
	[SerializeField] Transform filtros;

	public void CallRTVEAPI()
	{
		StartCoroutine(rtveAPI.Instance.GetData(GlobalController.Instance.SelectedTower.codigo_centro, true));
	}

	public void SetColliders3(bool isActive)
	{

		Transform centros = GeocomunidadesTransform.Find(GlobalController.Instance.SelectedComunidad.ToString()).Find("Centros");
		int comunidad = GlobalController.Instance.SelectedComunidad;

		float multiplicador = 1f;

		#region Multiplicador para el cambio de tamaño

		switch (comunidad)
		{
			case 1:
				multiplicador = -1.75f;
				break;
			case 2:
				multiplicador = -2f;
				break;
			case 3:
				multiplicador = 1f;
				break;
			case 4:
				multiplicador = 1f;
				break;
			case 5:
				multiplicador = -1.25f;
				break;
			case 6:
				multiplicador = 1.25f;
				break;
			case 7:
				multiplicador = -2f;
				break;
			case 8:
				multiplicador = -2f;
				break;
			case 9:
				multiplicador = -1.25f;
				break;
			case 10:
				multiplicador = -1.5f;
				break;
			case 11:
				multiplicador = -1.75f;
				break;
			case 12:
				multiplicador = -1.25f;
				break;
			case 13:
				multiplicador = 1f;
				break;
			case 14:
				multiplicador = 1.1f;
				break;
			case 15:
				multiplicador = 1f;
				break;
			case 16:
				multiplicador = 1.5f;
				break;
			case 17:
				multiplicador = 1.5f;
				break;
			default:
				multiplicador = 1f;
				break;
		}

		#endregion

		foreach (Transform centro in centros)
		{
			if (isActive)
			{
				if (multiplicador < 0)
					Aumentar(centro, -multiplicador);
				else if (multiplicador >= 1)
					Reducir(centro, multiplicador);
			}
			else
			{
				if (multiplicador < 0)
					Reducir(centro, -multiplicador);
				else if (multiplicador >= 1)
					Aumentar(centro, multiplicador);
			}

			switch (GlobalController.Instance.platform)
			{
				case Platform.PC:

					if(centro.gameObject.TryGetComponent(out TowerGeometry towerG))
						towerG.setNewScale(centro.localScale);

					if (centro.gameObject.TryGetComponent(out Collider collider))
						collider.enabled = isActive;
					
					centro.Find("Torre").gameObject.SetActive(isActive);

					break;

				case Platform.VR:
					centro.Find("pfNivelCentro").gameObject.SetActive(isActive);
					centro.Find("pfPais").gameObject.SetActive(!isActive);

					break;
			}			
		}
	}

 //   public void HideFiltros()
 //   {
	//	filtros.GetComponent<Animator>().Play("HideFiltro");
 //   }
	//public void ShowFiltros()
 //   {
	//	filtros.GetComponent<Animator>().Play("ShowFiltro");
 //   }

    void Aumentar(Transform centro, float multiplicador)
	{
		centro.localScale *= multiplicador;
	}

	void Reducir(Transform centro, float multiplicador)
	{
		centro.localScale /= multiplicador;
	}
}

