﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map3DAnimActions : MonoBehaviour
{
	Animator mapAnimator;

	private void Awake()
	{
		mapAnimator = GetComponent<Animator>();
	}

	public void SetAnimatorState(int state)
	{
		switch (state)
		{
			case 0:
				mapAnimator.enabled = false;
				break;
			case 1:
				mapAnimator.enabled = true;
				break;
			default:
				break;
		}
	}

	public void Reducir(float reduccion = 2f)
	{

		Transform centros = transform.Find("Spain3DSimplified").Find("GeoComunidades").Find(GlobalController.Instance.SelectedComunidad.ToString()).Find("Centros");

		foreach(Transform centro in centros)
		{
			centro.localScale /= reduccion;
		}
	}

	public void Ampliar(float multiplicador = 2f)
	{
		Transform centros = transform.Find("Spain3DSimplified").Find("GeoComunidades").Find(GlobalController.Instance.SelectedComunidad.ToString()).Find("Centros");

		foreach (Transform centro in centros)
		{
			centro.localScale *= multiplicador;
		}
	}
}
