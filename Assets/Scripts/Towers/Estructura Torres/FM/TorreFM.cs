﻿using UnityEngine;

public abstract class TorreFM : TorreBase
{
	[Header("Datos del receptor")]

	public int receptor_estado;
	public string receptor_rf;
	public string receptor_cn;
	public string receptor_locked;
	public string receptor_bitrate;
	public string receptor_temperatura;
	public string receptor_portadora;

	#region Datos del receptor : Audio 1

	[Header("Datos del receptor : Audio 1")]

	public string audio1_PID;
	public string audio1_estado;
	public string audio1_alarma;
	public string audio1_estado_RDS;

	#endregion

	#region Datos del receptor : Audio 2

	[Header("Datos del receptor : Audio 2")]

	public string audio2_PID;
	public string audio2_estado;
	public string audio2_alarma;
	public string audio2_estado_RDS;

	#endregion

	#region Datos del receptor : Audio 3 

	[Header("Datos del receptor : Audio 3")]

	public string audio3_PID;
	public string audio3_estado;
	public string audio3_alarma;
	public string audio3_estado_RDS;

	#endregion

	#region Datos del receptor : Audio 4

	[Header("Datos del receptor : Audio 4")]

	public string audio4_PID;
	public string audio4_estado;
	public string audio4_alarma;
	public string audio4_estado_RDS;

	#endregion
}

