﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM3 : TorreFM
{

	#region Datos del transmisor de reserva

	[Header("Datos del transmisor reserva")]

	public int txr_estado;
	public string txr_nombre;
	public string txr_frecuencia;
	public string txr_potenciadirecta;
	public string txr_potenciareflejada;
	public string txr_modulacion;
	public string txr_alarmageneral;
	public string txr_alarmapotencia;
	public string txr_temperaturatx;
	public string txr_temperaturaambiente;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_nombre;
	public string tx1_frecuencia;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_modulacion;
	public string tx1_alarmageneral;
	public string tx1_alarmapotencia;
	public string tx1_temperaturatx;
	public string tx1_temperaturaambiente;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_nombre;
	public string tx2_frecuencia;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_modulacion;
	public string tx2_alarmageneral;
	public string tx2_alarmapotencia;
	public string tx2_temperaturatx;
	public string tx2_temperaturaambiente;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_nombre;
	public string tx3_frecuencia;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_modulacion;
	public string tx3_alarmageneral;
	public string tx3_alarmapotencia;
	public string tx3_temperaturatx;
	public string tx3_temperaturaambiente;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_nombre;
	public string tx4_frecuencia;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_modulacion;
	public string tx4_alarmageneral;
	public string tx4_alarmapotencia;
	public string tx4_temperaturatx;
	public string tx4_temperaturaambiente;

	#endregion

}
