﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM5 : TorreFM
{

	#region Datos del transmisor reserva

	[Header("Datos del transmisor reserva")]

	public int txr_estado;
	public string txr_potenciadirecta;
	public string txr_potenciareflejada;
	public string txr_transmisorencendido;
	public string txr_selecexcitador;
	public string txr_frecuenciaex1;
	public string txr_potenciadirectaexc1;
	public string txr_potenciareflejadaexc1;
	public string txr_frecuenciaex2;
	public string txr_potenciadirectaexc2;
	public string txr_potenciareflejadaexc2;
	public string txr_temperatura;
	public string txr_estadotx;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_transmisorencendido;
	public string tx1_selecexcitador;
	public string tx1_frecuenciaex1;
	public string tx1_potenciadirectaexc1;
	public string tx1_potenciareflejadaexc1;
	public string tx1_frecuenciaex2;
	public string tx1_potenciadirectaexc2;
	public string tx1_potenciareflejadaexc2;
	public string tx1_temperatura;
	public string tx1_estadotx;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_transmisorencendido;
	public string tx2_selecexcitador;
	public string tx2_frecuenciaex1;
	public string tx2_potenciadirectaexc1;
	public string tx2_potenciareflejadaexc1;
	public string tx2_frecuenciaex2;
	public string tx2_potenciadirectaexc2;
	public string tx2_potenciareflejadaexc2;
	public string tx2_temperatura;
	public string tx2_estadotx;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_transmisorencendido;
	public string tx3_selecexcitador;
	public string tx3_frecuenciaex1;
	public string tx3_potenciadirectaexc1;
	public string tx3_potenciareflejadaexc1;
	public string tx3_frecuenciaex2;
	public string tx3_potenciadirectaexc2;
	public string tx3_potenciareflejadaexc2;
	public string tx3_temperatura;
	public string tx3_estadotx;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_transmisorencendido;
	public string tx4_selecexcitador;
	public string tx4_frecuenciaex1;
	public string tx4_potenciadirectaexc1;
	public string tx4_potenciareflejadaexc1;
	public string tx4_frecuenciaex2;
	public string tx4_potenciadirectaexc2;
	public string tx4_potenciareflejadaexc2;
	public string tx4_temperatura;
	public string tx4_estadotx;

	#endregion

	#region Datos de la UCA

	[Header("Datos dela UCA")]

	public int uca_estado;
	public string uca_estadopotenciatx1;
	public string uca_tipopotenciatx1;
	public string uca_estadotx1;
	public string uca_estadopotenciatx2;
	public string uca_tipopotenciatx2;
	public string uca_estadotx2;
	public string uca_estadopotenciatx3;
	public string uca_tipopotenciatx3;
	public string uca_estadotx3;
	public string uca_estadopotenciatx4;
	public string uca_tipopotenciatx4;
	public string uca_estadotx4;
	public string uca_estadopotenciatxr;
	public string uca_tipopotenciatxr;
	public string uca_estadotxr;
	public string uca_estadosistema;
	public string uca_estadocarga;
	public string uca_estadouca;

	#endregion
}
