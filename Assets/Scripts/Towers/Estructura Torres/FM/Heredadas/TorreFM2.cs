﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM2 : TorreFM
{

	#region Datos de transmisor reserva

	[Header("Datos del transmisor reserva")]

	public int txr_estado;
	public string txr_nombre;
	public string txr_tipo;
	public string txr_frecuencia;
	public string txr_potencia;
	public string txr_potenciadirecta;
	public string txr_potenciareflejada;
	public string txr_entrada;
	public string txr_salida;
	public string txr_modulacion;
	public string txr_modulacionR;
	public string txr_modulacionL;
	public string txr_alarmageneral;
	public string txr_alarmapotencia;
	public string txr_alarmaaudio;
	public string txr_remoto;
	public string txr_temperaturatx;
	public string txr_temperaturaambiente;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_nombre;
	public string tx1_tipo;
	public string tx1_frecuencia;
	public string tx1_potencia;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_entrada;
	public string tx1_salida;
	public string tx1_modulacion;
	public string tx1_modulacionR;
	public string tx1_modulacionL;
	public string tx1_alarmageneral;
	public string tx1_alarmapotencia;
	public string tx1_alarmaaudio;
	public string tx1_remoto;
	public string tx1_temperaturatx;
	public string tx1_temperaturaambiente;


	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_nombre;
	public string tx2_tipo;
	public string tx2_frecuencia;
	public string tx2_potencia;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_entrada;
	public string tx2_salida;
	public string tx2_modulacion;
	public string tx2_modulacionR;
	public string tx2_modulacionL;
	public string tx2_alarmageneral;
	public string tx2_alarmapotencia;
	public string tx2_alarmaaudio;
	public string tx2_remoto;
	public string tx2_temperaturatx;
	public string tx2_temperaturaambiente;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_nombre;
	public string tx3_tipo;
	public string tx3_frecuencia;
	public string tx3_potencia;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_entrada;
	public string tx3_salida;
	public string tx3_modulacion;
	public string tx3_modulacionR;
	public string tx3_modulacionL;
	public string tx3_alarmageneral;
	public string tx3_alarmapotencia;
	public string tx3_alarmaaudio;
	public string tx3_remoto;
	public string tx3_temperaturatx;
	public string tx3_temperaturaambiente;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_nombre;
	public string tx4_tipo;
	public string tx4_frecuencia;
	public string tx4_potencia;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_entrada;
	public string tx4_salida;
	public string tx4_modulacion;
	public string tx4_modulacionR;
	public string tx4_modulacionL;
	public string tx4_alarmageneral;
	public string tx4_alarmapotencia;
	public string tx4_alarmaaudio;
	public string tx4_remoto;
	public string tx4_temperaturatx;
	public string tx4_temperaturaambiente;

	#endregion

	#region Datos de la UCA Modo 20

	[Header("Datos de la UCA Modo 20")]

	public int uca_estado;
	public int uca_numero_tx;
	public string uca_modofuncionamiento;
	public string uca_modoremoto;
	public string uca_estado_txr;
	public string uca_estado_tx1;
	public string uca_estado_tx2;
	public string uca_estado_tx3;
	public string uca_estado_tx4;
	public string uca_fallo_txr;
	public string uca_fallo_tx1;
	public string uca_fallo_tx2;
	public string uca_fallo_tx3;
	public string uca_fallo_tx4;
	public string uca_radiante;
	public string uca_comunicaciones;
	public string uca_ventilacion;


	#endregion

}
