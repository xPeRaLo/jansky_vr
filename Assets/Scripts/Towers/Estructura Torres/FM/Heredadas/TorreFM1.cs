﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM1 : TorreFM
{

	#region Datos de transmisor reserva

	[Header("Datos del transmisor reserva")]

	public int txr_estado;
	public string txr_frecuencia;
	public string txr_potenciadirecta;
	public string txr_potenciareflejada;
	public string txr_temperatura;
	public string txr_alarmarf;
	public string txr_alarmaaudio;
	public string txr_reservaon;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_frecuencia;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_temperatura;
	public string tx1_alarmarf;
	public string tx1_alarmaaudio;
	public string tx1_encendido;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_frecuencia;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_temperatura;
	public string tx2_alarmarf;
	public string tx2_alarmaaudio;
	public string tx2_encendido;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_frecuencia;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_temperatura;
	public string tx3_alarmarf;
	public string tx3_alarmaaudio;
	public string tx3_encendido;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_frecuencia;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_temperatura;
	public string tx4_alarmarf;
	public string tx4_alarmaaudio;
	public string tx4_encendido;

	#endregion

	#region Datos de la UCA Modo 20

	[Header("Datos de la UCA Modo 20")]

	public int uca_estado;
	public string uca_timeout;
	public string uca_alarmageneral;
	public string uca_alarmarf;
	public string uca_fallored;
	public string uca_txreservaon;
	public string uca_audio_tx1;
	public string uca_audio_tx2;
	public string uca_audio_tx3;
	public string uca_audio_tx4;
	public string uca_audio_txr;
	public string uca_fallo_tx1;
	public string uca_fallo_tx2;
	public string uca_fallo_tx3;
	public string uca_fallo_tx4;
	public string uca_fallo_txr;
	public string uca_modo;

	#endregion

}
