﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM4 : TorreFM
{
	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_nombre;
	public string tx1_frecuencia;
	public string tx1_potencia;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_modulacion;
	public string tx1_estadotransmisor;
	public string tx1_alarmatransmisor;
	public string tx1_atenuacion;
	public string tx1_alarmaatenuacion;
	public string tx1_estadoamplificador;
	public string tx1_temperaturatx;
	public string tx1_modofuncionamiento;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_nombre;
	public string tx2_frecuencia;
	public string tx2_potencia;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_modulacion;
	public string tx2_estadotransmisor;
	public string tx2_alarmatransmisor;
	public string tx2_atenuacion;
	public string tx2_alarmaatenuacion;
	public string tx2_estadoamplificador;
	public string tx2_temperaturatx;
	public string tx2_modofuncionamiento;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_nombre;
	public string tx3_frecuencia;
	public string tx3_potencia;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_modulacion;
	public string tx3_estadotransmisor;
	public string tx3_alarmatransmisor;
	public string tx3_atenuacion;
	public string tx3_alarmaatenuacion;
	public string tx3_estadoamplificador;
	public string tx3_temperaturatx;
	public string tx3_modofuncionamiento;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_nombre;
	public string tx4_frecuencia;
	public string tx4_potencia;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_modulacion;
	public string tx4_estadotransmisor;
	public string tx4_alarmatransmisor;
	public string tx4_atenuacion;
	public string tx4_alarmaatenuacion;
	public string tx4_estadoamplificador;
	public string tx4_temperaturatx;
	public string tx4_modofuncionamiento;

	#endregion
}
