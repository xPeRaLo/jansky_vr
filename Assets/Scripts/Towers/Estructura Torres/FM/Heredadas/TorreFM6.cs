﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreFM6 : TorreFM
{

	#region Datos del transmisor Reserva

	[Header("Datos del transmisor reserva")]

	public int txr_estado;
	public string txr_power;
	public string txr_frecuencia;
	public string txr_potenciadirecta;
	public string txr_potenciareflejada;
	public string txr_desviacion;
	public string txr_canalizquierdo;
	public string txr_canalderecho;
	public string txr_entradaaudio;
	public string txr_muteaudio;
	public string txr_estadopll;
	public string txr_foldback;
	public string txr_tensionvpa;
	public string txr_corrienteipa;
	public string txr_eficiencia;
	public string txr_temperatura;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_power;
	public string tx1_frecuencia;
	public string tx1_potenciadirecta;
	public string tx1_potenciareflejada;
	public string tx1_desviacion;
	public string tx1_canalizquierdo;
	public string tx1_canalderecho;
	public string tx1_entradaaudio;
	public string tx1_muteaudio;
	public string tx1_estadopll;
	public string tx1_foldback;
	public string tx1_tensionvpa;
	public string tx1_corrienteipa;
	public string tx1_eficiencia;
	public string tx1_temperatura;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_power;
	public string tx2_frecuencia;
	public string tx2_potenciadirecta;
	public string tx2_potenciareflejada;
	public string tx2_desviacion;
	public string tx2_canalizquierdo;
	public string tx2_canalderecho;
	public string tx2_entradaaudio;
	public string tx2_muteaudio;
	public string tx2_estadopll;
	public string tx2_foldback;
	public string tx2_tensionvpa;
	public string tx2_corrienteipa;
	public string tx2_eficiencia;
	public string tx2_temperatura;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_power;
	public string tx3_frecuencia;
	public string tx3_potenciadirecta;
	public string tx3_potenciareflejada;
	public string tx3_desviacion;
	public string tx3_canalizquierdo;
	public string tx3_canalderecho;
	public string tx3_entradaaudio;
	public string tx3_muteaudio;
	public string tx3_estadopll;
	public string tx3_foldback;
	public string tx3_tensionvpa;
	public string tx3_corrienteipa;
	public string tx3_eficiencia;
	public string tx3_temperatura;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_power;
	public string tx4_frecuencia;
	public string tx4_potenciadirecta;
	public string tx4_potenciareflejada;
	public string tx4_desviacion;
	public string tx4_canalizquierdo;
	public string tx4_canalderecho;
	public string tx4_entradaaudio;
	public string tx4_muteaudio;
	public string tx4_estadopll;
	public string tx4_foldback;
	public string tx4_tensionvpa;
	public string tx4_corrienteipa;
	public string tx4_eficiencia;
	public string tx4_temperatura;

	#endregion

	#region Datos UCA

	[Header("Datos UCA")]

	public int uca_estado;
	public string uca_estadopotenciatx1;
	public string uca_tipopotenciatx1;
	public string uca_estadotx1;
	public string uca_estadopotenciatx2;
	public string uca_tipopotenciatx2;
	public string uca_estadotx2;
	public string uca_estadopotenciatx3;
	public string uca_tipopotenciatx3;
	public string uca_estadotx3;
	public string uca_estadopotenciatx4;
	public string uca_tipopotenciatx4;
	public string uca_estadotx4;
	public string uca_estadopotenciatxr;
	public string uca_tipopotenciatxr;
	public string uca_estadotxr;
	public string uca_estadosistema;
	public string uca_estadocarga;
	public string uca_estadouca;


	#endregion

}
