﻿using Microsoft.Geospatial;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorreOM : TorreBase
{
	#region Datos OM

	[Header("Datos característicos de OM")]

	public string centro_nombre;
	public string redcompania;
	public string redcentro;
	public string fuego;
	public string intrusion;
	public string grupoespera;
	public string grupo;
	public string temperatura;

	#endregion

	#region Datos del transmisor 1

	[Header("Datos del transmisor 1")]

	public int tx1_estado;
	public string tx1_valor1;
	public string tx1_valor2;
	public string tx1_valor3;

	#endregion

	#region Datos del transmisor 2

	[Header("Datos del transmisor 2")]

	public int tx2_estado;
	public string tx2_valor1;
	public string tx2_valor2;
	public string tx2_valor3;

	#endregion

	#region Datos del transmisor 3

	[Header("Datos del transmisor 3")]

	public int tx3_estado;
	public string tx3_valor1;
	public string tx3_valor2;
	public string tx3_valor3;

	#endregion

	#region Datos del transmisor 4

	[Header("Datos del transmisor 4")]

	public int tx4_estado;
	public string tx4_valor1;
	public string tx4_valor2;
	public string tx4_valor3;

	#endregion
}