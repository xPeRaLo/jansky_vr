﻿using Microsoft.Geospatial;
using System;
using UnityEngine;

[System.Serializable]
public class TorreBase : MonoBehaviour
{

	#region Datos generales

	[Header("Datos generales")]

	public string nombre;
	public string bbdd;
	public LatLon latLon;
	public string propietario;
	public string frecuencia;
	public string emision;
	public string tipo;
	public int cota;
	public int altura;

	public string provincia;
	public string comunidad;

	public int estado = 4;
	public int estado_transmisor1;
	public int estado_transmisor2;
	public int estado_transmisor3;
	public int estado_transmisor4;

	public int id_registro;
	public int codigo_centro;
	public DateTime dia;
	public DateTime hora;

	public AudioFile estacion = null;


	#endregion

	#region Datos del Modem

	[Header("Datos del Modem")]

	public int modem_estado;
	public string modem_nombre;
	public string modem_rssi;
	public string modem_datos_in;
	public string modem_datos_out;
	public float modem_consumo_in;
	public float modem_consumo_out;

	#endregion

	#region Datos de errores

	[Header("Datos de errores")]

	public string texto_alarma;

	#endregion

}