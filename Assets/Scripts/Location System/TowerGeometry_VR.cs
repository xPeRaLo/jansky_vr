﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerGeometry_VR : MonoBehaviour
{

	Vector3 normalScale;
	Vector3 HoverScale;

	int hoverSortingIndex;
	int originalSortingIndex;

	AudioSource clickSFX;
	AudioSource hoverSFX;

	[SerializeField] Animator globoHover;
	[SerializeField] Transform chincheta;

	private void Awake()
	{
		clickSFX = GameObject.Find("Sound_click_Btn").GetComponent<AudioSource>();
		hoverSFX = GameObject.Find("Sound_hover_Btn").GetComponent<AudioSource>();

	}

	private void Start()
	{
		normalScale = transform.localScale;
		HoverScale = normalScale * 1.4f;

		originalSortingIndex = transform.parent.GetSiblingIndex();
		hoverSortingIndex = transform.parent.parent.childCount - 1;

		FillGlobo();
	}

	private void FillGlobo()
	{

		//yield return new WaitUntil(() => transform.parent.GetComponent<TorreBase>() != null);
		
		UIReferences.Instance.UpdatePanelHover_VR(transform.parent.GetComponent<TorreBase>(),chincheta);
		//StartCoroutine(rtveAPI.Instance.GetData(codigoCentro, false));
	}

	public void setNewScale(Vector3 scale)
	{
		normalScale = scale;
		HoverScale = normalScale * 1.4f;
	}

	public void PointerEnter()
	{
		hoverSFX.Play();
		transform.localScale = HoverScale;
		AlwaysOnTop();
		globoHover.Play("open");
	}

	public void PointerClick()
	{
		rtveAPI.Instance.StopAllCoroutines();
		clickSFX.Play();
		globoHover.Play("idle_chincheta");
		GlobalController.Instance.SelectedTower = transform.parent.GetComponent<TorreBase>();

		TorreBase torre = transform.parent.GetComponent<TorreBase>();

		if (torre.estacion != null && GlobalController.Instance.platform == Utils.Enums.Platform.PC)
		{

			#region Eventos boton PLAY

			UIReferences.Instance.btnPlay.onClick.RemoveAllListeners();

			UIReferences.Instance.btnPlay.onClick.AddListener(() =>
			{
				GlobalController.Instance.PlayRadio(torre.estacion);

				UIReferences.Instance.btnPlay.gameObject.SetActive(false);
				UIReferences.Instance.loadingIcon.SetActive(true);
			});

			#endregion

			#region Eventos boton Stop

			UIReferences.Instance.btnStop.onClick.RemoveAllListeners();

			UIReferences.Instance.btnStop.onClick.AddListener(() =>
			{
				GlobalController.Instance.StopRadio();

				UIReferences.Instance.btnPlay.gameObject.SetActive(true);
				UIReferences.Instance.btnStop.gameObject.SetActive(false);
			});

			#endregion

			UIReferences.Instance.btnPlay.gameObject.SetActive(true);
			UIReferences.Instance.btnStop.gameObject.SetActive(false);
		}
	}

	public void PointerExit()
	{
		transform.localScale = normalScale;
		Normal();
		globoHover.Play("close");
	}

	void AlwaysOnTop()
	{
		transform.parent.SetSiblingIndex(hoverSortingIndex);
	}

	void Normal()
	{
		transform.parent.SetSiblingIndex(originalSortingIndex);
	}

}
