﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Comunidad 
{
	public int numeroComunidad;
	public string nombreComunidad;
	public List<Provincia> provincias;


	public Comunidad(string nombreComunidad, int numeroComunidad)
	{
		this.nombreComunidad = nombreComunidad;
		this.numeroComunidad = numeroComunidad;
	}

	public Comunidad() { }

	public void AddProvincia(string nombreProvincia, int numeroProvincia)
	{
		if (provincias == null)
			provincias = new List<Provincia>();

		Provincia Provincia = new Provincia(nombreProvincia, numeroProvincia);
		provincias.Add(Provincia);
		}
	}

