﻿using Microsoft.Geospatial;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Provincia
{
	public int numeroProvincia;
	public string nombreProvincia; 
	public List<Centro> centros; // Cada provincia tiene muchas ciudades



	public Provincia(string nombreProvincia, int numeroProvincia) // Constructor
	{
		this.nombreProvincia = nombreProvincia;
		this.numeroProvincia = numeroProvincia;
	}

	public Provincia() { }

	public void AddCentro(TorreBase t)
	{
		if (centros == null) // Si no esta inicializada
			centros = new List<Centro>(); // Inicializamos

		Centro centro = new Centro(t); // Nueva ciudad con los parametros
		centros.Add(centro); // La añadimos a la lista
	}

}


