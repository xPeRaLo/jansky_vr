﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerGeometry : MonoBehaviour
{

	Button btnVolver;

	Animator mapAnimator;
	Animator uiAnimator;

	Vector3 normalScale;
	Vector3 HoverScale;

	int hoverSortingIndex;
	int originalSortingIndex;

	GameObject dataTimer;

	private void Awake()
	{
		mapAnimator = GameObject.Find("3DMap").GetComponent<Animator>();
		uiAnimator = GameObject.Find("Niveles").GetComponent<Animator>();
		//btnVolver = GameObject.Find("Canvas").transform.Find("Cabecera").Find("bloq2").Find("BTN_ATRAS_CENTRO").GetComponent<Button>();
		btnVolver = UIReferences.Instance.botonCentro.GetComponent<Button>();
	}

	private void Start()
	{
		originalSortingIndex = transform.GetSiblingIndex();
		hoverSortingIndex = transform.parent.childCount - 1;
	}

	public void setNewScale(Vector3 scale)
	{
		normalScale = scale;
		HoverScale = normalScale * 1.4f;
	}

	private void OnMouseDown()
	{
		transform.localScale = normalScale;

		TorreBase torre = transform.GetComponent<TorreBase>();

		#region Eventos boton Volver 

		btnVolver.onClick.RemoveAllListeners();

		btnVolver.onClick.AddListener(() =>
		{
			uiAnimator.Play("Close_Centro");

			rtveAPI.Instance.StopAllCoroutines();

			rtveAPI.Instance.mapaBing.SetActive(false);
			rtveAPI.Instance.mapVis.gameObject.SetActive(true);
			rtveAPI.Instance.nivelCentroSynced = false;
			rtveAPI.Instance.datosSynced = false;
			rtveAPI.Instance.isRequesting = false;
			rtveAPI.Instance.timerGO.GetComponent<DataTimer>().isActive = false;
			rtveAPI.Instance.timerGO.SetActive(false);

			StartCoroutine(rtveAPI.Instance.GetEstadoCentros());

			UIReferences.Instance.textoCentroTitulo.text = GlobalController.Instance.SelectedTower.comunidad.ToString();

			if (UIReferences.Instance.botonComunidad != null)
				UIReferences.Instance.botonComunidad.SetActive(true);

			UIReferences.Instance.botonCentro.SetActive(false);
			UIReferences.Instance.btnPlay.gameObject.SetActive(false);
			UIReferences.Instance.btnStop.gameObject.SetActive(false);
			UIReferences.Instance.loadingIcon.SetActive(false);

			GlobalController.Instance.SelectedTower = null;
			GlobalController.Instance.StopRadio();
	
			LeanTween.cancelAll();

		
		});

		#endregion

		GlobalController.Instance.SelectedTower = torre;

		if (torre.estacion != null)
		{

			#region Eventos boton PLAY

			UIReferences.Instance.btnPlay.onClick.RemoveAllListeners();

			UIReferences.Instance.btnPlay.onClick.AddListener(() =>
			{
				GlobalController.Instance.PlayRadio(torre.estacion);

				UIReferences.Instance.btnPlay.gameObject.SetActive(false);
				UIReferences.Instance.loadingIcon.SetActive(true);
			});

			#endregion

			#region Eventos boton Stop

			UIReferences.Instance.btnStop.onClick.RemoveAllListeners();

			UIReferences.Instance.btnStop.onClick.AddListener(() =>
			{
				GlobalController.Instance.StopRadio();

				UIReferences.Instance.btnPlay.gameObject.SetActive(true);
				UIReferences.Instance.btnStop.gameObject.SetActive(false);
			});

			#endregion

			UIReferences.Instance.btnPlay.gameObject.SetActive(true);
			UIReferences.Instance.btnStop.gameObject.SetActive(false);
		}

		if (UIReferences.Instance.botonComunidad != null)
			UIReferences.Instance.botonComunidad.SetActive(false);

		UIReferences.Instance.botonCentro.SetActive(true);
	}

	private void OnMouseEnter()
	{
		TorreBase torre = transform.GetComponent<TorreBase>();

		AlwaysOnTop();

		transform.localScale = HoverScale;

		if (torre.propietario == "RNE" && torre != null)
        {
			//UIReferences.Instance.infoCentro.SetActive(true);
			StartCoroutine(UIReferences.Instance.UpdateInfoCentro(torre.codigo_centro));
		}

	}

	private void OnMouseExit()
	{
		Normal();

		transform.localScale = normalScale;
		StartCoroutine(UIReferences.Instance.HideInfoCentro());
	}

	void AlwaysOnTop()
	{
		transform.SetSiblingIndex(hoverSortingIndex);
	}

	void Normal()
	{
		transform.SetSiblingIndex(originalSortingIndex);
	}
}
