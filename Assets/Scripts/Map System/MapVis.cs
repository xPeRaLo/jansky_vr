﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.IO;
using UnityEngine.UI;
using SimpleJSON;
using System.Linq;
using System;
using UnityEngine.Events;
using Microsoft.Geospatial;
using Microsoft.Maps.Unity;
using Utils.Enums;
using Utils.FileSystem;
using UnityEngine.EventSystems;
using TMPro;

public class MapVis : MonoBehaviour
{
    #region SINGLETON

    private static MapVis instance;

    public static MapVis Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<MapVis>();

                if (instance == null)
                {
                    GameObject container = new GameObject("APIReaders");
                    instance = container.AddComponent<MapVis>();
                }
            }

            return instance;
        }
    }

    #endregion

    [SerializeField] GameObject marker;


    [SerializeField] Color colorInicio = new Color32(31, 146, 166, 255);
    [SerializeField] Color colorFin = new Color32(74, 43, 196, 255);

    [Space]

    [SerializeField] GameObject Geo1;
    [SerializeField] GameObject Geo2;
    [SerializeField] GameObject Geo3;
    [SerializeField] GameObject Geo4;
    [SerializeField] GameObject Geo5;
    [SerializeField] GameObject Geo6;
    [SerializeField] GameObject Geo7;
    [SerializeField] GameObject Geo8;
    [SerializeField] GameObject Geo9;
    [SerializeField] GameObject Geo10;
    [SerializeField] GameObject Geo11;
    [SerializeField] GameObject Geo12;
    [SerializeField] GameObject Geo13;
    [SerializeField] GameObject Geo14;
    [SerializeField] GameObject Geo15;
    [SerializeField] GameObject Geo16;
    [SerializeField] GameObject Geo17;

    List<GameObject> GeoList;

    Color color;

    Color colorAlarma = new Color32(212, 20, 90, 255);
    Color colorFault = new Color32(247, 147, 30, 255);
    Color colorWarning = new Color32(217, 206, 38, 255);
    Color colorOk = new Color32(140, 198, 63, 255);
    Color colorSinConexion = new Color32(0, 161, 178, 255);
    Color colorSinDatos = new Color32(150, 150, 150, 255);

    Vector3 geoCenter = new Vector3(-3.713f, 0, 40.208f);
    Button buttonMarker;

	[SerializeField] Animator animator;

    bool filtroRNE = true;
    bool filtroCellnex = true;
    bool filtroOM = true;
    bool filtroFM = true;
    bool filtroOk = true;
    bool filtroWarning = true;
    bool filtroFault = true;
    bool filtroAlarma = true;
    bool filtroSinConexion = true;

    List<bool> filtros;

    List<string> filtrosCondicional = new List<string>() { "4", "3", "2", "1", "0", "FM", "OM", "CELLNEX", "RNE" };

    public bool isChoropleth = false;

    void Start()
    {
        GeoList = new List<GameObject>() { Geo1, Geo2, Geo3, Geo4, Geo5, Geo6, Geo7, Geo8, Geo9, Geo10, Geo11, Geo12, Geo13, Geo14, Geo15, Geo16, Geo17 };

		// Se establece qué comunidad se ve en función al nivel de privilegios del usuario que se logea
		//SetPrivilegios();

		if(rtveAPI.Instance.mapVis == null)
		{
			rtveAPI.Instance.mapVis = this;
		}

		if(rtveAPI.Instance.mapaBing == null)
		{
			rtveAPI.Instance.mapaBing = GameObject.Find("3DMap").transform.Find("BingMap").gameObject;
		}
		
		if(rtveAPI.Instance.timerGO == null && GlobalController.Instance.platform == Platform.PC)
		{
			rtveAPI.Instance.timerGO = GameObject.Find("Canvas").transform.Find("Cabecera").Find("bloq3").Find("Timer").gameObject;
		}

		StartCoroutine(rtveAPI.Instance.GetEstadoCentros());

        StartCoroutine(SpawnMarkers());

    }

	private List<int> ToBinary(List<bool> list)
    {
        int value = 0;
        for (int i = 0; i < list.Count; i++)
        {
            //Debug.Log(list[i]);
            if (list[i])
                value += (int)Mathf.Pow(2,i);
        }

        string binary = Convert.ToString(value, 2);

        int cerosFaltan = list.Count - binary.Length;
        for (int i = 0; i < cerosFaltan; i++)
        {
            binary = "0" + binary;
        }

        List<int> binaryArray = new List<int>();

        foreach (char c in binary)
        {
            binaryArray.Add(int.Parse(c.ToString()));
        }
        return binaryArray;
    }

    public void UpdateFiltros(string btn)
    {
        switch (btn)
        {
            case "RNE":
                filtroRNE = !filtroRNE;
                break;
            case "CELLNEX":
                filtroCellnex = !filtroCellnex;
                break;
            case "OM":
                filtroOM = !filtroOM;
                break;
            case "FM":
                filtroFM = !filtroFM;
                break;
            case "0":
                filtroOk = !filtroOk;
                break;
            case "1":
                filtroWarning = !filtroWarning;
                break;
            case "2":
                filtroFault = !filtroFault;
                break;
            case "3":
                filtroAlarma = !filtroAlarma;
                break;
            case "4":
                filtroSinConexion = !filtroSinConexion;
                break;
            default:
                break;
        }

        filtros = new List<bool>() { filtroRNE, filtroCellnex, filtroOM, filtroFM, filtroOk, filtroWarning, filtroFault, filtroAlarma, filtroSinConexion };
        List<int> binary = ToBinary(filtros);

        List<string> filtrosCondicionalInicial = new List<string>() { "4", "3", "2", "1", "0", "FM", "OM", "CELLNEX", "RNE" };

        for (int i = 0; i < filtros.Count; i++)
        {
            if (binary[i] == 0)
            {
                filtrosCondicional[i] = "-1";
            }
            else
            {
                filtrosCondicional[i] = filtrosCondicionalInicial[i];
            }
        }

        Debug.Log($"( {filtrosCondicional[0]}, {filtrosCondicional[1]}, {filtrosCondicional[2]}, {filtrosCondicional[3]}, {filtrosCondicional[4]}, {filtrosCondicional[5]}, {filtrosCondicional[6]}, {filtrosCondicional[7]}, {filtrosCondicional[8]} )");

        SetFiltro();
    }

    public void SetFiltro()
    {
        // itera las comunidades
        for (int i = 0; i < GeoList.Count; i++)
        {
            // itera los centros de esa comunidad
            for (int j = 0; j < GeoList[i].transform.Find("Centros").childCount; j++)
            {
                TorreBase t = GeoList[i].transform.Find("Centros").GetChild(j).GetComponent<TorreBase>();
                if (
                    (t.propietario == filtrosCondicional[8] || t.propietario == filtrosCondicional[7])
                    &&
                    (t.frecuencia == filtrosCondicional[6] || t.frecuencia == filtrosCondicional[5])
                    &&
                    (t.estado == int.Parse(filtrosCondicional[4]) || t.estado == int.Parse(filtrosCondicional[3]) || t.estado == int.Parse(filtrosCondicional[2]) || t.estado == int.Parse(filtrosCondicional[1]) || t.estado == int.Parse(filtrosCondicional[0]))
                    )
                {
                    GeoList[i].transform.Find("Centros").GetChild(j).gameObject.SetActive(true);
                }
                else
                {
                    GeoList[i].transform.Find("Centros").GetChild(j).gameObject.SetActive(false);
                }
            }
        }
    }

    public IEnumerator SpawnMarkers()
	{
        if (rtveAPI.Instance.isAssigning)
            yield return new WaitUntil(() => !rtveAPI.Instance.isAssigning);
        //// Se lee de la API el estado de cada centro
        //string jsonString = rtveAPI.Instance.GetEstadoCentrosAsString();
        //var jsonParsed = JSON.Parse(jsonString);
        //var estadoCentros = jsonParsed["results"][0]["centros"];
        // Para cada comunidad
        for (int i = 1; i <= GlobalController.Instance.comunidades.Count; i++)
		{
			// Se selecciona el objeto para la comunidad correspondiente
			GameObject comunidad = transform.GetChild(0).Find(i.ToString()).gameObject;
			// La carpeta "Centros" es donde se instancian los marcadores
			GameObject carpeta = comunidad.transform.GetChild(1).gameObject;
			
			Vector3 currentPosition = transform.position;
			Vector3 currentScale = transform.localScale;
			Quaternion currentRotation = transform.rotation;

			transform.localScale = new Vector3(1, 1, 1);
			transform.rotation = new Quaternion(0, 0, 0, 0);
            //transform.position = new Vector3(0, 0, 0);

            List<Vector3> listaPosiciones = new List<Vector3>();

            // Información general de cada uno de los centros
            //List<TorreBase> centrosComunidad = GlobalController.Instance.GetCentrosComunidad(i);
            //for (int j = 0; j < centrosComunidad.Count; i++)

			foreach (var torre in GlobalController.Instance.GetCentrosComunidad(i))
			{
                if (i == 7)
                {
                    //Debug.Log(torre.codigo_centro);
                }

				// Se instancia el marcador dentro de la carpeta "Centros"
				GameObject g = Instantiate(marker, carpeta.transform);
				g.name = torre.codigo_centro.ToString();
				g.gameObject.tag = "Torre";

				// Se añade la información de ese centro al marcador en el inspector
				TorreBase t = g.AddComponent<TorreBase>();
				t.bbdd = torre.bbdd;
				t.codigo_centro = torre.codigo_centro;
				t.nombre = torre.nombre;
				t.latLon = torre.latLon;
				t.propietario = torre.propietario;
				t.frecuencia = torre.frecuencia;
				t.tipo = torre.tipo;
				t.emision = torre.emision;
				t.cota = torre.cota;
				t.altura = torre.altura;
                t.provincia = torre.provincia;
                t.comunidad = torre.comunidad;
                t.estado = torre.estado;
                t.estado_transmisor1 = torre.estado_transmisor1;
                t.estado_transmisor2 = torre.estado_transmisor2;
                t.estado_transmisor3 = torre.estado_transmisor3;
                t.estado_transmisor4 = torre.estado_transmisor4;

				t.estacion = torre.estacion;

				float ejeX = (float)torre.latLon.LongitudeInDegrees + currentPosition.x;
				float ejeY = transform.position.y + .5f;
				float ejeZ = (float)torre.latLon.LatitudeInDegrees + currentPosition.z;
				// Se resta la coordenada central de españa para poder usar las coordenadas desde el origen del objeto

				g.transform.position = new Vector3(ejeX, ejeY, ejeZ) - geoCenter;

                // Separar marcadores
                SepararMarkers(g, listaPosiciones);

                listaPosiciones.Add(g.transform.position);

                // Se itera por cada centro de la API de estados
                foreach (var centro in GlobalController.Instance.GetAllCentros())
				{
					//Debug.Log(centro.codigo_centro);
					// Selecciona el estado para cada centro respectivo
					if (centro.codigo_centro == torre.codigo_centro)
					{
						// Le asigna un color en función al estado
						int estado = centro.estado;
						switch (estado)
						{
							case 0:
								color = colorOk;
								break;
							case 1:
								color = colorWarning;
								break;
							case 2:
								color = colorFault;
								break;
							case 3:
								color = colorAlarma;
								break;
							case 4:
								color = colorSinConexion;
								break;
							default:
								break;
						}
						// Colorea el marcador

						switch (GlobalController.Instance.platform)
						{
							case Platform.PC:

								Transform circulo = g.transform.Find("Circulo");
								Transform cuadrado = g.transform.Find("Cuadrado");

								Image renderer = null;

								if (torre.frecuencia == "FM")
								{
									circulo.gameObject.SetActive(true);
									renderer = circulo.Find("BackGround").GetComponent<Image>();
								}
								else if (torre.frecuencia == "OM")
								{
									cuadrado.gameObject.SetActive(true);
									circulo.gameObject.SetActive(false);
									renderer = cuadrado.Find("BackGround").GetComponent<Image>();
								}
								else
								{
									circulo.gameObject.SetActive(true);
									renderer = circulo.Find("BackGround").GetComponent<Image>();
								}

								TowerGeometry towerG = g.GetComponent<TowerGeometry>();
								

								if (renderer != null)
									renderer.color = color;

								break;

							case Platform.VR:

								Transform pfCentro = g.transform.Find("pfNivelCentro");
								Transform pfPais = g.transform.Find("pfPais");

								Material MarkerMat = pfPais.Find("3DMarker").GetComponent<MeshRenderer>().materials[0]; // Material exterior del marcador
								Material towerMat = pfCentro.GetComponent<MeshRenderer>().material; // Material principal del modelo 3D a nivel comunidad

								Transform arosCentro3D = pfCentro.Find("Info_centro").Find("Content").Find("Centro").Find("Aros");

								
								if (MarkerMat != null)
									MarkerMat.color = color;

								if (towerMat != null)
								{
									towerMat.SetColor("_Color", color);
									towerMat.SetColor("_OutlineColor", color);
								}

								foreach (Transform aro in arosCentro3D)
								{
									Image imagen = aro.gameObject.GetComponent<Image>();

									if (imagen != null)
									{
										float alpha = imagen.color.a;
										imagen.color = new Color(color.r, color.g, color.b, alpha);
									}
								}

								break;
						}
					}
				}		
			}
			transform.localScale = currentScale;
			transform.rotation = currentRotation;

            //transform.position = currentPosition;

            // Separar marcadores
            //SepararMarkers();
            listaPosiciones.Clear();

			//transform.position = currentPosition;
        }
		// Función para colorear las comunidades

		isChoropleth = true;
		ChoroplethComunidades();
	}
    private void SepararMarkers(GameObject g, List<Vector3> listaPosiciones)
    {
        foreach (Vector3 pos in listaPosiciones)
        {
            float dist = Vector3.Distance(g.transform.position, pos);
            if (dist < .1f)
            {
                g.transform.position = Vector3.MoveTowards(g.transform.position, pos, -(.15f - dist));
            }
        }
    }
  
	// Carga el mapa de Bing según las coordenadas que se pasen de parámetro
	private void SetTorre(TorreBase torre)

    {
        Debug.Log("xd");
        // Desactivamos todas las comunidades y se selecciona el mapa de Bing
        transform.gameObject.SetActive(false);
        GameObject BingMap = transform.parent.Find("BingMap").gameObject;

        // Se establece el centro del mapa y se activa
        BingMap.GetComponent<MapRenderer>().Center = torre.latLon;

		GlobalController.Instance.SelectedTower = torre; // Set selected tower on global controller

		BingMap.SetActive(true);

        //Button btnVolver = BingMap.transform.Find("botones").Find("btnVolver").GetComponent<Button>();
        //btnVolver.onClick.AddListener(HideBingMap);
    }

    // Función para que en el caso que se logee con un usuario que no sea nivel España, te lleve directamente a su comunidad
    /*private void SetPrivilegios()
    {
        string nivel = rtveAPI.Instance.nivel;
        int? nivelInt = int.Parse(nivel);
        if (nivel != "00")
        {
            ZoomComunidad(GeoList[(int) nivelInt - 1], 3f);
        }
    }*/

    private void ChoroplethComunidades()
    {
		// Se lee un json temporal hasta que Manué tenga la API prepaarada
		string jsonString = rtveAPI.Instance.GetEstadoCentrosAsString();
        var jsonParsed = JSON.Parse(jsonString);
        var estadoCentros = jsonParsed["results"][0]["centros"];

        // Iteramos por los poliedros de comunidad
        for (int i = 1; i <= GeoList.Count; i++)
        {
            // Leemos las torres de esa comunidad
            var torres = GlobalController.Instance.GetCentrosComunidad(i);
            List<int> listaEstados = new List<int>();

            foreach (var torre in torres)
            {
                // Excluimos las torres de Cellnex
                if (torre.propietario == "RNE")
                {
                    // Iteramos por los datos de estado_centros
                    foreach (JSONNode centro in estadoCentros)
                    {
                        // Comparamos para que solo lea los estados de la comunidad en la que estamos
                        if (torre.codigo_centro == int.Parse(centro["num_centro"]))
                        {
                            listaEstados.Add(centro["estado_centro"]);
                        }
                    }
                }
            }

            // Si la comunidad contiene algún centro que pertenezca a RNE
            if (listaEstados.Count > 0)
            {
                // Sumamos cada alarma a la variable estado
                float estado = 0f;

                //Debug.Log("Numero de torres: " + listaEstados.Count);
                foreach (int value in listaEstados)
                {
                    switch (value)
                    {
                        case 0:
                            estado += 0;
                            break;
                        // Warning
                        case 1:
                            estado += 0.1f * listaEstados.Count;
                            break;
                        // Fault
                        case 2:
                            estado += 0.25f * listaEstados.Count;
                            break;
                        // Alarma
                        case 3:
                            estado += 0.5f * listaEstados.Count;
                            break;
                        // Ok, Sin conexión
                        case 4:
                            estado += 0;
                            break;
                        default:
                            break;
                    }
                }

                // Se hace una interpolación entre estos dos colores para el mapa coroplético
                

                color = Color.Lerp(colorInicio, colorFin, Mathf.PingPong(estado, 1));
            }
            // Si todos los centros de la comunidad pertenecen a Cellnex
            else
            {
                color = colorSinDatos;
            }
            // Función sacada de stackoverflow que colorea mallas 3D
            ColorizeComunidad(GeoList[i - 1], color);
        }

		animator.enabled = true;

		// Comprobacion permisos

		//int nivel = int.Parse(GlobalController.Instance.Nivel);
		int nivel = int.Parse(rtveAPI.nivel);

		if (nivel > 0 && nivel < 18)
		{
			GlobalController.Instance.SetComunidad(nivel);

			if (GlobalController.Instance.platform == Platform.PC)
			{
				GeometryButton permisos = transform.Find("GeoComunidades").Find(nivel.ToString()).Find("Poligonos").GetComponentInChildren<GeometryButton>();
				UIReferences.Instance.UpdateHeader(GlobalController.Instance.GetNombreComunidad(GlobalController.Instance.SelectedComunidad), DateTime.Now, DateTime.Now);
				
				permisos.PlayElevar();
				Destroy(UIReferences.Instance.botonComunidad.gameObject);
			}

			else
			{
			
				//GlobalController.Instance.actions.SwapCollider(false);
				Comunidad_info info = transform.Find("GeoComunidades").Find(nivel.ToString()).GetComponent<Comunidad_info>();

				if(info == null)
				{
					Debug.Log("Asigna el script a la comunidad pertinente");
				}

				info.Elevar();
			}
		}

		StartCoroutine(UIReferences.Instance.UpdateBodyPais());
        FillChinchetas();
    }

    public void FillChinchetas()
    {
        foreach (GameObject geo in GeoList)
        {
            int codigoComunidad = int.Parse(geo.name);

            List<TorreBase> centros = GlobalController.Instance.GetCentrosComunidadRNE(codigoComunidad);

            Transform datos = geo.transform.Find("Chincheta").Find("Content").Find("bg").Find("Datos");
            TextMeshProUGUI nombre = datos.Find("Text_comunidad").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI nCentros = datos.Find("Text_comunidad").Find("centros").GetComponent<TextMeshProUGUI>();

            nombre.text = GlobalController.Instance.GetNombreComunidad(codigoComunidad);
            nCentros.text = centros.Count.ToString() + " centros";

            int[] estadosTotales = UIReferences.Instance.GetEstados(centros);

            // Itera por los estados ok-SC
            for (int i = 0; i < 5; i++)
            {
                TextMeshProUGUI estadoDato = datos.Find("Content").GetChild(i).Find("ESTADO_DATO").GetComponent<TextMeshProUGUI>();
                int valorEstado = estadosTotales[i];
                estadoDato.text = valorEstado.ToString();
            }
        }
    }

    // Función que cambia el color del material
    private void ColorizeComunidad(GameObject comunidad, Color color)
    {
        GameObject polygons = comunidad.transform.GetChild(0).gameObject;

		for (int i = 0; i < polygons.transform.childCount; i++)
		{
			if (polygons.transform.GetChild(i).name == "Canarias")
			{
				foreach (Transform t in polygons.transform.GetChild(i))
				{
					if (t.name == "Image")
						continue;

					Material mat = t.gameObject.GetComponent<MeshRenderer>().material;
					mat.color = color;
				}
			}

			else
			{
				Material mat = polygons.transform.GetChild(i).GetComponent<MeshRenderer>().material;
				mat.color = color;
			}
		}
    }

    // Esta función desactiva todas las comunidades y muestra en zoom una específica
    void ZoomComunidad(GameObject comunidad, float scale)
    {
        foreach (GameObject geo in GeoList)
        {
            geo.SetActive(false);
        }
        Vector3 newScale = new Vector3(scale, 1f, scale);

        GameObject polygons = comunidad.transform.GetChild(0).gameObject;
        int childCount = polygons.transform.childCount;

        var totalX = 0f;
        var totalZ = 0f;
        Vector3 centerPoint;

        // Hay que centrar la comunidad, normalmente se centra en el polígono principal, pero en el caso de las islas se hace una media de cada coordenada central de la isla para sacar el centro
        if (comunidad == Geo4 || comunidad == Geo5)
        {
            for (int i = 0; i < childCount; i++)
            {
                Debug.Log(polygons.transform.GetChild(i).GetComponent<Renderer>().bounds.center);
                totalX += polygons.transform.GetChild(i).GetComponent<Renderer>().bounds.center.x;
                totalZ += polygons.transform.GetChild(i).GetComponent<Renderer>().bounds.center.z;
            }
            var centerX = totalX / childCount;
            var centerZ = totalZ / childCount;

            centerPoint = new Vector3(centerX, 0f, centerZ);
        }
        else
        {
            centerPoint = polygons.transform.GetChild(polygons.transform.childCount - 1).GetComponent<Renderer>().bounds.center;
        }

        Vector3 position = comunidad.transform.position;
        Vector3 newPosition = position - centerPoint;

        float RS = scale / comunidad.transform.localScale.x;

        comunidad.transform.localScale = newScale;
        comunidad.transform.position = newPosition * RS;
        comunidad.SetActive(true);
    }
}
