﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioFile
{
	public string nombreRadio;
	public string url = "http://centrosemisores.rtve.es:7100/"; // Se le añade el http delante ya que lo necesitamos para que el plugin pueda detectar la radio

	public string comunidad;
	public string provincia;
	public string torre;

	public AudioFile(string radioName, int codigo_centro, string comunidad, string provincia, string torre)
	{
		nombreRadio = radioName;
		url = url + codigo_centro.ToString(); 
		this.comunidad = comunidad;
		this.provincia = provincia;
		this.torre = torre;
	}
}
