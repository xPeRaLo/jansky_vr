﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weather
{
	public string main;
	public string description;
	public string icon;
	public float temperature;
	public float humidity;
	public float wind;
	public int clouds;
	public string timestamp;
}
