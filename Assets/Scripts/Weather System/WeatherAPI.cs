﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using SimpleJSON;
using System.Security.Cryptography.X509Certificates;

public class WeatherAPI : MonoBehaviour
{

	#region SINGLETON

	private static WeatherAPI instance;

	public static WeatherAPI Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<WeatherAPI>();

				if (instance == null)
				{
					GameObject container = new GameObject("Weather API");
					instance = container.AddComponent<WeatherAPI>();
				}
			}

			return instance;
		}
	}
	#endregion

	const string API_KEY = "4f7a0fd4982a9e49d1e371c9996f187d";

    public Weather GetWeather(double lat, double lon)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&appid={2}&lang=es", lat, lon, API_KEY));
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();

        var jsonParsed = JSON.Parse(jsonResponse);

        //Debug.Log(jsonParsed);

        Weather weather = new Weather();

        weather.main = jsonParsed["weather"][0]["main"];
        weather.description = jsonParsed["weather"][0]["description"];
        weather.temperature = jsonParsed["main"]["temp"] - 273f;
        weather.humidity = jsonParsed["main"]["humidity"];
        weather.wind = jsonParsed["wind"]["speed"];
        weather.clouds = jsonParsed["clouds"]["all"];
        weather.timestamp = DateTimeOffset.FromUnixTimeSeconds(jsonParsed["dt"]).ToString("yyyy-MM-dd HH:mm:ss UTC");

		
		weather.icon = jsonParsed["weather"][0]["icon"];

		return weather;
    }
}
