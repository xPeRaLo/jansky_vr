﻿using ChartAndGraph;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GraphType { Graph, Bar, Pie, Radar }

public class GraphFiller : MonoBehaviour
{


	private GraphChart graph = null;
	private BarChart barChart = null;
	private PieChart pieChart = null;
	private RadarChart radarChart = null;

	[SerializeField] private GraphType graphType;


	private void Awake()
	{
		GrabGraph();
	}

	private void OnEnable()
	{
		StartCoroutine(fillGraph());
	}


	void GrabGraph()
	{
		switch (graphType)
		{
			case GraphType.Graph:
				graph = GetComponent<GraphChart>();
				break;
			case GraphType.Bar:
				barChart = GetComponent<BarChart>();
				break;
			case GraphType.Pie:
				pieChart = GetComponent<PieChart>();
				break;
			case GraphType.Radar:
				radarChart = GetComponent<RadarChart>();
				break;
		}
	}

	IEnumerator fillGraph()
	{
		int numberOfRandomData = 11;

		graph.DataSource.StartBatch();

		graph.DataSource.ClearCategory("Value");

		for (int i = 0; i < numberOfRandomData; i++)
		{
			graph.DataSource.AddPointToCategory("Value", new System.DateTime(2020, 10, 15 + i), i * (Random.Range(0, 250)));
		}

		graph.DataSource.EndBatch();

		yield return new WaitForSeconds(2f);


		StartCoroutine(fillGraph());
	}

}
