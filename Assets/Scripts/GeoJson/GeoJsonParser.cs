﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Immersia
{
    public class GeoJsonParser : MonoBehaviour
    {
        //static string specsBaseDir = "/DxRSpecs/";
        string path;
        string jsonString;

		public Material mat;

		void Awake()
		{
			GeneratePolygon();
		}

		private void Start()
		{
			ModifyRotation();
		}

		void ModifyRotation()
		{
			Vector3 position = new Vector3(0f, 0f, 0f);
			Quaternion rotation = new Quaternion(0f, 0f, 180f, 0f);
			Vector3 scale = new Vector3(-1f, 1f, 1f);


			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}

		void GeneratePolygon()
		{
			// Se busca el archivo GeoJSON y se lee como JSONode

			//path = Application.streamingAssetsPath + "/comunidades-autonomas-espanolas.geojson";
			path = Application.streamingAssetsPath + "/provincias-espanolas.geojson";
				jsonString = File.ReadAllText(path);

				JSONNode FeatureCollectionSpecs = JSON.Parse(jsonString);

				// Se inicia un contador que sirve para asignar un número a cada polígono que se genera y sigan un orden
				int j = 0;

				// Se comienza iterando por por cada uno de los features del GeoJSON, es decir, cada Comunidad Autónoma
				foreach (JSONObject Feature in FeatureCollectionSpecs["features"].AsArray)
				{
					string name;

					JSONObject Properties = Feature["properties"].AsObject;

					if (Properties["texto"] != null)
					{
						// Se extrae el string de texto ya que es el nombre de cada Comunidad Autónoma y se usará para nombrar el GameObject
						name = Properties["texto"];
						//Debug.Log("name: " + name);
					}
					else
					{
						throw new System.Exception("Feature id not found");
					}
					string geometry;
					JSONObject GeometrySpecs = Feature["geometry"].AsObject;

					if (GeometrySpecs["type"] != null)
					{
						// Se extrae el string de geometry ya que es el que identifica si una comunidad es un Polygon o un MultiPolygon
						geometry = GeometrySpecs["type"];
						//Debug.Log("geometry: " + geometry);
					}
					else
					{
						throw new System.Exception("Geometry type not found");
					}

					double[] GeoPos = new double[2];
					if (Properties["geo_point_2d"] != null)
					{
						// Se extrae el centro de coordenadas de la Comunidad Autónoma, por ahora no se le da ningún uso
						JSONArray geoArray = Properties["geo_point_2d"].AsArray;
						GeoPos[0] = geoArray[0];
						GeoPos[1] = geoArray[1];
						//Debug.Log("Centre: " + geoArray.ToString());
						//dic_PosBarrios.Add(name, GeoPos);

					}

					// Este es el camino que tomará si la Comunidad Autónoma es un Polygon
					if (geometry == "Polygon")
					{
						//Debug.Log(name + " is a Polygon");

						// Se selecciona el primer valor del Array ya que en los Polígonos no se va a tener en cuenta los agujeros y el primero siempre es el polígono principal
						JSONArray coorValues = GeometrySpecs["coordinates"][0].AsArray;
						// Se crea un array en el que se guardarán las coordenadas del polígono, la dimensión del array es la del número total de coordenadas
						Vector2[] Arr_coordinates = new Vector2[coorValues.Count - 1];

						// Se itera por cada coordenada del polígono
						for (int i = 0; i < coorValues.Count - 1; i++)
						{
							// Se guarda la coordenada X
							Arr_coordinates[i][0] = coorValues[i][0];
							// Se guarda la coordenada Y
							Arr_coordinates[i][1] = coorValues[i][1];
						}

						// Se crea un nuevo objeto que será donde se guarde el polígono
						GameObject NewPlane = new GameObject();

						// Se enparenta dentro del objeto donde está el script, es decir, GeoMap
						NewPlane.transform.parent = transform;
						// Se establecen las dimensiones y posiciones por defecto del nuevo objeto
						NewPlane.transform.localPosition = Vector3.zero;
						NewPlane.transform.localScale = Vector3.one;
						NewPlane.transform.localRotation = Quaternion.identity;


						//int r = Random.Range(0, mat.Length);
						// Se crea un Polygon dentro del objeto para la comunidad
						Polygon poly = NewPlane.AddComponent<Polygon>();
						poly.InitPolygon(Arr_coordinates,mat, j.ToString());
						NewPlane.name = name;
						// Itera j para que el siguiente polígono tenga de nombre el siguiente número que toca
						j++;
					}
					else
					{
						//Debug.Log(name + " is a MultiPolygon");

						// Se seleccionan todos los valores de coordinates porque cada uno será un polígono
						JSONArray geoValuesArray = GeometrySpecs["coordinates"].AsArray;

						j = 0;

						// Como esta vez va a tener mucho polígonos, se crea un objeto con el nombre de la comunindad donde irán guardados todos los polígonos con nombre númerico
						GameObject MultiNewPlane = new GameObject();
						MultiNewPlane.transform.parent = transform;
						MultiNewPlane.name = name;

						foreach (JSONArray polygon in geoValuesArray)
						{
							// // Se selecciona el primer valor del Array ya que en los Polígonos no se va a tener en cuenta los agujeros y el primero siempre es el polígono principal
							JSONArray coorValues = polygon[0].AsArray;
							Vector2[] Arr_coordinates = new Vector2[coorValues.Count - 1];

							for (int i = 0; i < coorValues.Count - 1; i++)
							{

								Arr_coordinates[i][0] = coorValues[i][0];
								Arr_coordinates[i][1] = coorValues[i][1];

							}

							GameObject NewPlane = new GameObject();

							// El nuevo polígono se introduce dentro de MultiNewPlane
							NewPlane.transform.parent = MultiNewPlane.transform;
							NewPlane.transform.localPosition = Vector3.zero;
							NewPlane.transform.localScale = Vector3.one;
							NewPlane.transform.localRotation = Quaternion.identity;

							//int r = Random.Range(0, mat.Length);

							// Al ser multiPolygon necesitamos una lista de Polygon
							List<Polygon> MultiPolygon = new List<Polygon>();

							Polygon poly = NewPlane.AddComponent<Polygon>();
							poly.InitPolygon(Arr_coordinates,mat, j.ToString());
							MultiPolygon.Add(poly);
							NewPlane.name = j.ToString();
							j++;
						}
					}
				}
		}
 
		//        public static string GetStringFromFile(string filename)
		//        {
		//            return File.ReadAllText(filename);
		//        }

		//        public static string GetFullSpecsPath(string filename)
		//        {
		//            return Application.streamingAssetsPath + specsBaseDir + filename;
		//        }
	}
}

