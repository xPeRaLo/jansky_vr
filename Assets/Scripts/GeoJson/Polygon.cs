﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Immersia
{
	[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Polygon : MonoBehaviour
    {

		// Use this for initialization
		public Vector2[] Arr_coordinates;
        public Vector2[] planePoints;
       //MapVis targetMapVis;
        public string PlaneName;

		ProceduralPlane plane;
		Mesh mesh;
		Material mat;

		private void Awake()
		{
			//plane = gameObject.GetComponent<ProceduralPlane>(); // Añadimos componente de tipo procedural plane
			GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			GetComponent<MeshRenderer>().receiveShadows = false;
		}

		private void Start()
		{
			//plane.CreatePLane(Arr_coordinates, mat, mesh); // Creamos el plano con la malla creada anteriormente
		}

		// Inicializamos el poligono

		public void InitPolygon(Vector2[] Arr_coor, 
			Material mat,
            string name)
        {
            this.Arr_coordinates = Arr_coor;
            this.PlaneName = name;
			this.mat = mat;
		}

        public Vector2[] GetCoordinates()
        {
            return Arr_coordinates;
        }

        /*public Vector2[] TransformPoints()
        {
            planePoints = new Vector2[Arr_coordinates.Length];
            for(int i = 0; i < Arr_coordinates.Length; i++)
            {
               // double posX_double = Mathematics.Normalize(targetMapVis.geoRect.left, targetMapVis.geoRect.right, double.Parse(targetMapVis.mapsBoundary.ToString()), -double.Parse(targetMapVis.mapsBoundary.ToString()), double.Parse(Arr_coordinates[i][0].ToString()));
               // float posX = Mathematics.ConvertDoubleFloat(posX_double);

             //  double posY_double = Mathematics.Normalize(targetMapVis.geoRect.top, targetMapVis.geoRect.bottom, -double.Parse(targetMapVis.mapsBoundary.ToString()), double.Parse(targetMapVis.mapsBoundary.ToString()), double.Parse(Arr_coordinates[i][1].ToString()));
             //   float posY = Mathematics.ConvertDoubleFloat(posY_double);

               //Vector2 point = new Vector2(posX, posY);

              // planePoints[i] = point;
            }

            return planePoints;
        }*/
    }
}

