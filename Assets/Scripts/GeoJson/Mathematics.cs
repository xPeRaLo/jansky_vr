﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Mathematics  {

    public static int Normalize(int max, int min, int fixedMax, int fixedMin, int value)
    {
        int normalizeValue = 0;

        normalizeValue = fixedMin + ((fixedMax - fixedMin) * (value - min)) / (max - min);

        if (max == min)
        {
            return fixedMin;
        }
        return normalizeValue;
    }
    //TO DO: Contemplar valores negativos
    public static float Normalize(float max, float min, float fixedMax, float fixedMin, float value)
    {
        float normalizeValue = 0f;

        normalizeValue = fixedMin + ((fixedMax - fixedMin) * (value - min)) / (max - min);

        if (max == min)
        {
            return fixedMin;
        }
        return normalizeValue;
    }

    public static double Normalize(double max, double min, double fixedMax, double fixedMin, double value)
    {
        double normalizeValue = 0f;

        normalizeValue = fixedMin + ((fixedMax - fixedMin) * (value - min)) / (max - min);

        if (max == min)
        {
            return fixedMin;
        }
        return normalizeValue;
    }

    private static float RoundInteger(string value)
    {
        int rounded = 0;

        float tmp = float.Parse(value) / 4.0f;

        int ceiling = Convert.ToInt32(Math.Ceiling(tmp));

        rounded = ceiling * 4;

        return rounded;
    }


    public static List<float> GetMaxMinValues(string year, string field, List<Dictionary<string, Dictionary<string, string>>> lst_opportunityYears)
    {
        List<float> lst_MaxMin = new List<float> { 0f, 0f}; //0 -> Max , 1 -> Min

        int j = 0;
        foreach (Dictionary<string, Dictionary<string, string>> opportunityYears in lst_opportunityYears)
        {
            Dictionary<string, string> value;
            if (opportunityYears.TryGetValue(year, out value))
            {
                string lenght;
                if (value.TryGetValue(field, out lenght))
                {
                    if (float.Parse(lenght) > lst_MaxMin[0])
                    {
                        lst_MaxMin[0] = float.Parse(lenght);
                        if (j == 0)
                        {
                            lst_MaxMin[1] = float.Parse(lenght);
                        }
                        j = 1;
                    }
                    else if (float.Parse(lenght) < lst_MaxMin[1])
                    {
                        lst_MaxMin[1] = float.Parse(lenght);
                    }
                }
            }
        }
        lst_MaxMin[0] = RoundInteger(lst_MaxMin[0].ToString());
        return lst_MaxMin;
    }


    public static bool CompareMaxMin(List<float> lst_MaxMinNowField, List<float> lst_MaxMinComunField)
    {
        bool hasChange = false;
        if (lst_MaxMinNowField[0] > lst_MaxMinComunField[0])
        {
            hasChange = true;

            lst_MaxMinComunField[0] = lst_MaxMinNowField[0];
        }
        if (lst_MaxMinNowField[1] < lst_MaxMinComunField[1])
        {
            hasChange = true;

            lst_MaxMinComunField[1] = lst_MaxMinNowField[1];
        }

        return hasChange;
    }

    public static List<float> CompareMaxMinRet(List<float> lst_MaxMinNowField, List<float> lst_MaxMinComunField)
    {
        if (lst_MaxMinNowField[1] > lst_MaxMinComunField[1])
        {
            lst_MaxMinComunField[1] = lst_MaxMinNowField[1];
        }
        if (lst_MaxMinNowField[0] < lst_MaxMinComunField[0])
        {
            lst_MaxMinComunField[0] = lst_MaxMinNowField[0];
        }
        return lst_MaxMinComunField;
    }

    public static Dictionary<float,float> Interpolate2d(float initValue, float finishValue, List<float> interval, int resolution )
    {
        Dictionary<float, float> values = new Dictionary<float, float>(); //value 2d. Position en "x" e "y"

        float intervalLenght = (interval[1] - interval[0]) / resolution;
        float iniPos = interval[0];
        for(int i = 0; i < resolution; i++)
        {
            //calculate the values to include in values dictionary
            float x = iniPos + intervalLenght * i;
            float y = Mathematics.Normalize(interval[1], interval[0], finishValue, initValue, x);

            values.Add(x, y);
        }
        return values;
    }

    public static float ConvertDoubleFloat(double doubleVal)
    {
        float floatVal = 0;

        // Double to float conversion can overflow.
        try
        {
            floatVal = System.Convert.ToSingle(doubleVal);
            System.Console.WriteLine("{0} as a float is {1}",
                doubleVal, floatVal);
        }
        catch (System.OverflowException)
        {
            System.Console.WriteLine(
                "Overflow in double-to-float conversion.");
        }

        // Conversion from float to double cannot overflow.
        doubleVal = System.Convert.ToDouble(floatVal);
        return floatVal;
    }
    
    public static List<double> GetMaxMin(string field, List<Dictionary<string, string>> dataValue)
    {
        List<double> lst_maxmin = new List<double>();

        for(int i = 0; i< dataValue.Count; i++)
        {
            string value;
            if(dataValue[i].TryGetValue(field, out value))
            {
                if (lst_maxmin.Count == 0)
                {
                    lst_maxmin.Add(double.Parse(value));
                    lst_maxmin.Add(double.Parse(value));
                }
                else
                {
                    if (double.Parse(value) > lst_maxmin[1])
                    {
                        lst_maxmin[1] = double.Parse(value);
                    }
                    else if (double.Parse(value) < lst_maxmin[0])
                    {
                        lst_maxmin[0] = double.Parse(value);
                    }
                }
            }
        }
       
        return lst_maxmin;
    }

    public static List<float> GetMaxMinFloat(string field, List<Dictionary<string, string>> dataValue)
    {
        List<float> lst_maxmin = new List<float>();

        for (int i = 0; i < dataValue.Count; i++)
        {
            string value;
            if (dataValue[i].TryGetValue(field, out value))
            {
                if (lst_maxmin.Count == 0)
                {
                    lst_maxmin.Add(float.Parse(value));
                    lst_maxmin.Add(float.Parse(value));
                }
                else
                {
                    if (float.Parse(value) > lst_maxmin[1])
                    {
                        lst_maxmin[1] = float.Parse(value);
                    }
                    else if (float.Parse(value) < lst_maxmin[0])
                    {
                        lst_maxmin[0] = float.Parse(value);
                    }
                }
            }
        }

        return lst_maxmin;
    }

    public static List<double> Interpolate(double initValue, double finishValue, int resolution)
    {
        List<double> lst_values = new List<double>();

        double interval;
        interval = Math.Abs((finishValue - initValue) / resolution);

        for(int i = 0; i < resolution + 1; i++)
        {
            double value = initValue + i * interval;
            lst_values.Add(value);
        }

        return lst_values;
    }

    //For maps
    public static double Distance(double lng1, double lat1, double lng2, double lat2)
    {
        double d = 0d;

        var R = 6371000d;

        var lat1_radians = ToRadians(lat1);
        var lat2_radians = ToRadians(lat2);

        var delta_lat = ToRadians(lat2 - lat1);
        var delta_lng = ToRadians(lng2 - lng1);

        var a = Math.Sin(delta_lat / 2) * Math.Sin(delta_lat / 2) + Math.Cos(lat1_radians) * Math.Cos(lat2_radians) * Math.Sin(delta_lng / 2) * Math.Sin(delta_lng / 2);

        var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

        d = R * c;

        return d;
    }

    public static double ToRadians(double angle)
    {
        return (Math.PI / 180) * angle;
    }
}