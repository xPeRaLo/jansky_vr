﻿using Microsoft.Geospatial;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BingMapsAPI : MonoBehaviour
{

	static readonly string API_KEY = "Ai9LpUQxZCKxPyeaMLFU0z4MZ0LXqMgXZZ0WpDlTKmV2BjtyA1UHIkVNStC7MObD"; // Clave de API

	double LAT; 
	double LON; 
	int zoom = 12;
	double heading = 0;
	double pitch = 90;

	string LatLon = "";

	public GameObject image;

	private void Awake()
	{
		StartCoroutine(GetMap(40.416775, -3.703790));
	}

	public IEnumerator GetMap(double lat, double lon)
	{

		LAT = lat;
		LON = lon;

		LatLon = LAT + "," + LON; // Asignamos el string nuevo a su ubicacion correspondiente


		string URL = string.Format("http://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial/{0}?zoomlevel={1}&key={2}",LatLon, zoom, API_KEY);

		//Por algun motivo siguiendo los pasos de la documentacion, NO devuelve datos, intuyo que no existiran en su base de datos esa ubicacion en especifico? (ESPAÑA A NIVEL GENERAL)

		//string URL = string.Format("https://dev.virtualearth.net/REST/V1/Imagery/Map/Streetside/{0}/{1}?heading={2}&pitch={3}&key={4}", LatLon, zoom, heading, pitch, API_KEY);



		Debug.Log(URL);

		UnityWebRequest www = new UnityWebRequest(URL);

		//www.SetRequestHeader("access-token", token);

		www.downloadHandler = new DownloadHandlerBuffer(); //Obtenemos la informacion del request

		yield return www.SendWebRequest(); //Devolvemos la web request

		if (www.error == null) // Si no devuelve error
		{
			byte [] jsonParsed = www.downloadHandler.data; // Obtenemos los datos (Array de byte)

			Material mat = image.GetComponent<MeshRenderer>().material;

			Debug.Log(jsonParsed.Length);

		}
		else // Si devuelve error
		{
			Debug.Log(www.error);
		}

	}
}
