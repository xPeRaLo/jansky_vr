﻿using UnityEngine;
using UnityEngine.UI;
using Utils.Enums;
using System.Collections;
using UnityEngine.EventSystems;

public class GeometryButton : MonoBehaviour
{

	Button btnVolver;

	Color normalColor;
	Color HoverColor = Color.green;

	Animator mapAnimator;
	Animator uiAnimator;

	string elevarComunidad = "";
	string volverComunidad = "";

	private AnimacionUI animacionEntradaUI = AnimacionUI.Close_Pais;
	private AnimacionUI animacionSalidaUI = AnimacionUI.Close_Comunidad;

	AnimActions actions;
	Map3DAnimActions mapActions;

	[SerializeField] public AnimacionComunidad comunidad;

	bool isSetHoverColor = false;

	private void Awake()
	{
		mapAnimator = GameObject.Find("3DMap").GetComponent<Animator>();
		uiAnimator = GameObject.Find("Niveles").GetComponent<Animator>();
		btnVolver = UIReferences.Instance.botonComunidad.GetComponent<Button>();
		actions = GameObject.Find("Niveles").GetComponent<AnimActions>();
		mapActions = GameObject.Find("3DMap").GetComponent<Map3DAnimActions>();

		elevarComunidad = "Elevar" + comunidad.ToString();
		volverComunidad = "Volver" + comunidad.ToString();
	}


	private IEnumerator SetHoverColor()
    {
		yield return new WaitUntil(() => MapVis.Instance.isChoropleth);
		isSetHoverColor = true;

		normalColor = gameObject.GetComponent<MeshRenderer>().material.color;

        Transform parent = transform.parent;

		foreach (Transform r in parent)
		{
			MeshRenderer renderer = r.gameObject.GetComponent<MeshRenderer>();

			if (renderer != null)
			{
				float h, s, v;
				Color.RGBToHSV(renderer.material.color, out h, out s, out v);
				renderer.material.color = Color.HSVToRGB(h,s,1);
            }
		}
		isSetHoverColor = false;

		UIReferences.Instance.panelHover.SetActive(true);
	}

	private void OnMouseEnter()
	{
		StartCoroutine(SetHoverColor());
		Transform comunidad = transform.parent.parent;
		UIReferences.Instance.UpdatePanelHover(int.Parse(comunidad.name));
	}

	private void OnMouseExit()
    {
		StartCoroutine(SetHoverColorExit());
    }

    private IEnumerator SetHoverColorExit()
    {
		yield return new WaitUntil(() => !isSetHoverColor);

		Transform parent = transform.parent;

        foreach (Transform r in parent)
        {
            MeshRenderer renderer = r.gameObject.GetComponent<MeshRenderer>();

            if (renderer != null)
                renderer.material.color = normalColor;
        }

		UIReferences.Instance.panelHover.SetActive(false);
    }

    private void OnMouseDown()
	{
		if (mapAnimator.enabled)
		{
			if (btnVolver.gameObject != null)
			{
				btnVolver.onClick.RemoveAllListeners();

				btnVolver.onClick.AddListener(() => mapAnimator.Play(volverComunidad));
				btnVolver.onClick.AddListener(() => uiAnimator.Play(animacionSalidaUI.ToString()));
				btnVolver.onClick.AddListener(() => actions.SetColliders3(false));
				btnVolver.onClick.AddListener(() => UIReferences.Instance.botonComunidad.SetActive(false));
				btnVolver.onClick.AddListener(() => UIReferences.Instance.textoCentroTitulo.text = "España");
				btnVolver.onClick.AddListener(() => GlobalController.Instance.SelectedComunidad = 0);

				mapAnimator.Play(elevarComunidad);
				//uiAnimator.Play(animacionEntradaUI.ToString());

				GlobalController.Instance.SelectedComunidad = int.Parse(transform.parent.parent.name);
				UIReferences.Instance.botonComunidad.SetActive(true);
			}
		}
	}

	public void PlayElevar()
	{
		mapAnimator.Play(elevarComunidad);
	}
}
