﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Enums;

public class Comunidad_info : MonoBehaviour
{
	[HideInInspector] string elevarComunidad = "";
	[SerializeField] AnimacionComunidad comunidad;

	private void Awake()
	{
		elevarComunidad = "Elevar" + comunidad.ToString();
	}

	public void Elevar()
	{
		if(transform.parent.parent.parent.TryGetComponent(out Animator animator))
			animator.Play(elevarComunidad);

		Destroy(transform.Find("Botones").Find("btnVolver").gameObject);
	}
}
