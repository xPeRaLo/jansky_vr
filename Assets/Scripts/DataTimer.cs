﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DataTimer : MonoBehaviour
{

	float timer = 0;
	float maxTime = 0;
	[HideInInspector] public bool isActive = false;

	[SerializeField] Image background;
	[SerializeField] TextMeshProUGUI secondsRemaining;


	public void SetTimer(float time)
	{
		maxTime = time;
		timer = maxTime;

		background.fillAmount = 1f;
		secondsRemaining.text = time.ToString("F0");

		isActive = true;
	}


	private void Update()
	{
		if (isActive)
		{
			timer -= Time.deltaTime * 1;

			background.fillAmount = Normalize(0, maxTime, timer);
			secondsRemaining.text = timer.ToString("F0");

			if (timer <= 0)
			{
				isActive = false;
			}
				
		}
	}

	float Normalize(float min, float max, float value)
	{
		return Mathf.InverseLerp(min, max, value);
	}
}
