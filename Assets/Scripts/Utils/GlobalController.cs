﻿using Microsoft.Geospatial;
using System.Collections;
using System.Collections.Generic;
using UniStorm;
using UnityEngine;
using SimpleJSON;
using System.IO;
using Microsoft.Maps.Unity;
using Utils.Enums;
using Crosstales.Radio;
using System;
using UnityEngine.SceneManagement;

public class GlobalController : MonoBehaviour
{
	#region SINGLETON

	private static GlobalController instance;

	public static GlobalController Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<GlobalController>();

				if (instance == null)
				{
					GameObject container = new GameObject("Global Controller");
					instance = container.AddComponent<GlobalController>();
				}
			}

			return instance;
		}
	}
	#endregion

	public MapRenderer BingMap;

	public Platform platform;
	[SerializeField] private Animator canvasAnimator;
	[SerializeField] private Animator filtrosAnimator;

	#region Properties

	private TorreBase selectedTower;

	public TorreBase SelectedTower
	{
		get { return selectedTower; }
		set
		{

			selectedTower = value;
			rtveAPI.Instance.nivelCentroSynced = false;
			StartCoroutine(LoadCentro(canvasAnimator, "Comunidad-Centro"));
			
		}
	}

	private int selectedComunidad;

	public int SelectedComunidad
	{
		get { return selectedComunidad; }
		set
		{
			selectedComunidad = value;

			if (value == 0)
				return;

			StartCoroutine(LoadComunidad(canvasAnimator));
			actions.SetColliders3(true);
		}

	}

	#endregion

	[HideInInspector] public List<Comunidad> comunidades = new List<Comunidad>(); // Lista de comunidades
	[SerializeField] private RadioPlayer radio;

	[Header("Weather System")]
	public UniStormSystem weatherSystem;
	public Material unistormSkybox;
	public Material space_skybox;
	public Light unistormLight;
	public Light mainLight;

	public TextAsset codigosComunidad;
	[SerializeField] AnimActions actions;

	private void Awake()
	{

		#region FPS Cap

		switch (platform)
		{
			case Platform.PC:
				Application.targetFrameRate = 60;
				break;
			case Platform.VR:
				Application.targetFrameRate = 90;
				break;
		}

		#endregion

	}

	public void RestoreHeader()
	{
		UIReferences.Instance.textoCentroTitulo.text = SelectedTower.comunidad.ToString();
		rtveAPI.Instance.StopAllCoroutines();
		SelectedTower = null;
	}

	public void RestoreSkybox()
	{
		StartCoroutine(RestoreSkyboxCoroutine());
	}

	IEnumerator  RestoreSkyboxCoroutine()
	{
		weatherSystem.RealWorldTime = UniStormSystem.EnableFeature.Disabled;

		weatherSystem.gameObject.SetActive(false);

		yield return new WaitUntil(() => !weatherSystem.gameObject.activeSelf);

		UniStormManager.Instance.SetTime(2, 0);

		weatherSystem.gameObject.SetActive(true);

		yield return null;

		weatherSystem.gameObject.SetActive(false);

		RenderSettings.skybox = space_skybox;
		RenderSettings.sun = mainLight;
	}

	public void PlayRadio(AudioFile estacion)
	{
		radio.Station.Url = estacion.url;

		try
		{
			radio.Play();
		}catch(Exception e)
		{
			Debug.Log("Error: " + e.ToString());
			StopRadio();
			UIReferences.Instance.btnPlay.gameObject.SetActive(true);
			UIReferences.Instance.btnStop.gameObject.SetActive(false);
			UIReferences.Instance.loadingIcon.SetActive(false);
		}
	}

	public void StopRadio()
	{
		radio.Stop();
		radio.Station.Url = "";
		
	}

	public void DeactivateRadioElements()
	{
		UIReferences.Instance.btnPlay.gameObject.SetActive(false);
		UIReferences.Instance.btnStop.gameObject.SetActive(false);
		UIReferences.Instance.loadingIcon.gameObject.SetActive(false);

		StopRadio();

	}

	public GameObject GetRadio()
	{
		return radio.gameObject;
	}


	#region Get centros functions 

	public string GetNombreComunidad(int codigo_comunidad)
	{
		foreach (Comunidad c in comunidades)
		{
			if (c.numeroComunidad == codigo_comunidad)
			{
				return c.nombreComunidad;
			}
		}
		return null;

	}

	public List<TorreBase> GetAllCentros()
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			foreach (Provincia p in c.provincias)
			{
				foreach (Centro ce in p.centros)
				{
					if (ce.torre.propietario == "RNE")
					{
						torres.Add(ce.torre);
					}
				}
			}
		}
		return torres;
	}
	public List<TorreBase> GetAllCentrosFM()
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			foreach (Provincia p in c.provincias)
			{
				foreach (Centro ce in p.centros)
				{
					if (ce.torre.propietario == "RNE" && ce.torre.frecuencia == "FM")
					{
						torres.Add(ce.torre);
					}

				}
			}
		}
		return torres;
	}
	public List<TorreBase> GetAllCentrosOM()
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			foreach (Provincia p in c.provincias)
			{
				foreach (Centro ce in p.centros)
				{
					if (ce.torre.propietario == "RNE" && ce.torre.frecuencia == "OM")
					{
						torres.Add(ce.torre);
					}

				}
			}
		}
		return torres;
	}
	public List<TorreBase> GetCentrosComunidad(int codigoComunidad)
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			if (c.numeroComunidad == codigoComunidad)
			{
				foreach (Provincia p in c.provincias)
				{
					foreach (Centro ce in p.centros)
					{
						torres.Add(ce.torre);
					}
				}
			}
		}

		return torres;
	}
	public List<TorreBase> GetCentrosComunidadRNE(int codigoComunidad)
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			if (c.numeroComunidad == codigoComunidad)
			{
				foreach (Provincia p in c.provincias)
				{
					foreach (Centro ce in p.centros)
					{
						if (ce.torre.propietario == "RNE")
						{
							torres.Add(ce.torre);
						}
					}
				}
			}
		}

		return torres;
	}
	public List<TorreBase> GetCentrosComunidadFMRNE(int codigoComunidad)
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			if (c.numeroComunidad == codigoComunidad)
			{
				foreach (Provincia p in c.provincias)
				{
					foreach (Centro ce in p.centros)
					{
						if (ce.torre.propietario == "RNE" && ce.torre.frecuencia == "FM")
						{
							torres.Add(ce.torre);
						}
					}
				}
			}
		}

		return torres;
	}
	public List<TorreBase> GetCentrosComunidadOMRNE(int codigoComunidad)
	{
		List<TorreBase> torres = new List<TorreBase>();

		foreach (Comunidad c in comunidades)
		{
			if (c.numeroComunidad == codigoComunidad)
			{
				foreach (Provincia p in c.provincias)
				{
					foreach (Centro ce in p.centros)
					{
						if (ce.torre.propietario == "RNE" && ce.torre.frecuencia == "OM")
						{
							torres.Add(ce.torre);
						}
					}
				}
			}
		}

		return torres;
	}

	public TorreBase GetCentro(int codigoCentro)
	{
		TorreBase torre = null;

		foreach (Comunidad c in comunidades)
		{
			foreach (Provincia p in c.provincias)
			{
				foreach (Centro ce in p.centros)
				{
					if (ce.torre.codigo_centro == codigoCentro)
					{
						torre = ce.torre;
						return torre;
					}
				}
			}
		}

		return null;
	}

	public List<int> GetEstadosTransmisores(int codigoCentro)
	{
		TorreBase t = GetCentro(codigoCentro);
		string bbdd = t.bbdd;
		int num = int.Parse(bbdd.Substring(bbdd.Length - 1));

		int nTransmisores = int.Parse(t.tipo.Substring(0, 1));
		int nReservas = int.Parse(t.tipo.Substring(1, 2));

		List<int> estadosTransmisores = new List<int>();

		switch (num)
		{
			case 1:
				TorreFM1 t1 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM1>();
				if (nReservas > 0)
				{
					estadosTransmisores.Add(t1.txr_estado);
				}
				else
				{
					estadosTransmisores.Add(-1);
				}
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t1.GetType().GetField($"tx{i}_estado").GetValue(t1).ToString()));
				}
				//estadosTransmisores.Add(int.Parse(t1.GetType().GetField($"txr_estado").GetValue(t1).ToString()));
				break;
			case 2:
				TorreFM2 t2 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM2>();
				if (nReservas > 0)
				{
					estadosTransmisores.Add(t2.txr_estado);
				}
				else
				{
					estadosTransmisores.Add(-1);
				}
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t2.GetType().GetField($"tx{i}_estado").GetValue(t2).ToString()));
				}
				break;
			case 3:
				TorreFM3 t3 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM3>();
				if (nReservas > 0)
				{
					estadosTransmisores.Add(t3.txr_estado);
				}
				else
				{
					estadosTransmisores.Add(-1);
				}
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t3.GetType().GetField($"tx{i}_estado").GetValue(t3).ToString()));
				}
				break;
			case 4:
				TorreFM4 t4 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM4>();
				estadosTransmisores.Add(-1);
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t4.GetType().GetField($"tx{i}_estado").GetValue(t4).ToString()));
				}
				break;
			case 5:
				TorreFM5 t5 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM5>();
				if (nReservas > 0)
				{
					estadosTransmisores.Add(t5.txr_estado);
				}
				else
				{
					estadosTransmisores.Add(-1);
				}
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t5.GetType().GetField($"tx{i}_estado").GetValue(t5).ToString()));
				}
				break;
			case 6:
				TorreFM6 t6 = GameObject.Find(codigoCentro.ToString()).GetComponent<TorreFM6>();
				if (nReservas > 0)
				{
					estadosTransmisores.Add(t6.txr_estado);
				}
				else
				{
					estadosTransmisores.Add(-1);
				}
				for (int i = 1; i <= nTransmisores; i++)
				{
					estadosTransmisores.Add(int.Parse(t6.GetType().GetField($"tx{i}_estado").GetValue(t6).ToString()));
				}
				break;
		}

		return estadosTransmisores;
	}

	#endregion

	public void SetComunidad(int x)
	{
		SelectedComunidad = x;
	}
	public string TraducirComunidad(string codigo)
	{
		int codigoInt = int.Parse(codigo);

		JSONNode jsonParsed = JSON.Parse(codigosComunidad.text);

		return jsonParsed[codigoInt];
	}

	IEnumerator LoadCentro(Animator animator, string animation)
	{

		if (animator == null)
		{
			Debug.LogError("Animator no asignado o nulo");

			yield break;
		}

		if (SelectedTower == null)
			yield break;

		if (platform == Platform.PC)
			rtveAPI.Instance.mapVis.gameObject.SetActive(false);

		if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Open_Centro"))
			animator.Play(animation);

		if (platform == Platform.PC)
		{
			BingMap.Center = selectedTower.latLon;
			yield return new WaitForMapLoaded(rtveAPI.Instance.mapaBing.GetComponent<MapRenderer>());
			BingMap.gameObject.SetActive(true);	
		}

		if (platform == Platform.VR)
		{
			weatherSystem.gameObject.SetActive(true);
			yield return new WaitUntil(() => weatherSystem.gameObject.activeSelf);

			RenderSettings.skybox = unistormSkybox;
			RenderSettings.sun = unistormLight;

			rtveAPI.Instance.mapaBing.GetComponent<MapRenderer>().Center = SelectedTower.latLon;
			rtveAPI.Instance.mapVis.gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Bing");
			yield return new WaitForMapLoaded(rtveAPI.Instance.mapaBing.GetComponent<MapRenderer>());
			rtveAPI.Instance.mapVis.gameObject.transform.parent.GetComponent<Animator>().SetTrigger("BingReady");

			weatherSystem.RealWorldTime = UniStormSystem.EnableFeature.Enabled;
			SwitchWeather(SelectedTower.latLon);
		}

		if (SelectedTower != null)
			StartCoroutine(rtveAPI.Instance.GetData(SelectedTower.codigo_centro, true));

	}

	IEnumerator LoadComunidad(Animator animator, string animation = "Close_Pais")
	{
		if (animator == null)
		{
			Debug.LogError("Animator no asignado o nulo");

			yield break;
		}

		if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Open_Comunidad"))
			animator.Play(animation);

		StartCoroutine(UIReferences.Instance.UpdateBodyComunidad(SelectedComunidad));
	}


	#region Weather System

	#region Weather Types
	[Space]

	public WeatherType thunderstorm;
	public WeatherType drizzle;
	public WeatherType rain;
	public WeatherType snow;
	public WeatherType fog;
	public WeatherType haze;
	public WeatherType clear;
	public WeatherType clouds;

	#endregion

	void SwitchWeather(LatLon location)
	{
		if (!weatherSystem.isActiveAndEnabled || weatherSystem == null)
			return;

		Weather currentWeather = WeatherAPI.Instance.GetWeather(location.LatitudeInDegrees, location.LongitudeInDegrees);

		string mainWeather = currentWeather.main; // Weather main condition
		float currentWind = currentWeather.wind; // Weather wind amount

		switch (mainWeather) // If main Weather returns
		{
			case "Thunderstorm": // Thunderstorm
				weatherSystem.ChangeWeather(thunderstorm);
				break;
			case "Drizzle": // Drizzle
				weatherSystem.ChangeWeather(drizzle);
				break;
			case "Rain": // Rain
				weatherSystem.ChangeWeather(rain);
				break;
			case "Snow": // Snow
				weatherSystem.ChangeWeather(snow);
				break;
			case "Fog": // Fog
				weatherSystem.ChangeWeather(fog);
				break;
			case "Haze": // Haze
				weatherSystem.ChangeWeather(haze);
				break;
			case "Clear": // Clear
				weatherSystem.ChangeWeather(clear);
				break;
			case "Clouds": // Clouds
				weatherSystem.ChangeWeather(clouds);
				break;
			default:
				break;
		}

		weatherSystem.CurrentWindIntensity = currentWind; // Sets wind intensity
	}


	#endregion
}


