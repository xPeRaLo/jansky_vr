﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Microsoft.CSharp;
using System;
using SimpleJSON;
using UnityEngine.Networking;
using Microsoft.Geospatial;

public class jsonReader : MonoBehaviour
{

	public TextAsset rtveData;

	void Awake()
    {
		RellenarComunidades();
	}

	private void RellenarComunidades()
	{
		#region Torres FM 

		GameObject torres = new GameObject("Torres");
		GameObject torresOM = new GameObject("Torres OM");
		GameObject torresFM1 = new GameObject("Torres FM1");
		GameObject torresFM2 = new GameObject("Torres FM2");
		GameObject torresFM3 = new GameObject("Torres FM3");
		GameObject torresFM4 = new GameObject("Torres FM4");
		GameObject torresFM5 = new GameObject("Torres FM5");
		GameObject torresFM6 = new GameObject("Torres FM6");
		GameObject torresCellnex = new GameObject("Torres Cellnex");

		torresOM.transform.parent = torres.transform;
		torresFM1.transform.parent = torres.transform;
		torresFM2.transform.parent = torres.transform;
		torresFM3.transform.parent = torres.transform;
		torresFM4.transform.parent = torres.transform;
		torresFM5.transform.parent = torres.transform;
		torresFM6.transform.parent = torres.transform;
		torresCellnex.transform.parent = torres.transform;

		#endregion

		JSONNode jsonParsed = JSON.Parse(rtveData.text);

		int indexComunidades = 0; // Indice para controlar las comunidades
		int indexProvincias = 0; // Indice para controlar las provincias

		foreach (var com in jsonParsed)
		{

			GlobalController.Instance.comunidades.Add(new Comunidad(com.Value["nombre"],
																	com.Value["codigo"])); // Añadimos una nueva comunidad con el nombre del JSON

			foreach (var pro in com.Value["provincias"])
			{
				GlobalController.Instance.comunidades[indexComunidades].AddProvincia(pro.Value["nombre"],
																					 pro.Value["codigo"]); // Añadimos a su respectiva comunidad una provincia nueva con el nombre leido del JSON

				foreach (var ciu in pro.Value["centros"])
				{
					//Debug.Log(ciu.Value["num_centro"]);
					string bbdd = ciu.Value["bbdd"];

					int codigo_centro = ciu.Value["num_centro"];
					string nombre = ciu.Value["nombre"];
					string frecuencia = ciu.Value["frecuencia"];
					string emision = ciu.Value["emision"];
					string tipo = ciu.Value["tipo"];
					double lat = ciu.Value["lat"];
					double lon = ciu.Value["lon"];
					int cota = ciu.Value["cota"];
					int altura = ciu.Value["altura"];
					string propietario = ciu.Value["propietario"];
					int tieneAudio = ciu.Value["audio"].AsInt;

					string provincia = pro.Value["nombre"];
					string comunidad = com.Value["nombre"];


					if (bbdd != "cellnex")   // Si proviene de la bbdd de rne
					{
						GameObject go = new GameObject(codigo_centro.ToString());
						go.gameObject.tag = "Torre";

						TorreBase t = null;

						switch (bbdd.ToLower().ToString())
						{
							case "bbdd":
								t = go.AddComponent<TorreOM>();
								go.transform.parent = torresOM.transform;
								break;
							case "bbdd_1":
								t = go.AddComponent<TorreFM1>();
								go.transform.parent = torresFM1.transform;
								break;
							case "bbdd_2": // Tipo de torre 2
								t = go.AddComponent<TorreFM2>();
								go.transform.parent = torresFM2.transform;
								break;
							case "bbdd_3": // Tipo de torre 3
								t = go.AddComponent<TorreFM3>();
								go.transform.parent = torresFM3.transform;
								break;
							case "bbdd_4": // Tipo de torre 4
								t = go.AddComponent<TorreFM4>();
								go.transform.parent = torresFM4.transform;
								break;
							case "bbdd_5": // Tipo de torre 5
								t = go.AddComponent<TorreFM5>();
								go.transform.parent = torresFM5.transform;
								break;
							case "bbdd_6": // Tipo de torre 6
								t = go.AddComponent<TorreFM6>();
								go.transform.parent = torresFM6.transform;
								break;
							default:
								break;
						}
						t.bbdd = bbdd.ToLower().ToString();
						t.codigo_centro = codigo_centro;
						t.nombre = nombre;
						t.latLon = new LatLon(lat, lon);
						t.propietario = propietario;
						t.frecuencia = frecuencia;
						t.tipo = tipo;
						t.emision = emision;
						t.cota = cota;
						t.altura = altura;

						t.provincia = provincia;
						t.comunidad = comunidad;

						switch (tieneAudio)
						{
							case 0:
								t.estacion = null;
								break;
							default:
								t.estacion = new AudioFile(nombre, tieneAudio, comunidad, provincia, nombre);
								break;
						}
							
						go.SetActive(true);

						GlobalController.Instance.comunidades[indexComunidades].provincias[indexProvincias].AddCentro(t);
					}
					else // Si las torres son de cellnex, se le asignan variables placeholder temporales
					{
						GameObject torrefmCellnex = new GameObject(codigo_centro.ToString());
						torrefmCellnex.transform.parent = torresCellnex.transform;
						torrefmCellnex.gameObject.tag = "Torre";

						TorreCellnex tCellnex = torrefmCellnex.AddComponent<TorreCellnex>();
						tCellnex.bbdd = bbdd.ToLower().ToString();
						tCellnex.codigo_centro = codigo_centro;
						tCellnex.nombre = nombre;
						tCellnex.latLon = new LatLon(lat, lon);
						tCellnex.propietario = propietario;
						tCellnex.frecuencia = frecuencia;
						tCellnex.cota = cota;
						tCellnex.altura = altura;
						tCellnex.provincia = provincia;
						tCellnex.comunidad = comunidad;

						switch (tieneAudio)
						{
							case 0:
								tCellnex.estacion = null;
								break;								
							default:
								tCellnex.estacion = new AudioFile(nombre, tieneAudio, comunidad, provincia, nombre);
								break;
						}

						GlobalController.Instance.comunidades[indexComunidades].provincias[indexProvincias].AddCentro(tCellnex);
						torrefmCellnex.gameObject.SetActive(true);
					}

				}

				indexProvincias++; // Cada provincia nueva incrementamos
			}

			indexProvincias = 0; // Cuando salimos de una comunidad reseteamos el indice de provincias
			indexComunidades++; // Cuando terminamos de insertar todas las provincias de una comunidad incrementamos 
		}
	}
}	