﻿using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIReferences : MonoBehaviour
{
	#region SINGLETON

	private static UIReferences instance;

	public static UIReferences Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<UIReferences>();

				if (instance == null)
				{
					GameObject container = new GameObject("UIReferences");
					instance = container.AddComponent<UIReferences>();
				}
			}

			return instance;
		}
	}
	#endregion

	#region Variables

	float animSpeed = 0.01f;
	float animationSpeed = 3;
	bool isPC = false;
	bool isUpdating = false;

	[Header("Login Window")]

	public TMP_InputField UsuarioInput;
	public TMP_InputField PasswordInput;
	public Toggle passwordToggle;
	public GameObject passwordVisualizer;
	public Button LoginButton;
	public GameObject WarningText;
	public GameObject SuccessText;
	public UIManagerProgressBarLoop progressBar;
	public GameObject UINivelCentro;
	public GameObject UINivelComunidad;
	public GameObject UINivelSpain;

	[Header("Cabecera")]

	[SerializeField] private TextMeshProUGUI textoFecha;
	[SerializeField] private TextMeshProUGUI textoHora;
	[SerializeField] private TextMeshProUGUI textoActualizacion;
	[SerializeField] public TextMeshProUGUI textoCentroTitulo;

	public GameObject botonComunidad;
	public GameObject botonCentro;

	[SerializeField] GameObject panelSalir;

	[Header("UI Pais")]

	[SerializeField] private GameObject graficaBarrasPais;
	[SerializeField] private GameObject graficaCircularPais;
	[SerializeField] private GameObject graficaCuadradosPais;
	[SerializeField] private GameObject graficaFrecuenciasPais;

	[Header("UI Comunidad")]

	[SerializeField] private GameObject graficaBarrasComunidad;
	[SerializeField] private GameObject graficaCircularComunidad;
	[SerializeField] private GameObject graficaCuadradosComunidad;
	[SerializeField] private GameObject graficaFrecuenciasComunidad;

	[Header("UI Centros")]

	[SerializeField] private GameObject sinConexion;

	[SerializeField] private GameObject modem;
	[SerializeField] private TextMeshProUGUI modemNombre;
	[SerializeField] private TextMeshProUGUI modemBitRateIn;
	[SerializeField] private TextMeshProUGUI modemBitRateOut;
	[SerializeField] private TextMeshProUGUI modemRSSIValor;
	[SerializeField] private GameObject RSSIFlecha;
	[SerializeField] private GameObject frecuencias;
	[SerializeField] private GameObject FRbajo;
	[SerializeField] private GameObject FRalto;

	[SerializeField] private GameObject receptor;
	[SerializeField] private GameObject audios;
	[SerializeField] private GameObject alarmasOM;
	[SerializeField] private Transform[] listaAudios;
	[SerializeField] private TextMeshProUGUI receptorBitrate;
	[SerializeField] private TextMeshProUGUI receptorTemperature;
	[SerializeField] private TextMeshProUGUI receptorRFPower;
	[SerializeField] private TextMeshProUGUI receptorCN;
	[SerializeField] private GameObject RFFlecha;
	[SerializeField] private GameObject CNFlecha;

	//[SerializeField] private Transform[] audios;

	[SerializeField] private GameObject transmisores;
	[SerializeField] private GameObject bbddOM;
	[SerializeField] private GameObject bbdd1_4_1;
	[SerializeField] private GameObject bbdd2_4_1;
	[SerializeField] private GameObject bbdd3_4_1;
	[SerializeField] private GameObject bbdd4_4_0;
	[SerializeField] private GameObject bbdd5_4_1;
	[SerializeField] public GameObject bbdd6_4_1;

	[Header("Radio")]
	public Button btnPlay;
	public Button btnStop;
	public GameObject loadingIcon;

	[Header("Otros")]
	[SerializeField] public GameObject panelCentro;
	[SerializeField] private GameObject cuadrado;
	[SerializeField] private GameObject pf_barrasPais;
	[SerializeField] private GameObject pf_barrasComunidad;
	[SerializeField] private GameObject image_top_reserva;
	[SerializeField] public GameObject infoCentro;
	[SerializeField] public Transform infoBing;
	[SerializeField] public GameObject panelHover;

	Color colorRojo = new Color32(212, 20, 20, 255);
	Color colorVerde = new Color32(140, 198, 63, 255);

	Color colorWarning = new Color32(255, 224, 0, 92);
	Color colorFault = new Color32(247, 147, 30, 92);
	Color colorAlarma = new Color32(212, 20, 20, 92);
	Color colorSinConexion = new Color32(7, 127, 140, 92);

	Color colorAlarmaA = new Color32(212, 20, 90, 255);
	Color colorFaultA = new Color32(247, 147, 30, 255);
	Color colorWarningA = new Color32(252, 238, 33, 255);
	Color colorOkA = new Color32(140, 198, 63, 255);
	Color colorSinConexionA = new Color32(0, 161, 178, 255);
	

	#region Eventos

	UnityEvent comunidadEvent = null;
	UnityEvent ciudadEvent = null;

	#endregion

	#endregion

	private void Start()
	{
        //StartCoroutine(UpdateBodyPais());
        if (SceneManager.GetActiveScene().name == "MAIN_PC")
        {
            isPC = true;
        }
        else
        {
            isPC = false;
        }

    }

    private void Update()
    {
		if (SceneManager.GetActiveScene().name == "LOGIN_PC" || SceneManager.GetActiveScene().name == "LOGIN_VR")
			return;

		textoHora.text = DateTime.Now.ToString("HH:mm:ss");
    }

	#region Funciones para Actualizar UI
	public IEnumerator UpdateBodyPais()
    {
		//UpdateHeader("España", DateTime.Now, DateTime.Now);

		int centrosTotales = GlobalController.Instance.GetAllCentros().Count;

        // =========================== BARRAS

        int nComunidades = graficaBarrasPais.transform.Find("barras").childCount;

        // Itera por cada comunidad 0-16 (1-17)
        for (int i = 0; i < nComunidades; i++)
        {
            GameObject comunidad = graficaBarrasPais.transform.Find("barras").GetChild(i).gameObject;
            comunidad.transform.Find("Estados").Find("Background").GetComponent<Image>().enabled = false;

            GameObject estados = comunidad.transform.Find("Estados").Find("Background").Find("Estados").gameObject;
            var centrosComunidad = GlobalController.Instance.GetCentrosComunidadRNE(i + 1);

            // Función para calcular el porcentaje estados de cada comunidad
            int nCentrosComunidad = centrosComunidad.Count;

            int warningComunidad = 0;
            int faultComunidad = 0;
            int alarmaComunidad = 0;
            int okComunidad = 0;
            int sinConexionComunidad = 0;

            foreach (var centro in centrosComunidad)
            {
                switch (centro.estado)
                {
                    // Ok
                    case 0:
                        okComunidad += 1;
                        break;
                    // Warning
                    case 1:
                        warningComunidad += 1;
                        break;
                    // Fault
                    case 2:
                        faultComunidad += 1;
                        break;
                    // Alarma
                    case 3:
                        alarmaComunidad += 1;
                        break;
                    // Sin conexión
                    case 4:
                        sinConexionComunidad += 1;
                        break;
                    default:
                        break;
                }
            }
            float[] porcentajesComunidad = new float[5]
            {
                (float)okComunidad / nCentrosComunidad,
                (float)faultComunidad / nCentrosComunidad,
                (float)warningComunidad / nCentrosComunidad,
                (float)alarmaComunidad / nCentrosComunidad,
                (float)sinConexionComunidad / nCentrosComunidad,
            };
			
            // Itera por los tipos de estado ok, warning, fault...
            for (int j = 0; j < estados.transform.childCount; j++)
            {
                RectTransform barra = estados.transform.GetChild(j).GetComponent<RectTransform>();
				float maxDim;
				if (isPC)
					maxDim = 183.1228f;
				else
					maxDim = 461.4788f;

				float valorEstado = InterpolateValue(porcentajesComunidad[j].ToString(), 0, 1, 0, maxDim);

                //StartCoroutine(AnimPorcentajeBarras(barra, valorEstado, maxDim / 5f));
                LeanTween.value(gameObject, maxDim / 5f, valorEstado, animationSpeed)
					.setOnUpdate((x) => SetPorcentajeBarras(x, barra));
			}
        }

        // =========================== CIRCULO

        var centros = GlobalController.Instance.GetAllCentros();
        var graficaCircular = graficaCircularPais;

        FillGraficaCircular(centrosTotales, centros, graficaCircular);

        // =========================== CUADRADOS

        var centrosFM = GlobalController.Instance.GetAllCentrosFM();
        var centrosOM = GlobalController.Instance.GetAllCentrosOM();
        var graficaCuadrados = graficaCuadradosPais;

        yield return FillGraficaCuadrados(centrosFM, centrosOM, graficaCuadrados);

        // =========================== FRECUENCIAS

        var graficaFrecuancias = graficaFrecuenciasPais;

        FillGraficaFrecuencias(centrosTotales, centrosFM, graficaFrecuancias);
    }
    public IEnumerator UpdateBodyComunidad(int codigoComunidad)
    {

		UpdateHeader(GlobalController.Instance.GetNombreComunidad(codigoComunidad), DateTime.Now, rtveAPI.Instance.lastUpdate);

		int centrosTotales = GlobalController.Instance.GetCentrosComunidadRNE(codigoComunidad).Count;
		// =========================== BARRAS

		var graficaBarras = graficaBarrasComunidad;
		var centros = GlobalController.Instance.GetCentrosComunidadRNE(codigoComunidad);

		for (int i = 0; i < graficaBarras.transform.Find("barras").childCount; i++)
        {
			Destroy(graficaBarras.transform.Find("barras").GetChild(i).gameObject);
        }

		for (int i = 0; i < centrosTotales; i++)
        {
			GameObject go = Instantiate(pf_barrasComunidad, graficaBarras.transform.Find("barras"));
			TextMeshProUGUI tCentro = go.transform.Find("Centro").Find("NOMBRE").GetComponent<TextMeshProUGUI>();

			#region Estado transmisores

			if (go.transform.Find("Estados").Find("Background").Find("Estados").TryGetComponent(out Transform container))
			{
				tCentro.text = centros[i].nombre;

				if(container.Find("Estado_4").TryGetComponent(out Image estado4))
				{

					switch (centros[i].estado_transmisor4)
					{
						case 0:
							estado4.color = colorOkA;
							break;
						case 1:
							estado4.color = colorWarningA;
							break;
						case 2:
							estado4.color = colorFaultA;
							break;
						case 3:
							estado4.color = colorAlarmaA;
							break;
						case 4:
							estado4.color = colorSinConexionA;
							break;
						case 10:
							estado4.color = new Color(0, 0, 0, 0);
							break;
						default:
							break;
					}

				}
				
				if(container.Find("Estado_3").TryGetComponent(out Image estado3))
				{

					switch (centros[i].estado_transmisor3)
					{
						case 0:
							estado3.color = colorOkA;
							break;
						case 1:
							estado3.color = colorWarningA;
							break;
						case 2:
							estado3.color = colorFaultA;
							break;
						case 3:
							estado3.color = colorAlarmaA;
							break;
						case 4:
							estado3.color = colorSinConexionA;
							break;
						case 10:
							estado3.color = new Color(0, 0, 0, 0);
							break;
						default:
							break;
					}
				}
				
				if(container.Find("Estado_2").TryGetComponent(out Image estado2))
				{
					switch (centros[i].estado_transmisor2)
					{
						case 0:
							estado2.color = colorOkA;
							break;
						case 1:
							estado2.color = colorWarningA;
							break;
						case 2:
							estado2.color = colorFaultA;
							break;
						case 3:
							estado2.color = colorAlarmaA;
							break;
						case 4:
							estado2.color = colorSinConexionA;
							break;
						case 10:
							estado2.color = new Color(0, 0, 0, 0);
							break;
						default:
							break;
					}
				}
				
				if(container.Find("Estado_1").TryGetComponent(out Image estado1))
				{
					switch (centros[i].estado_transmisor1)
					{
						case 0:
							estado1.color = colorOkA;
							break;
						case 1:
							estado1.color = colorWarningA;
							break;
						case 2:
							estado1.color = colorFaultA;
							break;
						case 3:
							estado1.color = colorAlarmaA;
							break;
						case 4:
							estado1.color = colorSinConexionA;
							break;
						case 10:
							estado1.color = new Color(0, 0, 0, 0);
							break;
						default:
							break;
					}
				}

				#endregion
			
			}
		}

		// =========================== CIRCULO

		var grafica = graficaCircularComunidad;

		FillGraficaCircular(centrosTotales, centros, grafica);

		// =========================== CUADRADOS

		var centrosFM = GlobalController.Instance.GetCentrosComunidadFMRNE(codigoComunidad);
		var centrosOM = GlobalController.Instance.GetCentrosComunidadOMRNE(codigoComunidad);
		var graficaCuadrados = graficaCuadradosComunidad;

		yield return FillGraficaCuadrados(centrosFM, centrosOM, graficaCuadrados);

		// =========================== FRECUENCIAS

		var graficaFrecuencias = graficaFrecuenciasComunidad;

		FillGraficaFrecuencias(centrosTotales, centrosFM, graficaFrecuencias);
	}
	public void UpdateHeader(string nombre, DateTime dia, DateTime hora) // Actualiza el header 
	{
		textoCentroTitulo.text = nombre;
		textoFecha.text = dia.ToString("yyyy/MM/dd");
		textoActualizacion.text = hora.ToString("HH:mm:ss");
    }
	public void UpdateBody(TorreBase torre, TipoTorre tipoTorre) // Actualiza el cuerpo de datos
	{
		//Debug.Log(torre.codigo_centro);
		if (torre.bbdd == "cellnex")
        {
			sinConexion.SetActive(true);
			TextMeshProUGUI tSinConexion = sinConexion.transform.Find("TEXTO_MODEM").GetComponent<TextMeshProUGUI>();
			tSinConexion.text = "Centro de Cellnex";
			
			modem.SetActive(false);
			transmisores.SetActive(false);
			receptor.SetActive(false);
			audios.SetActive(false);
			alarmasOM.SetActive(false);
			if (isPC)
            {
				transmisores.transform.parent.Find("Dial-Bing").Find("Dial").gameObject.SetActive(false);

				TextMeshProUGUI tCentro = infoBing.Find("NOMBRE").GetComponent<TextMeshProUGUI>();
				TextMeshProUGUI tProvincia = infoBing.Find("PROVINCIA").GetComponent<TextMeshProUGUI>();
				TextMeshProUGUI tComunidad = infoBing.Find("COMUNIDAD").GetComponent<TextMeshProUGUI>();

				tCentro.text = torre.nombre;
				tProvincia.text = torre.provincia;
				tComunidad.text = torre.comunidad;
			}
			FillPopup(torre);

			return;
		}
		modem.SetActive(true);
		transmisores.SetActive(true);
		receptor.SetActive(true);
		audios.SetActive(true);
		// Se lee cuántos transmisores y reservas tiene el centro para spawnearlos más adelante
		int nTransmisores = int.Parse(torre.tipo.Substring(0, 1));
		int nReservas = int.Parse(torre.tipo.Substring(1, 2));
		//int totalTransmisores = nTransmisores + nReservas;

		UpdateHeader(torre.nombre, torre.dia, torre.hora);
		//if (GlobalController.Instance.SelectedTower != null)
		//{
		//	UpdateHeader(torre.nombre, torre.dia, torre.hora);
		//}

		GameObject bbdd;

		// Si el modem está caido solo se muestra el rssi 
		if (torre.modem_estado == 4)
		{
			//FillRSSI<TorreBase>(torre);
			sinConexion.SetActive(true);
			TextMeshProUGUI tSinConexion = sinConexion.transform.Find("TEXTO_MODEM").GetComponent<TextMeshProUGUI>();
			tSinConexion.text = torre.texto_alarma;
			
			modem.GetComponent<CanvasGroup>().alpha = .2f;
			receptor.GetComponent<CanvasGroup>().alpha = .2f;
			audios.GetComponent<CanvasGroup>().alpha = .2f;
			alarmasOM.GetComponent<CanvasGroup>().alpha = .2f;
			transmisores.GetComponent<CanvasGroup>().alpha = .2f;

            if (isPC)
            {
				transmisores.transform.parent.Find("Dial-Bing").Find("Dial").GetComponent<CanvasGroup>().alpha = .2f;
				transmisores.transform.parent.Find("Dial-Bing").Find("Bing").GetComponent<CanvasGroup>().alpha = .2f;
            }
		}
        else
        {
			sinConexion.SetActive(false);
			modem.GetComponent<CanvasGroup>().alpha = 1;
			receptor.GetComponent<CanvasGroup>().alpha = 1;
			alarmasOM.GetComponent<CanvasGroup>().alpha = 1;
			// Aquí se rellena el contenido del canvas en función al tipo de torre que se seleccione. Se ha separado en los 3 paneles y cada uno tiene una función para rellenar una sección
			switch (tipoTorre)
			{
                case TipoTorre.OM:

                    TorreOM t0 = (TorreOM)torre;

                    FillModem<TorreOM>(t0);
                    FillRSSI<TorreOM>(t0);
					if (!isPC)
                    {
						frecuencias.transform.parent.parent.gameObject.SetActive(false);
                    }
                    else
                    {
						frecuencias.transform.parent.GetComponent<CanvasGroup>().alpha = .3f;
						frecuencias.SetActive(false);
						frecuencias.transform.parent.parent.gameObject.SetActive(true);
						frecuencias.transform.parent.parent.Find("sin_conexion").gameObject.SetActive(true);
					}

                    bbdd = bbddOM;
                    FillTransmisores0(bbdd, t0);
                    FillAlarmasOM(t0);

                    FillPopup(t0);

                    break;
                case TipoTorre.FM1:

					TorreFM1 t1 = (TorreFM1)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM1>(t1);
					FillFrecuencias(nTransmisores, t1);
					FillRSSI<TorreFM1>(t1);

					//PANEL CENTRAL
					bbdd = bbdd1_4_1;
					FillTransmisores1(nTransmisores, nReservas, t1, bbdd);

					//PANEL DERECHO
					// Si el estado es 4, significa que está sin conexión y rellena todo con null
					if (t1.receptor_estado != 4)
					{
						FillReceptor(t1);
						FillAudios(t1);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t1);

					break;

				case TipoTorre.FM2:

					TorreFM2 t2 = (TorreFM2)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM2>(t2);
					FillFrecuencias(nTransmisores, t2);
					FillRSSI<TorreFM2>(t2);

					//PANEL CENTRAL
					bbdd = bbdd2_4_1;
					FillTransmisores2(nTransmisores, nReservas, t2, bbdd);

					//PANEL DERECHO
					if (t2.receptor_estado != 4)
					{
						FillReceptor(t2);
						FillAudios(t2);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t2);

					break;

				case TipoTorre.FM3:

					TorreFM3 t3 = (TorreFM3)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM3>(t3);
					FillFrecuencias(nTransmisores, t3);
					FillRSSI<TorreFM3>(t3);

					//PANEL CENTRAL
					bbdd = bbdd3_4_1;
					FillTransmisores3(nTransmisores, nReservas, t3, bbdd);

					//PANEL DERECHO
					if (t3.receptor_estado != 4)
					{
						FillReceptor(t3);
						FillAudios(t3);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t3);

					break;

				case TipoTorre.FM4:

					TorreFM4 t4 = (TorreFM4)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM4>(t4);
					FillFrecuencias(nTransmisores, t4);
					FillRSSI<TorreFM3>(t4);

					//PANEL CENTRAL
					bbdd = bbdd4_4_0;
					FillTransmisores4(nTransmisores, nReservas, t4, bbdd);

					//PANEL DERECHO
					if (t4.receptor_estado != 4)
					{
						FillReceptor(t4);
						FillAudios(t4);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t4);

					break;

				case TipoTorre.FM5:

					TorreFM5 t5 = (TorreFM5)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM5>(t5);
                    FillFrecuencias(nTransmisores, t5);
                    FillRSSI<TorreFM5>(t5);

					//PANEL CENTRAL
					bbdd = bbdd5_4_1;
					FillTransmisores5(nTransmisores, nReservas, t5, bbdd);

					//PANEL DERECHO
					if (t5.receptor_estado != 4)
					{
						FillReceptor(t5);
						FillAudios(t5);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t5);

					break;

				case TipoTorre.FM6:

					TorreFM6 t6 = (TorreFM6)torre;

					//PANEL IZQUIERDO
					FillModem<TorreFM6>(t6);
					FillFrecuencias(nTransmisores, t6);
					FillRSSI<TorreFM6>(t6);

					//PANEL CENTRAL
					bbdd = bbdd6_4_1;
					FillTransmisores6(nTransmisores, nReservas, t6, bbdd);

					//PANEL DERECHO
					if (t6.receptor_estado != 4)
					{
						FillReceptor(t6);
						FillAudios(t6);
					}
					else
					{
						FillReceptorAudiosNull();
					}

					FillPopup(t6);

					break;

				default:
					break;

			}
		}
		
	}
	public IEnumerator UpdateInfoCentro(int codigoCentro)
    {
		isUpdating = true;
		StartCoroutine(rtveAPI.Instance.GetData(codigoCentro, false));

		if (rtveAPI.Instance.isRequesting)
		{
			yield return new WaitUntil(() => !rtveAPI.Instance.isRequesting);
		}

		TorreBase t = GlobalController.Instance.GetCentro(codigoCentro);

		TextMeshProUGUI tNombre = infoCentro.transform.Find("Content").Find("Centro").Find("NOMBRE_CENTRO").GetComponent<TextMeshProUGUI>();

		TextMeshProUGUI tModemNombre = infoCentro.transform.Find("Content").Find("Modem").Find("Nombre").Find("NOMBRE_MODEM").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tRssi = infoCentro.transform.Find("Content").Find("Modem").Find("RSSI").Find("RSSI_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tBitrateIN = infoCentro.transform.Find("Content").Find("Modem").Find("BitrateIN").Find("BITRATEIN_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tBitrateOUT = infoCentro.transform.Find("Content").Find("Modem").Find("BitrateOUT").Find("BITRATEOUT_DATO").GetComponent<TextMeshProUGUI>();

		TextMeshProUGUI tCodigoCentro = infoCentro.transform.Find("Content").Find("InfoGeneral").Find("CodigoCentro").Find("CODIGO_CENTRO_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tBbdd = infoCentro.transform.Find("Content").Find("InfoGeneral").Find("BaseDeDatos").Find("BBDD_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tConfiguracion = infoCentro.transform.Find("Content").Find("InfoGeneral").Find("Configuracion").Find("CONFIGURACION_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tEmision = infoCentro.transform.Find("Content").Find("InfoGeneral").Find("Emisión").Find("EMISION_DATO").GetComponent<TextMeshProUGUI>();

		tNombre.text = t.nombre;

		if (t.modem_estado == 4)
        {
			tModemNombre.text = t.texto_alarma;
			tRssi.text = "null";
			tBitrateIN.text = "null";
			tBitrateOUT.text = "null";
		}
        else
        {
			tModemNombre.text = t.modem_nombre;
			tRssi.text = t.modem_rssi;
			tBitrateIN.text = t.modem_consumo_in.ToString();
			tBitrateOUT.text = t.modem_consumo_out.ToString();
		}

		tCodigoCentro.text = t.codigo_centro.ToString();
		tBbdd.text = t.bbdd;
		tConfiguracion.text = t.tipo;
		tEmision.text = t.emision;

		Transform transmisores = infoCentro.transform.Find("Content").Find("Transmisores");

		if (t.frecuencia == "FM" && t.modem_estado != 4)
        {
			transmisores.gameObject.SetActive(true);

			Image iEstado1 = transmisores.Find("TX_1").Find("TX_ESTADO").GetComponent<Image>();
			Image iEstado2 = transmisores.Find("TX_2").Find("TX_ESTADO").GetComponent<Image>();
			Image iEstado3 = transmisores.Find("TX_3").Find("TX_ESTADO").GetComponent<Image>();
			Image iEstado4 = transmisores.Find("TX_4").Find("TX_ESTADO").GetComponent<Image>();
			Image iEstadoR = transmisores.Find("TX_R").Find("TX_ESTADO").GetComponent<Image>();

			List<Image> iEstados = new List<Image>() { iEstado1, iEstado2, iEstado3, iEstado4, iEstadoR };

			int nTransmisores = int.Parse(t.tipo.Substring(0, 1));
			int nReservas = int.Parse(t.tipo.Substring(1, 2));

			RectTransform dimTransmisores = transmisores.GetComponent<RectTransform>();
			dimTransmisores.sizeDelta = new Vector2(dimTransmisores.sizeDelta.x, 30 * (nTransmisores + nReservas));

			List<int> estado = GlobalController.Instance.GetEstadosTransmisores(codigoCentro);

			for (int i = 0; i < transmisores.childCount - 1; i++)
			{
				if (i < nTransmisores)
				{
					transmisores.Find($"TX_{i + 1}").gameObject.SetActive(true);
				}
				else
				{
					transmisores.Find($"TX_{i + 1}").gameObject.SetActive(false);
				}
			}
			if (nReservas > 0)
			{
				transmisores.Find($"TX_R").gameObject.SetActive(true);
				iEstados[4].color = SetColor(estado[0]);
			}
			else
			{
				transmisores.Find($"TX_R").gameObject.SetActive(false);
			}

			for (int i = 0; i < nTransmisores; i++)
			{
				iEstados[i].color = SetColor(estado[i + 1]);
			}
        }
        else
        {
			transmisores.gameObject.SetActive(false);
		}

		//foreach (var a in GlobalController.Instance.GetEstadosTransmisores(codigoCentro, nTransmisores))
		//      {
		//	Debug.Log(a);
		//      }

		Transform weather = infoCentro.transform.Find("Content").Find("Weather");

		TextMeshProUGUI tTemperatura = weather.Find("TEMPERATURA").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tHumedad = weather.Find("HUMEDAD").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tViento = weather.Find("VIENTO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tCota = weather.Find("COTA").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tDescripcion = weather.Find("WEATHER").Find("DESCRIPCION").GetComponent<TextMeshProUGUI>();
		Image iWeather = weather.Find("WEATHER").GetComponent<Image>();

		Weather weatherAPI = WeatherAPI.Instance.GetWeather(t.latLon.LatitudeInDegrees, t.latLon.LongitudeInDegrees);

		tTemperatura.text = weatherAPI.temperature.ToString("00.0") + " ºC";
		tHumedad.text = weatherAPI.humidity.ToString() + " %";
		tViento.text = weatherAPI.wind.ToString() + " m/s";
		tCota.text = t.cota.ToString() + " m";
		tDescripcion.text = weatherAPI.description;

		string iconCode = weatherAPI.icon;
		iWeather.sprite = Resources.Load($"weather_icons/{iconCode}", typeof(Sprite)) as Sprite;
		iWeather.sprite = Resources.Load($"weather_icons/{iconCode}", typeof(Sprite)) as Sprite;

		infoCentro.SetActive(true);
		isUpdating = false;
	}

	public IEnumerator HideInfoCentro()
    {
		if (isUpdating)
		{
			yield return new WaitUntil(() => !isUpdating);
		}

		infoCentro.SetActive(false);
	}


	public void UpdatePanelHover(int codigoComunidad)
    {
		TextMeshProUGUI tNombre = panelHover.transform.Find("Cabecera").Find("NOMBRE_COMUNIDAD").GetComponent<TextMeshProUGUI>();
		tNombre.text = GlobalController.Instance.GetNombreComunidad(codigoComunidad);

		var centros = GlobalController.Instance.GetCentrosComunidadRNE(codigoComunidad);

		int[] estadosTotales = GetEstados(centros);

		for (int i = 0; i < panelHover.transform.Find("Datos").childCount; i++)
        {
			TextMeshProUGUI nCentros = panelHover.transform.Find("Datos").GetChild(i).Find("N_CENTROS").GetComponent<TextMeshProUGUI>();
			nCentros.text = estadosTotales[i].ToString();
		}
    }

	public void UpdatePanelHover_VR(TorreBase torre,Transform globo)
	{
		TextMeshProUGUI textoCentro = globo.Find("Content").Find("bg").Find("Datos").Find("Text_NombreTorre").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI textoCodigoCentro = globo.Find("Content").Find("bg").Find("Datos").Find("Text_NombreTorre").Find("Text_NumeroCentro").GetComponent<TextMeshProUGUI>();

		textoCentro.text = torre.nombre;
		//textoCodigoCentro.text = "Codigo centro nº" + torre.codigo_centro.ToString();
		textoCodigoCentro.text = "";
	}
	#endregion

	#region Funciones Fill
	private void FillPopup(TorreBase t)
	{
		if (!isPC)
        {
			panelCentro.SetActive(true);

			GameObject go = panelCentro.transform.Find("Content").gameObject;

			TextMeshProUGUI tNombre = go.transform.Find("NOMBRE").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tComunidad = go.transform.Find("COMUNIDAD").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tProvincia = go.transform.Find("PROVINCIA").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tTemperatura = go.transform.Find("TEMPERATURA").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tHumedad = go.transform.Find("HUMEDAD").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tViento = go.transform.Find("VIENTO").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tCota = go.transform.Find("COTA").GetComponent<TextMeshProUGUI>();
			Image iWeather = go.transform.Find("WEATHER").GetComponent<Image>();

			Weather weather = WeatherAPI.Instance.GetWeather(t.latLon.LatitudeInDegrees, t.latLon.LongitudeInDegrees);

			tNombre.text = t.nombre;
			tComunidad.text = t.comunidad;
			tProvincia.text = t.provincia;
			tTemperatura.text = weather.temperature.ToString("00.0") + " ºC";
			tHumedad.text = weather.humidity.ToString() + " %";
			tViento.text = weather.wind.ToString() + " m/s";
			tCota.text = t.cota.ToString() + " m";

			string iconCode = weather.icon;
			iWeather.sprite = Resources.Load($"weather_icons/{iconCode}", typeof(Sprite)) as Sprite;
		}
	}
	private void FillGraficaFrecuencias(int centrosTotales, List<TorreBase> centros, GameObject graficaFrecuancias)
	{
		float r1 = 0;
		float r2 = 0;
		float r3 = 0;
		float r4 = 0;

		float t1 = 0;
		float t2 = 0;
		float t3 = 0;
		float t4 = 0;

		foreach (var c in centros)
        {
			if(c.estado_transmisor1 != 10)
            {
				t1 += 1;
            }
			if (c.estado_transmisor2 != 10)
			{
				t2 += 1;
			}
			if (c.estado_transmisor3 != 10)
			{
				t3 += 1;
			}
			if (c.estado_transmisor4 != 10)
			{
				t4 += 1;
			}
		}

		foreach (var c in centros)
		{
			if (c.estado_transmisor1 == 0 || c.estado_transmisor1 == 1 || c.estado_transmisor1 == 2)
			{
				r1 += 1;
			}
			if (c.estado_transmisor2 == 0 || c.estado_transmisor2 == 1 || c.estado_transmisor2 == 2)
			{
				r2 += 1;
			}
			if (c.estado_transmisor3 == 0 || c.estado_transmisor3 == 1 || c.estado_transmisor3 == 2)
			{
				r3 += 1;
			}
			if (c.estado_transmisor4 == 0 || c.estado_transmisor4 == 1 || c.estado_transmisor4 == 2)
			{
				r4 += 1;
			}
		}

		float[] frecuenciasPercent = new float[4] { r1 / t1, r2 / t2, r3 / t3, r4 / t4 };

		//Debug.Log($"{r1}: {t1}");
		//Debug.Log($"{r2}: {t2}");
		//Debug.Log($"{r3}: {t3}");
		//Debug.Log($"{r4}: {t4}");

		for (int i = 0; i < graficaFrecuancias.transform.Find("Content").childCount; i++)
		{
			TextMeshProUGUI tFrecuencias = graficaFrecuancias.transform.Find("Content").GetChild(i).Find("porcentaje").GetComponent<TextMeshProUGUI>();
			Image bFrecuencias = graficaFrecuancias.transform.Find("Content").GetChild(i).Find("barra").Find("fill_mask").Find("Image").GetComponent<Image>();

			//StartCoroutine(TxtAnim(tFrecuencias, (frecuenciasPercent[i] * 100).ToString()));
			LeanTween.value(gameObject, 0, (frecuenciasPercent[i] * 100), animationSpeed)
				.setOnUpdate((x) => SetTextDec1(x, tFrecuencias));

			//StartCoroutine(FillBar(bFrecuencias, frecuenciasPercent[i]));
			LeanTween.value(gameObject, 0, frecuenciasPercent[i], animationSpeed)
				.setOnUpdate((x) => SetBar(x, bFrecuencias));
		}
	}
	private IEnumerator FillGraficaCuadrados(List<TorreBase> centrosFM, List<TorreBase> centrosOM, GameObject graficaCuadrados)
	{
		GameObject fm = graficaCuadrados.transform.Find("Content").Find("FM").gameObject;
		GameObject om = graficaCuadrados.transform.Find("Content").Find("OM").gameObject;

		for (int i = 0; i < fm.transform.Find("Cuadrados").childCount; i++)
		{
            Destroy(fm.transform.Find("Cuadrados").GetChild(i).gameObject);
		}
		for (int i = 0; i < om.transform.Find("Cuadrados").childCount; i++)
		{
			Destroy(om.transform.Find("Cuadrados").GetChild(i).gameObject);
		}

		yield return new WaitWhile(() => (fm.transform.Find("Cuadrados").childCount > 0 || om.transform.Find("Cuadrados").childCount > 0));

		int centrosTotalesFM = centrosFM.Count;
		int centrosTotalesOM = centrosOM.Count;

		TextMeshProUGUI tCentrosCuadradosFM = graficaCuadrados.transform.Find("Content").Find("FM").Find("Footer").Find("Text_title").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tCentrosCuadradosOM = graficaCuadrados.transform.Find("Content").Find("OM").Find("Footer").Find("Text_title").GetComponent<TextMeshProUGUI>();

		//StartCoroutine(TxtAnim(tCentrosCuadradosFM, centrosTotalesFM.ToString()));
		//StartCoroutine(TxtAnim(tCentrosCuadradosOM, centrosTotalesOM.ToString()));
		LeanTween.value(gameObject, 0, centrosTotalesFM, animationSpeed)
				.setOnUpdate((x) => SetText(x, tCentrosCuadradosFM));
		LeanTween.value(gameObject, 0, centrosTotalesOM, animationSpeed)
				.setOnUpdate((x) => SetText(x, tCentrosCuadradosOM));

		Color[] arrColor = new Color[5] { colorOkA, colorWarningA, colorFaultA, colorAlarmaA, colorSinConexionA };

		int warningTotalFM = 0;
		int faultTotalFM = 0;
		int alarmaTotalFM = 0;
		int okTotalFM = 0;
		int sinConexionTotalFM = 0;

		foreach (var c in centrosFM)
		{
			switch (c.estado)
			{
				case 0:
					okTotalFM += 1;
					break;
				case 1:
					warningTotalFM += 1;
					break;
				case 2:
					faultTotalFM += 1;
					break;
				case 3:
					alarmaTotalFM += 1;
					break;
				case 4:
					sinConexionTotalFM += 1;
					break;
				default:
					break;
			}
		}

		int[] estadosTotalesFM = new int[5] { okTotalFM, warningTotalFM, faultTotalFM, alarmaTotalFM, sinConexionTotalFM };

		for (int i = 0; i < estadosTotalesFM.Length; i++)
		{
			for (int j = 0; j < estadosTotalesFM[i]; j++)
			{
				GameObject go = Instantiate(cuadrado, fm.transform.Find("Cuadrados").transform);
				go.GetComponent<Image>().color = arrColor[i];
				go.SetActive(false);
			}
		}
		Transform cuadradosFM = fm.transform.Find("Cuadrados");
		int numCuadradosFM = cuadradosFM.childCount;
		//Debug.Log(cuadradosFM.childCount);
		//StartCoroutine(AnimCuadrados(fm.transform.Find("Cuadrados")));
		if (numCuadradosFM > 0)
        {
			LeanTween.value(gameObject, 0, numCuadradosFM - 1, animationSpeed)
				.setOnUpdate((x) => SetCuadrados(x, cuadradosFM));
		}
			

        int warningTotalOM = 0;
		int faultTotalOM = 0;
		int alarmaTotalOM = 0;
		int okTotalOM = 0;
		int sinConexionTotalOM = 0;

		foreach (var c in centrosOM)
		{
			switch (c.estado)
			{
				case 0:
					okTotalOM += 1;
					break;
				case 1:
					warningTotalOM += 1;
					break;
				case 2:
					faultTotalOM += 1;
					break;
				case 3:
					alarmaTotalOM += 1;
					break;
				case 4:
					sinConexionTotalOM += 1;
					break;
				default:
					break;
			}
		}

		int[] estadosTotalesOM = new int[5] { okTotalOM, warningTotalOM, faultTotalOM, alarmaTotalOM, sinConexionTotalOM };

		for (int i = 0; i < estadosTotalesOM.Length; i++)
		{
			for (int j = 0; j < estadosTotalesOM[i]; j++)
			{
				GameObject go = Instantiate(cuadrado, om.transform.Find("Cuadrados").transform);
				go.GetComponent<Image>().color = arrColor[i];
				go.SetActive(false);
			}
		}
		Transform cuadradosOM = om.transform.Find("Cuadrados");
		int numCuadradosOM = cuadradosOM.childCount;
		//Debug.Log(numCuadradosFM);
		//StartCoroutine(AnimCuadrados(om.transform.Find("Cuadrados")));
		if (numCuadradosOM > 0)
        {
			LeanTween.value(gameObject, 0, numCuadradosOM - 1, animationSpeed)
			.setOnUpdate((x) => SetCuadrados(x, cuadradosOM));
		}
	}
	private void FillGraficaCircular(int centrosTotales, List<TorreBase> centros, GameObject grafica)
    {
        TextMeshProUGUI tCentrosCirculo = grafica.transform.Find("Content").Find("NºTOTAL_CENTROS_DATO").GetComponent<TextMeshProUGUI>();

        //StartCoroutine(TxtAnim(tCentrosCirculo, centrosTotales.ToString()));
        LeanTween.value(gameObject, 0, centrosTotales, animationSpeed)
                .setOnUpdate((x) => SetText(x, tCentrosCirculo));

        int[] estadosTotales = GetEstados(centros);

        // Itera por los estados ok-SC
        for (int i = 0; i < 5; i++)
        {
            Image fillCirculo = grafica.transform.Find("Content").GetChild(i).Find("FILL").GetComponent<Image>();
            TextMeshProUGUI tCirculo = grafica.transform.Find("Content").GetChild(i).GetChild(2).GetComponent<TextMeshProUGUI>();

            float valorEstado = InterpolateValue(estadosTotales[i].ToString(), 0, centrosTotales, 0, 1);

            //StartCoroutine(FillBar(fillCirculo, valorEstado));
            //StartCoroutine(TxtAnim(tCirculo, estadosTotales[i].ToString()));
            LeanTween.value(gameObject, 0, valorEstado, animationSpeed)
                .setOnUpdate((x) => SetBar(x, fillCirculo));
            LeanTween.value(gameObject, 0, estadosTotales[i], animationSpeed)
                .setOnUpdate((x) => SetText(x, tCirculo));
        }
    }
    public int[] GetEstados(List<TorreBase> centros)
    {
        int warningTotal = 0;
        int faultTotal = 0;
        int alarmaTotal = 0;
        int okTotal = 0;
        int sinConexionTotal = 0;

        foreach (var c in centros)
        {
            switch (c.estado)
            {
                // Ok
                case 0:
                    okTotal += 1;
                    break;
                // Warning
                case 1:
                    warningTotal += 1;
                    break;
                // Fault
                case 2:
                    faultTotal += 1;
                    break;
                // Alarma
                case 3:
                    alarmaTotal += 1;
                    break;
                // Sin conexión
                case 4:
                    sinConexionTotal += 1;
                    break;
                default:
                    break;
            }
        }

        int[] estadosTotales = new int[5]{
                    okTotal,
                    warningTotal,
                    faultTotal,
                    alarmaTotal,
                    sinConexionTotal,
        };
        return estadosTotales;
    }

    // PANEL IZQUIERDO
    // Rellena los campos de modem: nombre y bitrate

    private void FillModem<T>(TorreBase t) where T : new()
	{
		modemNombre.text = t.modem_nombre;

		LeanTween.value(gameObject, float.Parse(modemBitRateIn.text), t.modem_consumo_in, animationSpeed)
			.setOnUpdate((x) => SetText(x, modemBitRateIn));
		LeanTween.value(gameObject, float.Parse(modemBitRateOut.text), t.modem_consumo_out, animationSpeed)
			.setOnUpdate((x) => SetText(x, modemBitRateOut));

		if (isPC)
        {
			TextMeshProUGUI tCentro = infoBing.Find("NOMBRE").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tProvincia = infoBing.Find("PROVINCIA").GetComponent<TextMeshProUGUI>();
			TextMeshProUGUI tComunidad = infoBing.Find("COMUNIDAD").GetComponent<TextMeshProUGUI>();

			tCentro.text = $"Centro {t.codigo_centro}";
			tProvincia.text = t.provincia;
			tComunidad.text = t.comunidad;
		}
	}
    // Funcionas que colocan las frecuencias en el dial del panel del modem
    private void FillFrecuencias(int nTransmisores, TorreFM t)
    {
		frecuencias.SetActive(true);
		frecuencias.transform.parent.parent.gameObject.SetActive(true);

		string frecuenciaKey = "";
		if (t.bbdd == "bbdd_5")
        {
			frecuenciaKey = "ex1";
        }

		if (isPC)
        {
			frecuencias.transform.parent.GetComponent<CanvasGroup>().alpha = 1;
			frecuencias.transform.parent.parent.Find("sin_conexion").gameObject.SetActive(false);

			transmisores.transform.parent.Find("Dial-Bing").Find("Dial").GetComponent<CanvasGroup>().alpha = 1;
			transmisores.transform.parent.Find("Dial-Bing").Find("Bing").GetComponent<CanvasGroup>().alpha = 1;
		}

		for (int i = 0; i < frecuencias.transform.childCount; i++)
        {
			Destroy(frecuencias.transform.GetChild(i).gameObject);
        }

		float frecuenciaValor;
		GameObject fr = null;
		bool isNear = false;
		float height = 0;

		List<float> frecList = new List<float>();
		// Itera por los transmisores
		for (int i = 0; i < nTransmisores; i++)
        {
			// Si un transmisor está caído, se coloca con valor null en el principio del dial
            if (int.Parse(t.GetType().GetField($"tx{i + 1}_estado").GetValue(t).ToString()) == 4)
            {
                //fr = Instantiate(FRbajo, frecuencias.transform);
                //fr.transform.Find("Content").Find("FRECUENCIA").GetComponent<TextMeshProUGUI>().text = "null";
                //fr.transform.localPosition = new Vector3(-283, fr.transform.localPosition.y, fr.transform.localPosition.z);
            }
			// Para el resto de transmisores que están activos
			else
			{
				string frecuenciaTexto = $"FR{i + 1}";

				if (t.GetType().GetField($"tx{i + 1}_estado").GetValue(t).ToString() == "4")
                {
					frecuenciaValor = 0;
                }
                else
                {
					frecuenciaValor = float.Parse(t.GetType().GetField($"tx{i + 1}_frecuencia{frecuenciaKey}").GetValue(t).ToString().Split(' ')[0]);
				}
				
				if (frecuenciaValor == 0)
				{
					frecuenciaValor = float.Parse(t.GetType().GetField($"txr_frecuencia{frecuenciaKey}").GetValue(t).ToString().Split(' ')[0]);
					frecuenciaTexto = "Reserva";
				}
				if (frecuenciaValor > 1000)
				{
					frecuenciaValor = frecuenciaValor / 100;
				}

				foreach (var f in frecList)
                {
                    if (Mathf.Abs(frecuenciaValor - f) < 2.6)
                    {
						fr = Instantiate(FRalto, frecuencias.transform);
						height = fr.transform.localPosition.y;
						if (isPC)
                        {
							height = 28;
						}
						isNear = true;
						break;
                    }
				}

				if (isNear == true)
                {
					//frecList.Clear();
					isNear = false;
				}
                else
                {
					frecList.Add(frecuenciaValor);
					fr = Instantiate(FRbajo, frecuencias.transform);
					height = fr.transform.localPosition.y;
					if (isPC)
					{
						height = -45;
					}
				}
				float maxDim, minDim;
				if (isPC)
                {
					maxDim = 273;
					minDim = -277;
                }
                else
                {
					maxDim = 279;
					minDim = -283;
				}

				fr.transform.Find("Content").Find("FRECUENCIA").GetComponent<TextMeshProUGUI>().text = frecuenciaValor.ToString("00.0", CultureInfo.InvariantCulture);
				fr.transform.Find("Content").Find("text").GetComponent<TextMeshProUGUI>().text = frecuenciaTexto;
				float scaledFR = InterpolateValue(frecuenciaValor.ToString(), 85, 110, minDim, maxDim);

				fr.transform.localPosition = new Vector3(scaledFR, height, fr.transform.localPosition.z);
			}
        }
    }
    //Función para rellenar el campo del RSSI y colocar la flecha en su sitio
    private void FillRSSI<T>(TorreBase t)
	{
		LeanTween.value(gameObject, float.Parse(modemRSSIValor.text), float.Parse(t.modem_rssi), animationSpeed)
			.setOnUpdate((x) => SetText(x, modemRSSIValor));

		float scaledRssi = InterpolateValue(t.modem_rssi, -180, 180, 90, -90);
		
		LeanTween.value(gameObject, RSSIFlecha.transform.eulerAngles.z, scaledRssi, animationSpeed)
			.setOnUpdate((x) => SetGauge(x, RSSIFlecha));

		if(float.Parse(t.modem_rssi) > -51f || float.Parse(t.modem_rssi) < -130f)
		{
			modemRSSIValor.color = Color.red;
		}
		else
		{
			modemRSSIValor.color = Color.white;
		}
	}

	// PANEL CENTRAL
	private void FillTransmisores0(GameObject bbdd, TorreOM t0)
	{
		HideBbdds(bbdd);

		HideTransmisores(bbdd);

		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		GameObject tx1 = bbdd.transform.Find("Transmisores").Find("TX_1").gameObject;
		GameObject tx2 = bbdd.transform.Find("Transmisores").Find("TX_2").gameObject;
		GameObject tx3 = bbdd.transform.Find("Transmisores").Find("TX_3").gameObject;
		GameObject tx4 = bbdd.transform.Find("Transmisores").Find("TX_4").gameObject;

		int e1 = int.Parse(t0.tipo[0].ToString());
		int e2 = int.Parse(t0.tipo[1].ToString());
		int e3 = int.Parse(t0.tipo[2].ToString());
		int e4 = int.Parse(t0.tipo[3].ToString());

		if(e1 > 0 && e3 > 0)
        {
			GameObject separador = tx3.transform.parent.GetChild(tx3.transform.GetSiblingIndex() - 1).gameObject;
			separador.SetActive(true);
		}

		switch (e1)
        {
			case 0:

				tx1.SetActive(false);
				tx2.SetActive(false);

				break;
			case 1:

				tx1.SetActive(true);

				FillTransmisor0(tx1, t0, "tx1");

				if (e2 == 1)
				{
					tx2.SetActive(true);

					FillTransmisor0(tx2, t0, "tx2");

					tx2.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
					tx2.transform.Find("marco").Find("codo").gameObject.SetActive(false);

					GameObject separador = tx2.transform.parent.GetChild(tx2.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(true);
				}

				tx1.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
				tx1.transform.Find("marco").Find("codo").gameObject.SetActive(false);

				tx2.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
				tx2.transform.Find("marco").Find("codo").gameObject.SetActive(false);

				break;

			case 2:

				tx1.SetActive(true);

				FillTransmisor0(tx1, t0, "tx1");

				if (e2 == 2)
				{
					tx2.SetActive(true);

					FillTransmisor0(tx2, t0, "tx2");

					GameObject separador = tx2.transform.parent.GetChild(tx2.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(true);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(false);
				}

				tx1.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx1.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				tx2.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx2.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				break;

			case 3:

				tx1.SetActive(true);

				FillTransmisor0(tx1, t0, "tx1");

				if (e2 == 3)
                {
					tx2.SetActive(true);

					FillTransmisor0(tx2, t0, "tx2");

					GameObject separador = tx2.transform.parent.GetChild(tx2.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(true);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(false);
				}

				tx1.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx1.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				tx2.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx2.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				break;
		}

		switch (e3)
		{
			case 0:

				tx3.SetActive(false);
				tx4.SetActive(false);

				break;
			case 1:

				tx3.SetActive(true);

				FillTransmisor0(tx3, t0, "tx3");

				if (e4 == 1)
				{
					tx4.SetActive(true);

					FillTransmisor0(tx4, t0, "tx4");

					tx4.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
					tx4.transform.Find("marco").Find("codo").gameObject.SetActive(false);

					GameObject separador = tx4.transform.parent.GetChild(tx4.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(true);
				}

				tx3.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
				tx3.transform.Find("marco").Find("codo").gameObject.SetActive(false);

				tx4.transform.Find("marco").Find("Image_top").gameObject.SetActive(true);
				tx4.transform.Find("marco").Find("codo").gameObject.SetActive(false);

				break;

			case 2:

				tx3.SetActive(true);

				FillTransmisor0(tx3, t0, "tx3");

				if (e4 == 2)
				{
					tx4.SetActive(true);

					FillTransmisor0(tx4, t0, "tx4");

					GameObject separador = tx4.transform.parent.GetChild(tx4.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(true);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(false);
				}

				tx3.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx3.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				tx4.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx4.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				break;

			case 3:

				tx3.SetActive(true);

				FillTransmisor0(tx3, t0, "tx3");

				if (e4 == 3)
				{
					tx4.SetActive(true);

					FillTransmisor0(tx4, t0, "tx4");

					GameObject separador = tx4.transform.parent.GetChild(tx4.transform.GetSiblingIndex() - 1).gameObject;

					separador.SetActive(true);

					separador.transform.Find("Content").Find("uca").gameObject.SetActive(true);
					separador.transform.Find("Content").Find("suma").gameObject.SetActive(false);
					separador.transform.Find("Content").Find("nada").gameObject.SetActive(false);
				}

				tx3.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx3.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				tx4.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);
				tx4.transform.Find("marco").Find("codo").gameObject.SetActive(true);

				break;
		}
	}
    private void FillTransmisores1(int nTransmisores, int nReservas, TorreFM1 t, GameObject bbdd)
    {
        HideBbdds(bbdd);

        // oculta los transmisores, por si se han dejado encendidos en la escena
        HideTransmisores(bbdd);

		transmisores.GetComponent<CanvasGroup>().alpha = 1;

        //En los tipo 1 puede haber o no haber transmisor, se hace la comprobación y se rellena en el caso de que haya
        if (nReservas > 0)
        {
            GameObject tx = bbdd.transform.Find("Transmisores").Find("TX_R").gameObject;
            tx.SetActive(true);

            // si el transmisor está sin conexión se nullea, si no se rellena normal
            if (t.txr_estado == 4)
            {
                FillTransmisorNull(tx);
            }
            else
            {
                FillTransmisor1(tx, t, "txr");
            }

            // se rellena la parte uca correspondiente al transmisor
            GameObject uca = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find("TX_R").gameObject;
            uca.SetActive(true);
            FillUca1(uca, t, "txr");

            // si el transmisor no está caído y está en uso se canvia la opacidad
            string reservaOn = t.txr_reservaon;
            if (reservaOn == "1")
            {
                tx.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
            }
            else
            {
                tx.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
            }

            // Se colorea en función al estado ok, warning, fault, alarma
            int estado = t.txr_estado;
            ColorizeTransmisores(tx, estado);

            // La uca solo aparece cuando hay reserva, así que se rellenan los modos que son comunes a todos los transmisores desde aquí
            FillModosUCA1(t, bbdd);
        }
        else
        {
            // Si no hay reserva no hay uca
            bbdd.transform.Find("UCA").gameObject.SetActive(false);
        }

        // Iteramos por los transmisores que no son reserva
        for (int i = 1; i <= nTransmisores; i++)
        {
            GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
            tx.SetActive(true);

            // muestra también la separación entre los transmisores
            GameObject separacion = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
            separacion.SetActive(true);

            string nombreTransmisor = $"tx{i}";

            // si el transmisor está sin conexión se nullea, si no se rellena normal
            if (int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString()) == 4)
            {
                FillTransmisorNull(tx);
            }
            else
            {
                FillTransmisor1(tx, t, nombreTransmisor);
            }

            // Si hay reserva, se rellena los valores de uca de cada transmisor
            if (nReservas > 0)
            {
                bbdd.transform.Find("UCA").gameObject.SetActive(true);
                GameObject uca = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find($"TX_{i}").gameObject;
                uca.SetActive(true);
                GameObject barraUCA = uca.transform.parent.GetChild(uca.transform.GetSiblingIndex() - 1).gameObject;
                barraUCA.SetActive(true);
                FillUca1(uca, t, nombreTransmisor);

                // Si hay un transmisor en warning, fault o alarma, se colorea el marco/fondo/icono
                int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
                ColorizeTransmisores(tx, estado);
            }
            // Si no hay reserva, se desactivan las partes de la uca
            else
            {
                GameObject icon = tx.transform.Find("marco").Find("Image_top_reserva").gameObject;
                icon.SetActive(true);
                tx.transform.Find("marco").Find("Image_top").gameObject.SetActive(false);

                GameObject carga = bbdd.transform.Find("Transmisores").Find("Carga").gameObject;
                carga.SetActive(false);

                GameObject barra_marco = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).Find("marco").gameObject;
                barra_marco.SetActive(false);
            }
        }
    }
    private void FillTransmisores2(int nTransmisores, int nReservas, TorreFM2 t, GameObject bbdd)
	{
		HideBbdds(bbdd);

		// oculta los transmisores, por si se han dejado encendidos en la escena
		HideTransmisores(bbdd);

		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		// En las bbdd2 siempre hay reserva
		GameObject txr = bbdd.transform.Find("Transmisores").Find("TX_R").gameObject;
		txr.SetActive(true);

		// si el transmisor está sin conexión se nullea, si no se rellena normal
		if (t.txr_estado == 4)
		{
			FillTransmisorNull(txr);
		}
		else
		{
			FillTransmisor2(txr, t, "txr");

			// si hay potencia en la reserva, esta activa
			if (float.Parse(t.txr_potenciadirecta) > 0)
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
			}
			else
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
			}

			// Se colorea en función al estado ok, warning, fault, alarma
			int estadoReserva = t.txr_estado;
			ColorizeTransmisores(txr, estadoReserva);
		}

		// se rellena la parte uca correspondiente al transmisor
		GameObject ucar = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find("TX_R").gameObject;
		ucar.SetActive(true);
		FillUca2(ucar, t, "txr");

		// Se rellenan los modos de la UCA
		FillModosUCA2(t, bbdd);

		// Iteramos por los transmisores que no son reserva
		for (int i = 1; i <= nTransmisores; i++)
		{
			GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
			tx.SetActive(true);

			// muestra también la separación entre los transmisores
			GameObject barra = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
			barra.SetActive(true);

			string nombreTransmisor = $"tx{i}";

			// si el transmisor está sin conexión se nullea, si no se rellena normal
			if (int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString()) == 4)
			{
				FillTransmisorNull(tx);
			}
			else
			{
				FillTransmisor2(tx, t, nombreTransmisor);
			}

			// se rellena los valores de uca de cada transmisor
			GameObject uca = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find($"TX_{i}").gameObject;
			uca.SetActive(true);
			GameObject barraUCA = uca.transform.parent.GetChild(uca.transform.GetSiblingIndex() - 1).gameObject;
			barraUCA.SetActive(true);
			FillUca2(uca, t, nombreTransmisor);

			// Si hay un transmisor en warning, fault o alarma, se colorea el marco/fondo/icono
			int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
			ColorizeTransmisores(tx, estado);
		}
	}
	private void FillTransmisores3(int nTransmisores, int nReservas, TorreFM3 t, GameObject bbdd)
	{
		HideBbdds(bbdd);
		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		// Siempre hay reserva en bbdd3
		GameObject txr = bbdd.transform.Find("Transmisores").Find("TX_R").gameObject;
		txr.SetActive(true);

		// si el transmisor está sin conexión se nullea, si no se rellena normal
		if (t.txr_estado == 4)
		{
			FillTransmisorNull(txr);
		}
		else
		{
			FillTransmisor3(txr, t, "txr");

			// si hay potencia en la reserva, esta activa
			if (float.Parse(Regex.Replace(t.txr_potenciadirecta, @"\s+", "").Split('W')[0]) > 0)
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
			}
			else
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
			}

			// Se colorea en función al estado ok, warning, fault, alarma
			int estadoReserva = t.txr_estado;
			ColorizeTransmisores(txr, estadoReserva);
		}

		// Iteramos por los transmisores
		for (int i = 1; i <= nTransmisores; i++)
		{
			GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
			tx.SetActive(true);

			// muestra también la separación entre los transmisores
			GameObject barra = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
			barra.SetActive(true);

			string nombreTransmisor = $"tx{i}";
			

			// si el transmisor está sin conexión se nullea, si no se rellena normal
			if (int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString()) == 4) //t.GetType().GetField($"tx{i}_frecuencia").GetValue(t) is null
			{
				FillTransmisorNull(tx);
			}
			else
			{
				FillTransmisor3(tx, t, nombreTransmisor);
			}

			// Si hay un transmisor en warning, fault o alarma, se colorea el marco/fondo/icono
			int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
			ColorizeTransmisores(tx, estado);
		}
	}
	private void FillTransmisores4(int nTransmisores, int nReservas, TorreFM4 t, GameObject bbdd)
	{
		HideBbdds(bbdd);
		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		// Los bbdd4 no tienen reserva, iteramos directamente por los transmisores
		for (int i = 1; i <= nTransmisores; i++)
		{
			GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
			tx.SetActive(true);

			// muestra también la separación entre los transmisores
			GameObject barra = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
			barra.SetActive(true);

			string nombreTransmisor = $"tx{i}";

			// Si el transmisor está sin conexión
			if (int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString()) == 4) //t.GetType().GetField($"tx{i}_frecuencia").GetValue(t).ToString() is null
			{
				FillTransmisorNull(tx);
			}
			else
			{
				FillTransmisor4(tx, t, nombreTransmisor);
			}

			int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
			ColorizeTransmisores(tx, estado);
		}
	}
	private void FillTransmisores5(int nTransmisores, int nReservas, TorreFM5 t, GameObject bbdd)
	{
		HideBbdds(bbdd);

		// oculta los transmisores, por si se han dejado encendidos en la escena
		HideTransmisores(bbdd);

		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		// si el transmisor está sin conexión se nullea, si no se rellena normal
		GameObject txr = bbdd.transform.Find("Transmisores").Find("TX_R").gameObject;
		txr.SetActive(true);

		if (t.txr_potenciadirecta is null)
		{
			FillTransmisorNull(txr);
		}
		else
		{
			FillTransmisor5(txr, t, "txr");
			// si hay potencia en la reserva, esta activa
			if (float.Parse(t.txr_potenciadirecta) > 0)
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
			}
			else
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
			}
		}

		GameObject ucar = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find("TX_R").gameObject;
		ucar.SetActive(true);
		FillUca5(ucar, t, "txr");

		int estadoReserva = t.txr_estado;
		ColorizeTransmisores(txr, estadoReserva);

		FillModosUCA5(t, bbdd);

		for (int i = 1; i <= nTransmisores; i++)
		{
			GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
			tx.SetActive(true);

			GameObject barra = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
			barra.SetActive(true);

			string nombreTransmisor = $"tx{i}";

			if (t.GetType().GetField($"tx{i}_frecuenciaex1").GetValue(t) is null)
			{
				FillTransmisorNull(tx);
			}
			else
			{
				FillTransmisor5(tx, t, nombreTransmisor);
			}

			if (nReservas > 0)
			{
				bbdd.transform.Find("UCA").gameObject.SetActive(true);
				GameObject uca = bbdd.transform.Find("UCA").Find("Audio_Fallo").Find($"TX_{i}").gameObject;
				uca.SetActive(true);
				GameObject barraUCA = uca.transform.parent.GetChild(uca.transform.GetSiblingIndex() - 1).gameObject;
				barraUCA.SetActive(true);
				FillUca5(uca, t, nombreTransmisor);

				int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
				ColorizeTransmisores(tx, estado);
			}
			else
			{
				GameObject icon = tx.transform.Find("marco").Find("Image_top_reserva").gameObject;
				icon.SetActive(true);

				GameObject carga = bbdd.transform.Find("Transmisores").Find("Carga").gameObject;
				carga.SetActive(false);

				GameObject barra_marco = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).Find("marco").gameObject;
				barra_marco.SetActive(false);
			}
		}
	}
	private void FillTransmisores6(int nTransmisores, int nReservas, TorreFM6 t, GameObject bbdd)
	{
		HideBbdds(bbdd);

		// oculta los transmisores, por si se han dejado encendidos en la escena
		HideTransmisores(bbdd);

		transmisores.GetComponent<CanvasGroup>().alpha = 1;

		GameObject txr = bbdd.transform.Find("Transmisores").Find("TX_R").gameObject;
		txr.SetActive(true);

		if (t.txr_estado == 4)
		{
			FillTransmisorNull(txr);
		}
		else
		{
			FillTransmisor6(txr, t, "txr");
			// si hay potencia en la reserva, esta activa
			if (float.Parse(t.txr_potenciadirecta) > 0)
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
			}
			else
			{
				txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
			}
		}

		GameObject ucar = bbdd.transform.Find("UCA").Find("Content").Find("TX_R").gameObject;
		ucar.SetActive(true);
		FillUca6(ucar, t, "txr");

		int estadoReserva = t.txr_estado;
		ColorizeTransmisores(txr, estadoReserva);

		FillModosUCA6(t, bbdd);

		for (int i = 1; i <= nTransmisores; i++)
		{
			GameObject tx = bbdd.transform.Find("Transmisores").Find($"TX_{i}").gameObject;
			tx.SetActive(true);

			GameObject barra = tx.transform.parent.GetChild(tx.transform.GetSiblingIndex() - 1).gameObject;
			barra.SetActive(true);

			string nombreTransmisor = $"tx{i}";

			if (int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString()) == 4)
			{
				FillTransmisorNull(tx);
				if (int.Parse(t.GetType().GetField($"txr_estado").GetValue(t).ToString()) != 4 && float.Parse(t.txr_potenciadirecta) > 0)
				{
					txr.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1;
				}
			}
			else
			{
				FillTransmisor6(tx, t, nombreTransmisor);
			}

			bbdd.transform.Find("UCA").gameObject.SetActive(true);

			GameObject uca = bbdd.transform.Find("UCA").Find("Content").Find($"TX_{i}").gameObject;
			uca.SetActive(true);
			GameObject barraUCA = uca.transform.parent.GetChild(uca.transform.GetSiblingIndex() - 1).gameObject;
			barraUCA.SetActive(true);
			FillUca6(uca, t, nombreTransmisor);

			int estado = int.Parse(t.GetType().GetField($"tx{i}_estado").GetValue(t).ToString());
			ColorizeTransmisores(tx, estado);
		}
	}
	private void FillTransmisorNull(GameObject tx)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		tNombre.text = "Sin Conexión";

		CanvasGroup cTx = tx.transform.Find("Content").GetComponent<CanvasGroup>();
		cTx.alpha = .2f;
	}
	private void FillTransmisor0(GameObject tx, TorreOM t, string tr)
	{
		Image tAlarmaRF = tx.transform.Find("Content").Find("Alarma_RF").Find("BG_COLOR").GetComponent<Image>();
		Image tAlarmaAudio = tx.transform.Find("Content").Find("Alarma_Audio").Find("BG_COLOR").GetComponent<Image>();
		CanvasGroup cTx = tx.transform.Find("Content").GetComponent<CanvasGroup>();

		if (t.GetType().GetField($"{tr}_valor1").GetValue(t) == null)
        {
			cTx.alpha = .2f;
		}
        else
        {
			cTx.alpha = 1;
			
			if (t.GetType().GetField($"{tr}_valor1").GetValue(t).ToString() == "1")
			{
				tAlarmaRF.color = colorRojo;
			}
			else
			{
				tAlarmaRF.color = colorVerde;
			}

			if (t.GetType().GetField($"{tr}_valor2").GetValue(t).ToString() == "1")
			{
				tAlarmaAudio.color = colorRojo;
			}
			else
			{
				tAlarmaAudio.color = colorVerde;
			}

			int estado = int.Parse(t.GetType().GetField($"{tr}_estado").GetValue(t).ToString());
			ColorizeTransmisores(tx, estado);
		}
	}
	private void FillTransmisor1(GameObject tx, TorreFM1 t, string tr)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotencia = tx.transform.Find("Content").Find("PotenciaTX").Find("POTENCIA").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();

		Image tPower = tx.transform.Find("Content").Find("Power").Find("bg").GetComponent<Image>();
		Image tAlarmaRF = tx.transform.Find("Content").Find("Alarma_RF").Find("BG_COLOR").GetComponent<Image>();
		Image tAlarmaAudio = tx.transform.Find("Content").Find("Alarma_Audio").Find("BG_COLOR").GetComponent<Image>();

		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("TemperaturaTX").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();

		tFrecuencia.text = t.GetType().GetField($"{tr}_frecuencia").GetValue(t).ToString();

		//StartCoroutine(TxtAnim(tPotencia, t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1)));
		LeanTween.value(gameObject, float.Parse(tPotencia.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetTextDec2(x, tPotencia));
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetTextDec2(x, tPotenciaReflejada));
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));

		if (float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()) > float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		if (tr == "txr")
		{
			tNombre.text = "Reserva";

			if (t.GetType().GetField($"{tr}_reservaon").GetValue(t).ToString() == "0")
			{
				tPower.color = colorRojo;
				tx.transform.Find("Content").GetComponent<CanvasGroup>().alpha = .2f;
			}
			else
			{
				tPower.color = colorVerde;
				tx.transform.Find("Content").GetComponent<CanvasGroup>().alpha = 1f;
			}
		}
		else
		{
            switch (tr)
            {
				case "tx1":
					tNombre.text = t.GetType().GetField("emision").GetValue(t).ToString().Substring(0, 2);
					break;
				case "tx2":
					tNombre.text = t.GetType().GetField("emision").GetValue(t).ToString().Substring(3, 2);
					break;
				case "tx3":
					tNombre.text = t.GetType().GetField("emision").GetValue(t).ToString().Substring(6, 2);
					break;
				case "tx4":
					tNombre.text = t.GetType().GetField("emision").GetValue(t).ToString().Substring(9, 2);
					break;
				default:
					break;
			}

			if (t.GetType().GetField($"{tr}_encendido").GetValue(t).ToString() == "1")
			{
				tPower.color = colorRojo;
			}
		}


		if (t.GetType().GetField($"{tr}_alarmarf").GetValue(t).ToString() == "1")
		{
			tAlarmaRF.color = colorRojo;
		}

		if (t.GetType().GetField($"{tr}_alarmaaudio").GetValue(t).ToString() == "1")
		{
			tAlarmaAudio.color = colorRojo;
		}

		//StartCoroutine(TxtAnim(tTempTx, t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetTextDec2(x, tTempTx));
	}
	private void FillTransmisor2(GameObject tx, TorreFM2 t, string tr)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotencia = tx.transform.Find("Content").Find("PotenciaTX").Find("POTENCIA").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tEntrada = tx.transform.Find("Content").Find("Entrada_salida").Find("H_layout").Find("ENTRADA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tSalida = tx.transform.Find("Content").Find("Entrada_salida").Find("H_layout").Find("SALIDA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tModulacion = tx.transform.Find("Content").Find("Modulacion").Find("MODULACION_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionFill = tx.transform.Find("Content").Find("Modulacion").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacionL = tx.transform.Find("Content").Find("CanalIzq").Find("CANAL_IZQ_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionLFill = tx.transform.Find("Content").Find("CanalIzq").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacionR = tx.transform.Find("Content").Find("CanalDch").Find("CANAL_DCH_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionRFill = tx.transform.Find("Content").Find("CanalDch").Find("Barra").Find("FILL").GetComponent<Image>();
		Image tAlarmaGeneral = tx.transform.Find("Content").Find("Alarma_General").Find("BG_COLOR").GetComponent<Image>();
		Image tAlarmaRF = tx.transform.Find("Content").Find("Alarma_RF").Find("BG_COLOR").GetComponent<Image>();
		Image tAlarmaAudio = tx.transform.Find("Content").Find("Alarma_Audio").Find("BG_COLOR").GetComponent<Image>();
		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("Temperaturas").Find("TEMP_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tTempAmb = tx.transform.Find("Content").Find("Temperaturas").Find("TEMP_AMBIENTE_DATO").GetComponent<TextMeshProUGUI>();
		CanvasGroup tLocal = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Local").GetComponent<CanvasGroup>();
		CanvasGroup tRemoto = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Remoto").GetComponent<CanvasGroup>();

		tNombre.text = t.GetType().GetField($"{tr}_nombre").GetValue(t).ToString();
		tFrecuencia.text = (float.Parse(t.GetType().GetField($"{tr}_frecuencia").GetValue(t).ToString()) / 100).ToString();

		//StartCoroutine(TxtAnim(tPotencia, t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1)));

		LeanTween.value(gameObject, tModulacionFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionFill));
		
		LeanTween.value(gameObject, tModulacionLFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_modulacionL").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionLFill));
		
		LeanTween.value(gameObject, tModulacionRFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_modulacionR").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionLFill));

		
		LeanTween.value(gameObject, float.Parse(tPotencia.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotencia));
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaReflejada));
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));
		
		if (float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()) > float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		if (float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()) > 140f)
		{
			tModulacionFill.color = colorAlarmaA;
		}

		if (float.Parse(t.GetType().GetField($"{tr}_modulacionL").GetValue(t).ToString()) > 140f)
		{
			tModulacionLFill.color = colorAlarmaA;
		}

		if (float.Parse(t.GetType().GetField($"{tr}_modulacionR").GetValue(t).ToString()) > 140f)
		{
			tModulacionRFill.color = colorAlarmaA;
		}

		string tr_entrada_texto = "";
		string tr_salida_texto = "";

		switch (t.GetType().GetField($"{tr}_entrada").GetValue(t).ToString())
		{
			case "1":
				tr_entrada_texto = "DIGITAL";
				break;
			case "2":
				tr_entrada_texto = "ANALOG";
				break;
			case "3":
				tr_entrada_texto = "MPX";
				break;
			default:
				break;
		}

		tEntrada.text = tr_entrada_texto;

		switch (t.GetType().GetField($"{tr}_salida").GetValue(t).ToString())
		{
			case "1":
				tr_salida_texto = "ESTÉREO";
				break;
			case "2":
				tr_salida_texto = "MONO";
				break;
			case "3":
				tr_salida_texto = "MPX EX";
				break;
			default:
				break;
		}

		tSalida.text = tr_salida_texto;

		//StartCoroutine(TxtAnim(tModulacion, t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacion.text), float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacion));
		// BARRA
		//StartCoroutine(TxtAnim(tModulacionL, t.GetType().GetField($"{tr}_modulacionL").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacionL.text), float.Parse(t.GetType().GetField($"{tr}_modulacionL").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacionL));
		// BARRA
		//StartCoroutine(TxtAnim(tModulacionR, t.GetType().GetField($"{tr}_modulacionR").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacionR.text), float.Parse(t.GetType().GetField($"{tr}_modulacionR").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacionR));
		// BARRA
		if (t.GetType().GetField($"{tr}_alarmageneral").GetValue(t).ToString() == "1")
		{
			tAlarmaGeneral.color = colorRojo;
		}
		if (t.GetType().GetField($"{tr}_alarmapotencia").GetValue(t).ToString() == "1")
		{
			tAlarmaRF.color = colorRojo;
		}
		if (t.GetType().GetField($"{tr}_alarmaaudio").GetValue(t).ToString() == "1")
		{
			tAlarmaAudio.color = colorRojo;
		}
		//StartCoroutine(TxtAnim(tTempTx, t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempTx));
		//StartCoroutine(TxtAnim(tTempAmb, t.GetType().GetField($"{tr}_temperaturaambiente").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempAmb.text), float.Parse(t.GetType().GetField($"{tr}_temperaturaambiente").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempAmb));
		if (t.GetType().GetField($"{tr}_remoto").GetValue(t).ToString() == "0")
		{
			tLocal.alpha = 1f;
			tRemoto.alpha = .3f;

		}
		else
		{
			tLocal.alpha = .3f;
			tRemoto.alpha = 1f;
		}
	}
	private void FillTransmisor3(GameObject tx, TorreFM3 t, string tr)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotencia = tx.transform.Find("Content").Find("PotenciaDirecta").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacion = tx.transform.Find("Content").Find("Modulacion").Find("MODULACION_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionFill = tx.transform.Find("Content").Find("Modulacion").Find("Barra").Find("FILL").GetComponent<Image>();
		Image tAlarmaGeneral = tx.transform.Find("Content").Find("Alarma_General").Find("BG_COLOR").GetComponent<Image>();
		Image tAlarmaRF = tx.transform.Find("Content").Find("Alarma_RF").Find("BG_COLOR").GetComponent<Image>();
		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("TemperaturaTX").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tTempAmb = tx.transform.Find("Content").Find("TemperaturaAmbiente").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();

		tNombre.text = t.GetType().GetField($"{tr}_nombre").GetValue(t).ToString();
		tFrecuencia.text = float.Parse(t.GetType().GetField($"{tr}_frecuencia").GetValue(t).ToString().Split(' ')[0]).ToString("00.0");

		string potenciaDirecta = Regex.Replace(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString(), @"\s+", "").Split('W')[0];
		string potenciaReflejada = Regex.Replace(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), @"\s+", "").Split('W')[0];
		//StartCoroutine(TxtAnim(tPotencia, potenciaDirecta));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, potenciaReflejada));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(potenciaReflejada, 0, float.Parse(potenciaDirecta) * 0.05f, 0, 1)));


		LeanTween.value(gameObject, float.Parse(tPotencia.text), float.Parse(potenciaDirecta), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotencia));
		
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(potenciaReflejada), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaReflejada));
		
		float scaledFill = InterpolateValue(potenciaReflejada, 0, float.Parse(potenciaDirecta) * 0.05f, 0, 1);
		
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, scaledFill, animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));
		
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, scaledFill, animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));

		LeanTween.value(gameObject, tModulacionFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionFill));


		if (float.Parse(potenciaReflejada) > float.Parse(potenciaDirecta) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		if (float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()) > 140)
		{
			tModulacionFill.color = colorAlarmaA;
		}

		//StartCoroutine(TxtAnim(tModulacion, t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacion.text), float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacion));
		// BARRA
		if (t.GetType().GetField($"{tr}_alarmageneral").GetValue(t).ToString() == "1")
		{
			tAlarmaGeneral.color = colorRojo;
		}
		// la api devuelve un 1 siempre, no se si será que 1 es desactivada y 0 activada
		if (t.GetType().GetField($"{tr}_alarmapotencia").GetValue(t).ToString() == "1")
		{
			tAlarmaRF.color = colorRojo;
		}
		//StartCoroutine(TxtAnim(tTempTx, t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempTx));
		//StartCoroutine(TxtAnim(tTempAmb, t.GetType().GetField($"{tr}_temperaturaambiente").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempAmb.text), float.Parse(t.GetType().GetField($"{tr}_temperaturaambiente").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempAmb));
	}
	private void FillTransmisor4(GameObject tx, TorreFM4 t, string tr)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaTX = tx.transform.Find("Content").Find("PotenciaTX").Find("POTENCIA").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaDirecta = tx.transform.Find("Content").Find("PotenciaDirecta").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacion = tx.transform.Find("Content").Find("Modulacion").Find("MODULACION_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionFill = tx.transform.Find("Content").Find("Modulacion").Find("Barra").Find("FILL").GetComponent<Image>();
		Image tEstadoTX = tx.transform.Find("Content").Find("EstadoTX").Find("bg").GetComponent<Image>();
		Image tFuncionamientoTX = tx.transform.Find("Content").Find("FuncionamientoTX").Find("bg").GetComponent<Image>();

		Image tAlarmaAtenuacion = tx.transform.Find("Content").Find("Alarma_Atenuacion").Find("bg").GetComponent<Image>();
		Image tAtenuacion = tx.transform.Find("Content").Find("Atenuacion").Find("bg").GetComponent<Image>();
		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("TemperaturaTX").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();

		CanvasGroup tLocal = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Local").GetComponent<CanvasGroup>();
		CanvasGroup tRemoto = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Remoto").GetComponent<CanvasGroup>();

        tNombre.text = t.GetType().GetField($"{tr}_nombre").GetValue(t).ToString();
        tFrecuencia.text = (float.Parse(t.GetType().GetField($"{tr}_frecuencia").GetValue(t).ToString()) / 100).ToString("00.0");

		//StartCoroutine(TxtAnim(tPotenciaTX, t.GetType().GetField($"{tr}_potencia").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaDirecta, t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()));

		LeanTween.value(gameObject, tModulacionFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionFill));
		LeanTween.value(gameObject, float.Parse(tPotenciaTX.text), float.Parse(t.GetType().GetField($"{tr}_potencia").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaTX));
		LeanTween.value(gameObject, float.Parse(tPotenciaDirecta.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaDirecta));
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaReflejada));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1)));
		float scaledFill = InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1);
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, scaledFill, animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));
		
		if (float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()) > float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		if (float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()) > 140)
		{
			tModulacionFill.color = colorAlarmaA;
		}

		//StartCoroutine(TxtAnim(tModulacion, t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacion.text), float.Parse(t.GetType().GetField($"{tr}_modulacion").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacion));
		// BARRA

		if (t.GetType().GetField($"{tr}_estadotransmisor").GetValue(t).ToString() == "0")
		{
			tEstadoTX.color = colorRojo;
        }
        else
        {
			tEstadoTX.color = colorVerde;
		}

		if (t.GetType().GetField($"{tr}_alarmatransmisor").GetValue(t).ToString() == "1")
		{
			tFuncionamientoTX.color = colorRojo;
		}
		else
		{
			tFuncionamientoTX.color = colorVerde;
		}

		if (t.GetType().GetField($"{tr}_alarmaatenuacion").GetValue(t).ToString() == "1")
		{
			tAlarmaAtenuacion.color = colorRojo;
		}
		else
		{
			tAlarmaAtenuacion.color = colorVerde;
		}

		if (t.GetType().GetField($"{tr}_atenuacion").GetValue(t).ToString() == "1")
		{
			tAtenuacion.color = colorRojo;
		}
		else
		{
			tAtenuacion.color = colorVerde;
		}

		if (t.GetType().GetField($"{tr}_modofuncionamiento").GetValue(t).ToString() == "1")
		{
			tLocal.alpha = 1f;
			tRemoto.alpha = .3f;

		}
		else
		{
			tLocal.alpha = .3f;
			tRemoto.alpha = 1f;
		}

		//StartCoroutine(TxtAnim(tTempTx, (float.Parse(t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()) / 10).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperaturatx").GetValue(t).ToString()) / 10, animationSpeed)
			.setOnUpdate((x) => SetTextDec2(x, tTempTx));
	}
	private void FillTransmisor5(GameObject tx, TorreFM5 t, string tr)
	{
		//TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();

		TextMeshProUGUI tPotenciaDirecta = tx.transform.Find("Content").Find("PotenciaDirecta").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();

		Image tPower = tx.transform.Find("Content").Find("Power").Find("bg").GetComponent<Image>();

		TextMeshProUGUI tPotenciaDirecta1 = tx.transform.Find("Content").Find("PotenciaDirecta (1)").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada1 = tx.transform.Find("Content").Find("Pot_Reflejada (1)").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill1 = tx.transform.Find("Content").Find("Pot_Reflejada (1)").Find("Barra").Find("FILL").GetComponent<Image>();
		CanvasGroup tCanvasPotDir1 = tx.transform.Find("Content").Find("PotenciaDirecta (1)").GetComponent<CanvasGroup>();
		CanvasGroup tCanvasPotRef1 = tx.transform.Find("Content").Find("Pot_Reflejada (1)").GetComponent<CanvasGroup>();

		TextMeshProUGUI tPotenciaDirecta2 = tx.transform.Find("Content").Find("PotenciaDirecta (2)").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada2 = tx.transform.Find("Content").Find("Pot_Reflejada (2)").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill2 = tx.transform.Find("Content").Find("Pot_Reflejada (2)").Find("Barra").Find("FILL").GetComponent<Image>();
		CanvasGroup tCanvasPotDir2 = tx.transform.Find("Content").Find("PotenciaDirecta (2)").GetComponent<CanvasGroup>();
		CanvasGroup tCanvasPotRef2 = tx.transform.Find("Content").Find("Pot_Reflejada (2)").GetComponent<CanvasGroup>();

		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("TemperaturaTX").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();

		CanvasGroup tLocal = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Local").GetComponent<CanvasGroup>();
		CanvasGroup tRemoto = tx.transform.Find("Content").Find("Local_Remoto").Find("H_layout").Find("Remoto").GetComponent<CanvasGroup>();

		//tNombre.text = t.GetType().GetField($"{tr}_nombre").GetValue(t).ToString();

		//StartCoroutine(TxtAnim(tPotenciaDirecta, t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1)));
		LeanTween.value(gameObject, float.Parse(tPotenciaDirecta.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaDirecta));
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaReflejada));
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));
		if (float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()) > float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		if (t.GetType().GetField($"{tr}_transmisorencendido").GetValue(t).ToString() == "1")
        {
			tPower.color = colorRojo;
        }
        else
        {
			tPower.color = colorVerde;
		}

		if (t.GetType().GetField($"{tr}_selecexcitador").GetValue(t).ToString() == "1")
        {
			tFrecuencia.text = (float.Parse(t.GetType().GetField($"{tr}_frecuenciaex1").GetValue(t).ToString()) / 100).ToString("00.0");

			//StartCoroutine(TxtAnim(tPotenciaDirecta1, t.GetType().GetField($"{tr}_potenciadirectaexc1").GetValue(t).ToString()));
			//StartCoroutine(TxtAnim(tPotenciaReflejada1, t.GetType().GetField($"{tr}_potenciareflejadaexc1").GetValue(t).ToString()));
			//StartCoroutine(FillBar(tPotenciaReflejadaFill1, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejadaexc1").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc1").GetValue(t).ToString()) * 0.05f, 0, 1)));
			LeanTween.value(gameObject, float.Parse(tPotenciaDirecta1.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc1").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaDirecta1));
			LeanTween.value(gameObject, float.Parse(tPotenciaReflejada1.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejadaexc1").GetValue(t).ToString()), animationSpeed)
				.setOnUpdate((x) => SetText(x, tPotenciaReflejada1));
			LeanTween.value(gameObject, tPotenciaReflejadaFill1.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejadaexc1").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc1").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
				.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill1));

			tCanvasPotDir1.alpha = 1;
			tCanvasPotRef1.alpha = 1;
			tCanvasPotDir2.alpha = .3f;
			tCanvasPotRef2.alpha = .3f;
        }
        else
        {
			tFrecuencia.text = (float.Parse(t.GetType().GetField($"{tr}_frecuenciaex2").GetValue(t).ToString()) / 100).ToString("00.0");

			//StartCoroutine(TxtAnim(tPotenciaDirecta2, t.GetType().GetField($"{tr}_potenciadirectaexc2").GetValue(t).ToString()));
			//StartCoroutine(TxtAnim(tPotenciaReflejada2, t.GetType().GetField($"{tr}_potenciareflejadaexc2").GetValue(t).ToString()));
			//StartCoroutine(FillBar(tPotenciaReflejadaFill2, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejadaexc2").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc2").GetValue(t).ToString()) * 0.05f, 0, 1)));
			LeanTween.value(gameObject, float.Parse(tPotenciaDirecta2.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc2").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaDirecta2));
			LeanTween.value(gameObject, float.Parse(tPotenciaReflejada2.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejadaexc2").GetValue(t).ToString()), animationSpeed)
				.setOnUpdate((x) => SetText(x, tPotenciaReflejada2));
			LeanTween.value(gameObject, tPotenciaReflejadaFill1.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejadaexc2").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirectaexc2").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
				.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill1));

			tCanvasPotDir1.alpha = .3f;
			tCanvasPotRef1.alpha = .3f;
			tCanvasPotDir2.alpha = 1f;
			tCanvasPotRef2.alpha = 1f;
		}

		//StartCoroutine(TxtAnim(tTempTx, t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempTx));

		if (t.GetType().GetField($"{tr}_estadotx").GetValue(t).ToString() == "1")
		{
			tLocal.alpha = 1f;
			tRemoto.alpha = .3f;

		}
		else
		{
			tLocal.alpha = .3f;
			tRemoto.alpha = 1f;
		}
	}
	private void FillTransmisor6(GameObject tx, TorreFM6 t, string tr)
	{
		TextMeshProUGUI tNombre = tx.transform.Find("Content").Find("NombreTX").Find("NOMBRE_TX_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tFrecuencia = tx.transform.Find("Content").Find("Frecuencia").Find("FRECUENCIA_DATO").GetComponent<TextMeshProUGUI>();

		TextMeshProUGUI tPotenciaDirecta = tx.transform.Find("Content").Find("PotenciaDirecta").Find("POTENCIA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tPotenciaReflejada = tx.transform.Find("Content").Find("Pot_Reflejada").Find("POT_REFLEJADA_DATO").GetComponent<TextMeshProUGUI>();
		Image tPotenciaReflejadaFill = tx.transform.Find("Content").Find("Pot_Reflejada").Find("Barra").Find("FILL").GetComponent<Image>();

		TextMeshProUGUI tModulacion = tx.transform.Find("Content").Find("Modulacion").Find("MODULACION_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionFill = tx.transform.Find("Content").Find("Modulacion").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacionL = tx.transform.Find("Content").Find("CanalIzq").Find("CANAL_IZQ_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionLFill = tx.transform.Find("Content").Find("CanalIzq").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tModulacionR = tx.transform.Find("Content").Find("CanalDch").Find("CANAL_DCH_DATO").GetComponent<TextMeshProUGUI>();
		Image tModulacionRFill = tx.transform.Find("Content").Find("CanalDch").Find("Barra").Find("FILL").GetComponent<Image>();

		Image tAudio = tx.transform.Find("Content").Find("Alarmas_Audio_Mute").Find("AlarmaAudio").Find("BG_COLOR").GetComponent<Image>();
		Image tMute = tx.transform.Find("Content").Find("Alarmas_Audio_Mute").Find("AlarmaMute").Find("BG_COLOR").GetComponent<Image>();
		Image tPLL = tx.transform.Find("Content").Find("Alarmas_Pll_Foldback").Find("Pll_Foldback").Find("BG_COLOR").GetComponent<Image>();
		Image tFoldback = tx.transform.Find("Content").Find("Alarmas_Pll_Foldback").Find("AlarmaFoldback").Find("BG_COLOR").GetComponent<Image>();

		TextMeshProUGUI tTensionVPA = tx.transform.Find("Content").Find("Vpa").Find("VPA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tCorrienteIPA = tx.transform.Find("Content").Find("Ipa").Find("IPA_DATO").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI tEficiencia = tx.transform.Find("Content").Find("Eficiencia").Find("EFICIENCIA_DATO").GetComponent<TextMeshProUGUI>();
		Image tEficienciaFill = tx.transform.Find("Content").Find("Eficiencia").Find("Barra").Find("FILL").GetComponent<Image>();
		TextMeshProUGUI tTempTx = tx.transform.Find("Content").Find("TemperaturaTX").Find("TEMP_TX").GetComponent<TextMeshProUGUI>();

		//tNombre.text = t.GetType().GetField($"{tr}_nombre").GetValue(t).ToString();
		tFrecuencia.text = float.Parse(t.GetType().GetField($"{tr}_frecuencia").GetValue(t).ToString()).ToString("00.0");

		//StartCoroutine(TxtAnim(tPotenciaDirecta, t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tPotenciaReflejada, t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()));
		//StartCoroutine(FillBar(tPotenciaReflejadaFill, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1)));


		LeanTween.value(gameObject, tModulacionFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_desviacion").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionFill));

		LeanTween.value(gameObject, tModulacionLFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_canalizquierdo").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionLFill));
		
		LeanTween.value(gameObject, tModulacionRFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_canalderecho").GetValue(t).ToString(), 0, 140, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tModulacionRFill));
		
		LeanTween.value(gameObject, tEficienciaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString(), 0, 100, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tEficienciaFill));

		LeanTween.value(gameObject, float.Parse(tPotenciaDirecta.text), float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaDirecta));
		
		LeanTween.value(gameObject, float.Parse(tPotenciaReflejada.text), float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tPotenciaReflejada));
		
		LeanTween.value(gameObject, tPotenciaReflejadaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString(), 0, float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tPotenciaReflejadaFill));
		
		if (float.Parse(t.GetType().GetField($"{tr}_potenciareflejada").GetValue(t).ToString()) > float.Parse(t.GetType().GetField($"{tr}_potenciadirecta").GetValue(t).ToString()) * 0.05f)
		{
			tPotenciaReflejadaFill.color = colorAlarmaA;
		}

		//StartCoroutine(TxtAnim(tModulacion, t.GetType().GetField($"{tr}_desviacion").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacion.text), float.Parse(t.GetType().GetField($"{tr}_desviacion").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacion));
		// BARRA
		//StartCoroutine(TxtAnim(tModulacionL, t.GetType().GetField($"{tr}_canalizquierdo").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacionL.text), float.Parse(t.GetType().GetField($"{tr}_canalizquierdo").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacionL));
		// BARRA
		//StartCoroutine(TxtAnim(tModulacionR, t.GetType().GetField($"{tr}_canalderecho").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tModulacionR.text), float.Parse(t.GetType().GetField($"{tr}_canalderecho").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tModulacionR));
		// BARRA

		if (float.Parse(t.GetType().GetField($"{tr}_desviacion").GetValue(t).ToString()) > 140)
		{
			tModulacionFill.color = colorAlarmaA;
		}
		if (float.Parse(t.GetType().GetField($"{tr}_canalizquierdo").GetValue(t).ToString()) > 140)
		{
			tModulacionLFill.color = colorAlarmaA;
		}
		if (float.Parse(t.GetType().GetField($"{tr}_canalderecho").GetValue(t).ToString()) > 140)
		{
			tModulacionRFill.color = colorAlarmaA;
		}
		
		if (float.Parse(t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString()) > 100)
		{
			tEficienciaFill.color = colorAlarmaA;
		}


		if (t.GetType().GetField($"{tr}_entradaaudio").GetValue(t).ToString() == "1")
        {
			tAudio.color = colorVerde;
        }
        else
        {
			tAudio.color = colorRojo;
		}

		if (t.GetType().GetField($"{tr}_muteaudio").GetValue(t).ToString() == "1")
		{
			tMute.color = colorVerde;
		}
		else
		{
			tMute.color = colorRojo;
		}

		if (t.GetType().GetField($"{tr}_estadopll").GetValue(t).ToString() == "2")
		{
			tPLL.color = colorVerde;
		}
		else
		{
			tPLL.color = colorRojo;
		}

		if (t.GetType().GetField($"{tr}_foldback").GetValue(t).ToString() == "1")
		{
			tFoldback.color = colorVerde;
		}
		else
		{
			tFoldback.color = colorRojo;
		}

		//StartCoroutine(TxtAnim(tTensionVPA, t.GetType().GetField($"{tr}_tensionvpa").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tCorrienteIPA, t.GetType().GetField($"{tr}_corrienteipa").GetValue(t).ToString()));
		//StartCoroutine(TxtAnim(tEficiencia, t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString()));
		//StartCoroutine(FillBar(tEficienciaFill, InterpolateValue(t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString(), 0, 100, 0, 1)));
		LeanTween.value(gameObject, float.Parse(tTensionVPA.text), float.Parse(t.GetType().GetField($"{tr}_tensionvpa").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTensionVPA));
		LeanTween.value(gameObject, float.Parse(tCorrienteIPA.text), float.Parse(t.GetType().GetField($"{tr}_corrienteipa").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tCorrienteIPA));
		LeanTween.value(gameObject, float.Parse(tEficiencia.text), float.Parse(t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tEficiencia));
		LeanTween.value(gameObject, tEficienciaFill.fillAmount, InterpolateValue(t.GetType().GetField($"{tr}_eficiencia").GetValue(t).ToString(), 0, 100, 0, 1), animationSpeed)
			.setOnUpdate((x) => SetBar(x, tEficienciaFill));

		//StartCoroutine(TxtAnim(tTempTx, t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()));
		LeanTween.value(gameObject, float.Parse(tTempTx.text), float.Parse(t.GetType().GetField($"{tr}_temperatura").GetValue(t).ToString()), animationSpeed)
			.setOnUpdate((x) => SetText(x, tTempTx));
	}
	private static void FillUca1(GameObject uca, TorreFM1 t, string tr)
	{
		GameObject audioTXok = uca.transform.Find("Content").Find("AudioTX").Find("Ic").gameObject;
        GameObject audioTXalarm = uca.transform.Find("Content").Find("AudioTX").Find("IcNOK").gameObject;

		string ucaAudio = t.GetType().GetField($"uca_audio_{tr}").GetValue(t)?.ToString() ?? "0";
		string ucaFallo = t.GetType().GetField($"uca_fallo_{tr}").GetValue(t)?.ToString() ?? "0";

		if (ucaAudio == "1")
		{
			audioTXok.SetActive(true);
			audioTXalarm.SetActive(false);

		}
		else if (ucaAudio == "2")
		{
			audioTXok.SetActive(false);
			audioTXalarm.SetActive(true);
		}

		GameObject falloTXok = uca.transform.Find("Content").Find("FalloTX").Find("Ic").gameObject;
        GameObject falloTXalarm = uca.transform.Find("Content").Find("FalloTX").Find("IcNOK").gameObject;

		if (ucaFallo == "0")
		{
			falloTXok.SetActive(true);
			falloTXalarm.SetActive(false);

		}
		else
		{
			falloTXok.SetActive(false);
			falloTXalarm.SetActive(true);
		}
	}
	private static void FillUca2(GameObject uca, TorreFM2 t, string tr)
	{
		GameObject audioTXok = uca.transform.Find("Content").Find("AudioTX").Find("Ic").gameObject;
		GameObject audioTXalarm = uca.transform.Find("Content").Find("AudioTX").Find("IcNOK").gameObject;

		string ucaEstado = t.GetType().GetField($"uca_estado_{tr}").GetValue(t)?.ToString() ?? "0";
		string ucaFallo = t.GetType().GetField($"uca_fallo_{tr}").GetValue(t)?.ToString() ?? "0";

		if (ucaEstado == "1")
		{
			audioTXok.SetActive(true);
			audioTXalarm.SetActive(false);

		}
		else if (ucaEstado == "2")
		{
			audioTXok.SetActive(false);
			audioTXalarm.SetActive(true);
		}

		GameObject falloTXok = uca.transform.Find("Content").Find("FalloTX").Find("Ic").gameObject;
		GameObject falloTXalarm = uca.transform.Find("Content").Find("FalloTX").Find("IcNOK").gameObject;

		if (ucaFallo == "0")
		{
			falloTXok.SetActive(true);
			falloTXalarm.SetActive(false);

		}
		else
		{
			falloTXok.SetActive(false);
			falloTXalarm.SetActive(true);
		}
	}
	private static void FillUca5(GameObject uca, TorreFM5 t, string tr)
	{
		GameObject audioTXok = uca.transform.Find("Content").Find("AudioTX").Find("Ic").gameObject;
		GameObject audioTXalarm = uca.transform.Find("Content").Find("AudioTX").Find("IcNOK").gameObject;

		string ucaEstadoPotencia = t.GetType().GetField($"uca_estadopotencia{tr}").GetValue(t)?.ToString() ?? "0";
		string ucaTipoPotencia = t.GetType().GetField($"uca_tipopotencia{tr}").GetValue(t)?.ToString() ?? "0";

		if (ucaEstadoPotencia == "2")
		{
			audioTXok.SetActive(true);
			audioTXalarm.SetActive(false);

		}
		else if (ucaEstadoPotencia == "1")
		{
			audioTXok.SetActive(false);
			audioTXalarm.SetActive(true);
		}

		GameObject falloTXok = uca.transform.Find("Content").Find("FalloTX").Find("Ic").gameObject;
		GameObject falloTXalarm = uca.transform.Find("Content").Find("FalloTX").Find("IcNOK").gameObject;

		if (ucaTipoPotencia == "2")
		{
			falloTXok.SetActive(true);
			falloTXalarm.SetActive(false);

		}
		else
		{
			falloTXok.SetActive(false);
			falloTXalarm.SetActive(true);
		}
	}
	private static void FillUca6(GameObject uca, TorreFM6 t, string tr)
	{
		CanvasGroup estadoOn = uca.transform.Find("Content").Find("Estado").Find("On").GetComponent<CanvasGroup>();
		CanvasGroup estadoOff = uca.transform.Find("Content").Find("Estado").Find("Off").GetComponent<CanvasGroup>();
		CanvasGroup potenciaBaja = uca.transform.Find("Content").Find("NivelPotencia").Find("baja").GetComponent<CanvasGroup>();
		CanvasGroup potenciaAlta = uca.transform.Find("Content").Find("NivelPotencia").Find("alta").GetComponent<CanvasGroup>();

		string ucaEstadoPotencia = t.GetType().GetField($"uca_estadopotencia{tr}").GetValue(t)?.ToString() ?? "0";
		string ucaTipoPotencia = t.GetType().GetField($"uca_tipopotencia{tr}").GetValue(t)?.ToString() ?? "0";

		if (ucaEstadoPotencia == "2")
        {
			estadoOn.alpha = 1;
			estadoOff.alpha = .2f;
		}
		else
        {
			estadoOn.alpha = .2f;
			estadoOff.alpha = 1;
		}

		if (ucaTipoPotencia == "2")
		{
			potenciaAlta.alpha = 1;
			potenciaBaja.alpha = .2f;
		}
		else
		{
			potenciaAlta.alpha = .2f;
			potenciaBaja.alpha = 1;
		}

	}
	private void FillModosUCA1(TorreFM1 t, GameObject bbdd)
	{
		GameObject uca = bbdd.transform.Find("UCA").Find("DatosUCA").Find("Content").gameObject;

		CanvasGroup ucaManual = uca.transform.Find("UCA_Modo").Find("Manual").GetComponent<CanvasGroup>();
		CanvasGroup ucaAuto = uca.transform.Find("UCA_Modo").Find("Auto").GetComponent<CanvasGroup>();
		Image ucaGeneral = uca.transform.Find("UCA_AlarmasII").Find("General").GetComponent<Image>();
		Image ucaRF = uca.transform.Find("UCA_AlarmasII").Find("RF").GetComponent<Image>();
		Image ucaElectrica = uca.transform.Find("UCA_AlarmasII").Find("Electrica").GetComponent<Image>();
		Image ucaTimeOut = uca.transform.Find("UCA_TimeOut").Find("Manual").GetComponent<Image>();

		if (t.uca_modo == "0")
		{
			ucaManual.alpha = 1;
			ucaAuto.alpha = .3f;
		}
		else
		{
			ucaManual.alpha = .3f;
			ucaAuto.alpha = 1;
		}

		if (t.uca_alarmageneral == "1")
		{
			ucaGeneral.color = colorRojo;
		}
		else
		{
			ucaGeneral.color = colorVerde;
		}

		if (t.uca_alarmarf == "1")
		{
			ucaRF.color = colorRojo;
		}
		else
		{
			ucaRF.color = colorVerde;
		}

		if (t.uca_fallored == "1")
		{
			ucaElectrica.color = colorRojo;
		}
		else
		{
			ucaElectrica.color = colorVerde;
		}

		if (t.uca_timeout == "1")
		{
			ucaTimeOut.color = colorRojo;

		}
		else
		{
			ucaTimeOut.color = colorVerde;
		}
	}
	private void FillModosUCA2(TorreFM2 t, GameObject bbdd)
	{
		Color colorRojo = new Color32(212, 20, 20, 255);
		Color colorVerde = new Color32(140, 198, 63, 255);

		GameObject uca = bbdd.transform.Find("UCA").Find("DatosUCA").Find("Content").gameObject;

		CanvasGroup ucaManual = uca.transform.Find("UCA_Modo").Find("Manual").GetComponent<CanvasGroup>();
		CanvasGroup ucaAuto = uca.transform.Find("UCA_Modo").Find("Auto").GetComponent<CanvasGroup>();
		CanvasGroup ucaLocal = uca.transform.Find("UCA_Estado").Find("Local").GetComponent<CanvasGroup>();
		CanvasGroup ucaRemoto = uca.transform.Find("UCA_Estado").Find("Remoto").GetComponent<CanvasGroup>();
		Image ucaRadiante = uca.transform.Find("UCA_Alarmas").Find("Radiante").GetComponent<Image>();
		Image ucaComunicaciones = uca.transform.Find("UCA_Alarmas").Find("Comunicaciones").GetComponent<Image>();
		Image ucaVentilacion = uca.transform.Find("UCA_Alarmas").Find("Ventilacion").GetComponent<Image>();

		if (t.uca_modofuncionamiento == "0" || t.uca_modofuncionamiento == "1")
		{
			ucaManual.alpha = 1;
			ucaAuto.alpha = .3f;
		}
		else
		{
			ucaManual.alpha = .3f;
			ucaAuto.alpha = 1;
		}

		if (t.uca_modoremoto == "0")
		{
			ucaLocal.alpha = 1;
			ucaRemoto.alpha = .3f;
		}
		else
		{
			ucaLocal.alpha = .3f;
			ucaRemoto.alpha = 1;
		}

		if (t.uca_radiante == "1")
		{
			ucaRadiante.color = colorRojo;
		}
		else
		{
			ucaRadiante.color = colorVerde;
		}

		if (t.uca_comunicaciones == "1")
		{
			ucaComunicaciones.color = colorRojo;
		}
		else
		{
			ucaComunicaciones.color = colorVerde;
		}

		if (t.uca_ventilacion == "1")
		{
			ucaVentilacion.color = colorRojo;
		}
		else
		{
			ucaVentilacion.color = colorVerde;
		}
	}
	private void FillModosUCA5(TorreFM5 t, GameObject bbdd)
	{
		Color colorRojo = new Color32(212, 20, 20, 255);
		Color colorVerde = new Color32(140, 198, 63, 255);

		GameObject uca = bbdd.transform.Find("UCA").Find("DatosUCA").Find("Content").gameObject;

		CanvasGroup ucaManual = uca.transform.Find("UCA_Modo").Find("Manual").GetComponent<CanvasGroup>();
		CanvasGroup ucaAuto = uca.transform.Find("UCA_Modo").Find("Auto").GetComponent<CanvasGroup>();

		if (t.uca_estadouca == "1")
		{
			ucaManual.alpha = 1;
			ucaAuto.alpha = .3f;
		}
		else
		{
			ucaManual.alpha = .3f;
			ucaAuto.alpha = 1;
		}
	}
	private void FillModosUCA6(TorreFM6 t, GameObject bbdd)
	{
		GameObject uca = bbdd.transform.Find("UCA").Find("DatosUCA").Find("Content").gameObject;

		CanvasGroup ucaManual = uca.transform.Find("UCA_Modo").Find("Manual").GetComponent<CanvasGroup>();
		CanvasGroup ucaAuto = uca.transform.Find("UCA_Modo").Find("Auto").GetComponent<CanvasGroup>();

		CanvasGroup ucaLocal = uca.transform.Find("UCA_Estado").Find("Local").GetComponent<CanvasGroup>();
		CanvasGroup ucaRemoto = uca.transform.Find("UCA_Estado").Find("Remoto").GetComponent<CanvasGroup>();

		if (t.uca_estadocarga == "1")
		{
			ucaLocal.alpha = 1;
			ucaRemoto.alpha = .3f;
		}
		else
		{
			ucaLocal.alpha = .3f;
			ucaRemoto.alpha = 1;
		}

		if (t.uca_estadouca == "1")
		{
			ucaManual.alpha = 1;
			ucaAuto.alpha = .3f;
		}
		else
		{
			ucaManual.alpha = .3f;
			ucaAuto.alpha = 1;
		}
	}
	private Color SetColor(int estado)
    {
		Color color = Color.white;
		switch (estado)
        {
			case 0:
				color = new Color32(140, 198, 63, 255);
				break;
			case 1:
				color = new Color32(255, 224, 0, 255);
				break;
			case 2:
				color = new Color32(247, 147, 30, 255);
				break;
			case 3:
				color = new Color32(212, 20, 20, 255);
				break;
			case 4:
				color = new Color32(7, 127, 140, 255);
				break;
		}
		return color;
	}

	// Función que colorea un transmisor en función al estado Ok, Warning, Fault, Alarma
	private void ColorizeTransmisores(GameObject tx, int estado)
	{
		GameObject icon = tx.transform.Find("marco").Find("Image_top").gameObject;
		GameObject bg = tx.transform.Find("marco").Find("BG").gameObject;
		GameObject marco = tx.transform.Find("marco").Find("BG").Find("Image").gameObject;

		if (tx.transform.Find("marco").Find("CARGA") != null)
		{
			GameObject iconCarga = tx.transform.Find("marco").Find("CARGA").gameObject;
			iconCarga.SetActive(false);
		}

		switch (estado)
		{
			case 0: //ok
				
				icon.SetActive(true);
				bg.GetComponent<Image>().color = new Color32(11, 30, 38, 255);
				marco.GetComponent<Image>().color = new Color32(91, 240, 255, 255);

				break;
			case 1: //warning

				bg.GetComponent<Image>().color = new Color32(11, 30, 38, 255);
				marco.GetComponent<Image>().color = new Color32(255, 224, 0, 255);
				icon.SetActive(true);

				break;

			case 2: //fault

				bg.GetComponent<Image>().color = new Color32(11, 30, 38, 255);
				marco.GetComponent<Image>().color = new Color32(247, 147, 30, 255);
				icon.SetActive(true);

				break;

			case 3: //alarma

				bg.GetComponent<Image>().color = colorAlarma;
				marco.GetComponent<Image>().color = new Color32(212, 20, 20, 255);

				if (tx.transform.Find("marco").Find("CARGA") != null)
				{
					GameObject iconCarga = tx.transform.Find("marco").Find("CARGA").gameObject;

					iconCarga.SetActive(true);
					icon.SetActive(false);
				}

				break;
		}
	}

	// PANEL DERECHO

	private void FillReceptor<T>(T t) where T : TorreFM
	{
		receptor.GetComponent<CanvasGroup>().alpha = 1;
		audios.GetComponent<CanvasGroup>().alpha = 1;

		receptor.SetActive(true);
		audios.SetActive(true);
		alarmasOM.SetActive(false);

		//StartCoroutine(TxtAnim(receptorBitrate, t.receptor_bitrate));
		//StartCoroutine(TxtAnim(receptorTemperature, t.receptor_temperatura));
		//StartCoroutine(TxtAnim(receptorRFPower, t.receptor_rf));
		//StartCoroutine(TxtAnim(receptorCN, t.receptor_cn));
		LeanTween.value(gameObject, float.Parse(receptorBitrate.text), float.Parse(t.receptor_bitrate), animationSpeed)
			.setOnUpdate((x) => SetText(x, receptorBitrate));
		LeanTween.value(gameObject, float.Parse(receptorTemperature.text), float.Parse(t.receptor_temperatura), animationSpeed)
			.setOnUpdate((x) => SetText(x, receptorTemperature));
		LeanTween.value(gameObject, float.Parse(receptorRFPower.text), float.Parse(t.receptor_rf), animationSpeed)
			.setOnUpdate((x) => SetText(x, receptorRFPower));
		LeanTween.value(gameObject, float.Parse(receptorCN.text), float.Parse(t.receptor_cn), animationSpeed)
			.setOnUpdate((x) => SetText(x, receptorCN));

		float scaledRF = InterpolateValue(t.receptor_rf, -180, 180, 90, -90);
		RFFlecha.transform.rotation = Quaternion.Euler(0, 0, scaledRF);

		//StartCoroutine(FillGauge(RFFlecha, scaledRF));
		LeanTween.value(gameObject, RFFlecha.transform.eulerAngles.z, scaledRF, animationSpeed)
			.setOnUpdate((x) => SetGauge(x, RFFlecha));

		if(float.Parse(t.receptor_rf) < -70f || float.Parse(t.receptor_rf) > -35f)
		{
			receptorRFPower.color = Color.red;
		}
		else
		{
			receptorRFPower.color = Color.white;
		}

		float scaledCN = InterpolateValue(t.receptor_cn, -180, 180, 90, -90);
		CNFlecha.transform.rotation = Quaternion.Euler(0, 0, scaledCN);

		//StartCoroutine(FillGauge(CNFlecha, scaledCN));
		float valorCN = 0;
		if (CNFlecha.transform.eulerAngles.z < 0)
        {
			valorCN = CNFlecha.transform.eulerAngles.z - 360;

		}
		LeanTween.value(gameObject, valorCN, scaledCN, animationSpeed)
			.setOnUpdate((x) => SetGauge(x, CNFlecha));

		if(float.Parse(t.receptor_cn) < 0f || float.Parse(t.receptor_cn) > 20f)
		{
			receptorCN.color = Color.red;
		}
		else
		{
			receptorCN.color = Color.white;
		}
	}
	private void FillAudios(TorreFM t)
	{
		for (int i = 1; i <= listaAudios.Length; i++)
		{
			TextMeshProUGUI pid = listaAudios[i - 1].transform.Find("list").Find("PID_DATO").GetComponent<TextMeshProUGUI>();
            //StartCoroutine(TxtAnim(pid, t.GetType().GetField($"audio{i}_PID").GetValue(t)?.ToString() ?? "0"));
			LeanTween.value(gameObject, float.Parse(pid.text), float.Parse(t.GetType().GetField($"audio{i}_PID").GetValue(t)?.ToString() ?? "0"), animationSpeed)
				.setOnUpdate((x) => SetText(x, pid));

			Image colorEstado = listaAudios[i - 1].transform.Find("list").Find("Estado").Find("COLOR_ESTADO").GetComponent<Image>();

			string estado = t.GetType().GetField($"audio{i}_estado").GetValue(t)?.ToString() ?? "0";

			if (estado == "1")
			{
				colorEstado.color = colorRojo;
			}
			else
			{
				colorEstado.color = colorVerde;
			}

			GameObject audioAlarma = listaAudios[i - 1].transform.Find("list").Find("Alarma").Find("icon_estado").gameObject;
			string alarma = t.GetType().GetField($"audio{i}_alarma").GetValue(t)?.ToString() ?? "0";
			if (alarma == "1")
			{
				audioAlarma.SetActive(true);
			}
			else
			{
				audioAlarma.SetActive(false);
			} 

			GameObject audioRDS = listaAudios[i - 1].transform.Find("list").Find("RDS").Find("bg").Find("FILL").gameObject;
			string rds = t.GetType().GetField($"audio{i}_estado_RDS").GetValue(t)?.ToString() ?? "0";

			//Gradient gradient = new Gradient();
			//GradientColorKey[] colorKey = new GradientColorKey[2];

			//colorKey[0].color = Color.red;
			//colorKey[1].color = Color.yellow;
			//colorKey[2].color = Color.green;

			if (rds == "1")
            {
				audioRDS.GetComponent<Image>().fillAmount = .8f;
				//LeanTween.value(gameObject, .6f, .8f, animSpeed)
				//	.setOnUpdate((x) => SetText(x, modemBitRateIn));
			}
            else
            {
				audioRDS.GetComponent<Image>().fillAmount = .2f;
            }

		}
	}
	private void FillReceptorAudiosNull()
	{

		receptor.SetActive(true);
		audios.SetActive(true);
		alarmasOM.SetActive(false);

		receptorBitrate.text = "null";
		receptorTemperature.text = "null";
		receptorRFPower.text = "null";
		receptorCN.text = "null";

		receptor.GetComponent<CanvasGroup>().alpha = .2f;
		audios.GetComponent<CanvasGroup>().alpha = .2f;
	}
	private void FillAlarmasOM(TorreOM t)
	{
		receptor.SetActive(false);
		audios.SetActive(false);
		alarmasOM.SetActive(true);

		GameObject receptorDatos = alarmasOM.transform.Find("Alarmas").Find("Content").Find("Datos").gameObject;
		GameObject receptorContent = alarmasOM.transform.Find("Alarmas").Find("Content").Find("Alarmas").gameObject;

		for (int i = 0; i < receptorDatos.transform.childCount; i++)
		{
			string variable = receptorDatos.transform.GetChild(i).name;

			if (t.GetType().GetField($"{variable}").GetValue(t).ToString() == "10")
			{
				receptorDatos.transform.GetChild(i).gameObject.SetActive(false);
				receptorContent.transform.GetChild(i).gameObject.SetActive(false);
			}
			else if (t.GetType().GetField($"{variable}").GetValue(t).ToString() == "1")
			{
				receptorDatos.transform.GetChild(i).gameObject.SetActive(true);
				receptorContent.transform.GetChild(i).gameObject.SetActive(true);

				receptorContent.transform.GetChild(i).Find("BG_COLOR").GetComponent<Image>().color = colorRojo;
			}
			else
			{
				receptorDatos.transform.GetChild(i).gameObject.SetActive(true);
				receptorContent.transform.GetChild(i).gameObject.SetActive(true);

				receptorContent.transform.GetChild(i).Find("BG_COLOR").GetComponent<Image>().color = colorVerde;
			}
		}


	}

	#endregion
	// Función que hace una interpolación lineal para manejar gráficos.
	// value = valor actual del kpi a interpolar
	// minValue = valor mínimo posible del kpi
	// maxValue = valor máximo posible del kpi
	// minDim = valor en dimensiones de Unity del valor mínimo
	// maxDim = valor en dimensiones de Unity del valor máximo
	private float InterpolateValue(string value, float minValue, float maxValue, float minDim, float maxDim)
	{
		float.TryParse(value, out float valuef);
		return Mathf.Lerp(minDim, maxDim, (valuef - minValue) / (maxValue - minValue));
	}

	private float InterpolateValue(float value, float max, float min = 0)
	{

		return 0;
	}

	// Anima el texto para que en 100 frames pase del valor firstValue, que por defecto es 0, al final value. El parámetro txt es el texto que se quiere cambiar
	private void SetText(float value, TextMeshProUGUI txt)
	{
		txt.text = value.ToString("N0");
	}
	private void SetTextDec1(float value, TextMeshProUGUI txt)
	{
		txt.text = value.ToString("N1");
	}
	private void SetTextDec2(float value, TextMeshProUGUI txt)
	{
		txt.text = value.ToString("N2");
	}
	private void SetGauge(float value, GameObject flecha)
	{
		flecha.transform.rotation = Quaternion.Euler(0, 0, value);
	}
	private void SetBar(float value, Image barra)
	{
		barra.fillAmount = value;
	}
	private void SetCuadrados(float value, Transform cuadrados)
    {
		cuadrados.GetChild((int)value).gameObject.SetActive(true);
    }
	private void SetPorcentajeBarras(float value, RectTransform barra)
    {
		barra.sizeDelta = new Vector2(barra.sizeDelta.x, value);
    }
    //private IEnumerator TxtAnim(TextMeshProUGUI txt, string finalValue, float firstValue = 0)
    //{
    //    float.TryParse(finalValue, out float finalValuef);
    //    for (float i = 0; i <= 1; i += animSpeed)
    //    {
    //        txt.text = Mathf.Round(Mathf.Lerp(firstValue, finalValuef, i)).ToString();
    //        yield return null;
    //    }
    //}
    //// Anima un gauge para que en 100 frames pase del valor firstValue, que por defecto es 0, al final value. flecha es el gameobject de la flecha del gauge
    //private IEnumerator FillGauge(GameObject flecha, float finalValue, float firstValue = 0)
    //{
    //	for (float i = 0; i <= 1; i += animSpeed)
    //	{
    //		//flecha.fillAmount = Mathf.Lerp(firstValue, finalValue, i);
    //		flecha.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(firstValue, finalValue, i));
    //		yield return null;
    //	}
    //}
    //// Anima un gauge para que en 100 frames pase del valor firstValue, que por defecto es 0, al final value. barra es la imagen de la barra del gráfico
    //private IEnumerator FillBar(Image barra, float finalValue, float firstValue = 0)
    //{
    //	for (float i = 0; i <= 1; i += animSpeed)
    //	{
    //		barra.fillAmount = Mathf.Lerp(firstValue, finalValue, i);
    //		yield return null;
    //	}
    //}
    //private IEnumerator AnimPorcentajeBarras(RectTransform barra, float finalValue, float firstValue = 0)
    //{
    //	for (float i = 0; i <= 1; i += animSpeed)
    //	{
    //		barra.sizeDelta = new Vector2(barra.sizeDelta.x, Mathf.Lerp(firstValue, finalValue, i));
    //		yield return null;
    //	}
    //}
    //private IEnumerator AnimCuadrados(Transform cuadrados)
    //{
    //	int numCuadrados = cuadrados.childCount;
    //	for (int i = 0; i < numCuadrados; i++)
    //	{
    //		cuadrados.GetChild(i).name = i.ToString();
    //		cuadrados.GetChild(i).gameObject.SetActive(true);
    //		yield return null;
    //	}
    //}

    // Oculta todos los transmisores en las bbdd que puedan tener un número variable de transmisores, para poder mostrarlos en función al número que tengan realmente
    private void HideTransmisores(GameObject bbdd)
	{
		for (int i = 1; i < bbdd.transform.Find("Transmisores").childCount - 1; i++)
		{
			bbdd.transform.Find("Transmisores").GetChild(i).gameObject.SetActive(false);
		}
		if (bbdd == bbdd6_4_1)
        {
			for (int i = 1; i < bbdd.transform.Find("UCA").Find("Content").childCount - 1; i++)
			{
				bbdd.transform.Find("UCA").Find("Content").GetChild(i).gameObject.SetActive(false);
			}
		} else if (bbdd == bbddOM)
        {
			
        }
        else
        {
			for (int i = 1; i < bbdd.transform.Find("UCA").Find("Audio_Fallo").childCount - 1; i++)
			{
				bbdd.transform.Find("UCA").Find("Audio_Fallo").GetChild(i).gameObject.SetActive(false);
			}
		}
	}
	private void HideBbdds(GameObject bbdd)
	{
		for (int i = 0; i < bbdd.transform.parent.childCount; i++)
		{
			bbdd.transform.parent.GetChild(i).gameObject.SetActive(false);
		}
		bbdd.SetActive(true);
	}

	public void AlternarPanelSalir()
	{
		panelSalir.SetActive(!panelSalir.activeSelf);
	}
}