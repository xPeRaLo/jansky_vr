﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserData
{
	public string usuario;
	public string password;
	public bool isAuth;
	public string token;

	public string nombre;
	public string nivel;
}
