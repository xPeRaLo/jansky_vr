﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using SimpleJSON;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.SceneManagement;
using Utils.Enums;
using Utils.FileSystem;

public enum TipoTorre { FM1, FM2, FM3, FM4, FM5, FM6, OM, TorreBase }
public class rtveAPI : MonoBehaviour
{

	#region SINGLETON

	private static rtveAPI instance;

	public static rtveAPI Instance
	{
		get
		{
			/*if (instance == null)
			{
				

				instance = GameObject.FindObjectOfType<rtveAPI>();

				if (instance == null)
				{

						GameObject container = new GameObject("APIReaders");
						instance = container.AddComponent<rtveAPI>();
					
				}
			} */

			return instance;
		}
	}

	#endregion

	[HideInInspector] public string downloadHandler;
	public GameObject loginWindow;

	[HideInInspector] public static string token;
	[HideInInspector] string usuarioDebug = "immersia";
	[HideInInspector] string passwordDebug = "12345";

	[HideInInspector] public static string usuario;
	[HideInInspector] public static string password;
	[HideInInspector] public static string nombre;
	[HideInInspector] public static string apellido;
	[HideInInspector] public static string nivel;
	[HideInInspector] public static bool isAuth = false;
	[HideInInspector] public bool isEndLogin = true;
	[HideInInspector] public bool isRequesting = false;
	[HideInInspector] public bool nivelCentroSynced = false;
	[HideInInspector] public bool datosSynced = false;

	[HideInInspector] public string towerData = "";

	[SerializeField] public MapVis mapVis;
	[SerializeField] public GameObject mapaBing;
	public TextAsset estadoCentros;

	public DateTime lastUpdate; 
	DateTime nextUpdate;

	DateTime systemTime;

	public GameObject timerGO;
    public bool isAssigning;

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			instance = this;
		}
	}

	public IEnumerator Login(string usuario, string password)
	{
		isEndLogin = false;
		UIReferences.Instance.LoginButton.interactable = isEndLogin;

		string url = String.Format("http://centrosemisores.rtve.es:7600/login?usuario={0}&password={1}", usuario, password); // Iniciamos sesión en la url de RTVE con user y pass definidos

		UnityWebRequest www = new UnityWebRequest(url, "POST")
		{
			downloadHandler = new DownloadHandlerBuffer() 
		};

		yield return www.SendWebRequest(); // Devolvemos la peticion

		downloadHandler = www.downloadHandler.text;

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
		}
		else
		{

			var jsonParsed = JSON.Parse(www.downloadHandler.text);

			rtveAPI.usuario = usuario;
			rtveAPI.password = password;
			token = jsonParsed["token"];
			nombre = jsonParsed["nombre"];
			apellido = jsonParsed["apellido"];
			nivel = jsonParsed["nivel"];

			if (token != null)
			{
				isAuth = true;

				if (UIReferences.Instance.passwordToggle != null)
				{

					UserData userData = new UserData()
					{
						usuario = usuario,
						password = "",
						isAuth = isAuth,
						token = token,
						nombre = nombre,
						nivel = nivel
					};

					if (UIReferences.Instance.passwordToggle.isOn)
						userData.password = password;

					FileSystem.ExportData(userData);
				}		
			}
		}

		isEndLogin = true;
		UIReferences.Instance.LoginButton.interactable = isEndLogin;

	}

	public IEnumerator GetData(int num_centro, bool nivelCentro)
	{
		isRequesting = true;

		if (GlobalController.Instance.GetCentro(num_centro).propietario == "CELLNEX")
        {
			if(transform.Find("/Torres").Find("Torres Cellnex").Find(num_centro.ToString()).TryGetComponent(out TorreBase tC))
			{
				TipoTorre tipo = TipoTorre.TorreBase;

				if (nivelCentro) // Solo actualiza la UI a nivel centro si estamos en ese nivel
				{
					UIReferences.Instance.UpdateBody(tC, tipo);
				}
			}
		
			yield break;
		}

		if (!isAuth)
			yield return new WaitUntil(() => isAuth); // Espera hasta que este autenticado para entrar, si no no entra

		string url = String.Format("http://centrosemisores.rtve.es:7600/api/{0}", num_centro); // Hacemos una llamada con numero de centro a la API de RTVE

		UnityWebRequest www = new UnityWebRequest(url);
		www.SetRequestHeader("access-token", token);
		www.downloadHandler = new DownloadHandlerBuffer(); //Obtenemos la informacion del request

		yield return www.SendWebRequest(); //Devolvemos la web request


		if (www.error == null) // Si no devuelve error
		{
			var jsonParsed = JSON.Parse(www.downloadHandler.text); // Parseamos el JSON
			var results = jsonParsed["results"][0]; // Cogemos solo el result

			towerData = results.ToString(); // Lo almacenamos en una var como string
			StartCoroutine(SetTowerData(towerData, num_centro, nivelCentro));
		}
		else // Si devuelve error
		{
			Debug.Log(www.error);
			towerData = www.error.ToString(); // Almacenamos en la variable el error
		}
		isRequesting = false;

    }

	IEnumerator SetTowerData(string towerData, int num_centro, bool nivelCentro)
	{
		//Debug.Log(num_centro);

        var results = JSON.Parse(towerData);

		string tipoCentro = GlobalController.Instance.GetCentro(num_centro).bbdd;

		TipoTorre tipo;

		float wait = 0f; 

		switch (tipoCentro)
		{
			case "bbdd":

				TorreOM t0 = transform.Find("/Torres").Find("Torres OM").Find(num_centro.ToString()).GetComponent<TorreOM>();
				tipo = TipoTorre.OM;

				t0.dia = DateTime.Parse(results["dia"]);
				t0.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t0.modem_estado = results["modem_estado"];
				t0.modem_nombre = results["modem_nombre"];
				t0.modem_rssi = results["modem_rssi"];
				t0.modem_datos_in = results["modem_datos_in"];
				t0.modem_datos_out = results["modem_datos_out"];
				t0.modem_consumo_in = results["modem_consumo_in"];
				t0.modem_consumo_out = results["modem_consumo_out"];
				t0.texto_alarma = results["texto_alarma"];

				#endregion
				t0.centro_nombre = results["centro_nombre"];
				t0.redcompania = results["redcompania"];
				t0.redcentro = results["redcentro"];
				t0.fuego = results["fuego"];
				t0.intrusion = results["intrusion"];
				t0.grupoespera = results["grupoespera"];
				t0.grupo = results["grupo"];
				t0.temperatura = results["temperatura"];

				#region Transmisores

				t0.tx1_estado = results["tx1_estado"];
				t0.tx1_valor1 = results["tx1_valor1"];
				t0.tx1_valor2 = results["tx1_valor2"];
				t0.tx1_valor3 = results["tx1_valor3"];

				t0.tx2_estado = results["tx2_estado"];
				t0.tx2_valor1 = results["tx2_valor1"];
				t0.tx2_valor2 = results["tx2_valor2"];
				t0.tx2_valor3 = results["tx2_valor3"];

				t0.tx3_estado = results["tx3_estado"];
				t0.tx3_valor1 = results["tx3_valor1"];
				t0.tx3_valor2 = results["tx3_valor2"];
				t0.tx3_valor3 = results["tx3_valor3"];

				t0.tx4_estado = results["tx4_estado"];
				t0.tx4_valor1 = results["tx4_valor1"];
				t0.tx4_valor2 = results["tx4_valor2"];
				t0.tx4_valor3 = results["tx4_valor3"];

				#endregion

				if (nivelCentro) // Solo actualiza la UI a nivel centro si estamos en ese nivel
				{
					UIReferences.Instance.UpdateBody(t0, tipo);
				}

				break;

			case "bbdd_1":

				TorreFM1 t1 = transform.Find("/Torres").Find("Torres FM1").Find(num_centro.ToString()).GetComponent<TorreFM1>();
				tipo = TipoTorre.FM1;

				t1.dia = DateTime.Parse(results["dia"]);
				t1.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t1.modem_estado = results["modem_estado"];
				t1.modem_nombre = results["modem_nombre"];
				t1.modem_rssi = results["modem_rssi"];
				t1.modem_datos_in = results["modem_datos_in"];
				t1.modem_datos_out = results["modem_datos_out"];
				t1.modem_consumo_in = results["modem_consumo_in"];
				t1.modem_consumo_out = results["modem_consumo_out"];
				t1.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t1.audio1_alarma = results["audio1_alarma"];
				t1.audio1_estado = results["audio1_estado"];
				t1.audio1_estado_RDS = results["audio1_estado_RDS"];
				t1.audio1_PID = results["audio1_PID"];

				t1.audio2_alarma = results["audio2_alarma"];
				t1.audio2_estado = results["audio2_estado"];
				t1.audio2_estado_RDS = results["audio2_estado_RDS"];
				t1.audio2_PID = results["audio2_PID"];

				t1.audio3_alarma = results["audio3_alarma"];
				t1.audio3_estado = results["audio3_estado"];
				t1.audio3_estado_RDS = results["audio3_estado_RDS"];
				t1.audio3_PID = results["audio3_PID"];

				t1.audio4_alarma = results["audio4_alarma"];
				t1.audio4_estado = results["audio4_estado"];
				t1.audio4_estado_RDS = results["audio4_estado_RDS"];
				t1.audio4_PID = results["audio4_PID"];

				#endregion

				#region Transmisores

				t1.tx1_alarmaaudio = results["tx1_alarmaaudio"];
				t1.tx1_alarmarf = results["tx1_alarmarf"];
				t1.tx1_encendido = results["tx1_encendido"];
				t1.tx1_estado = results["tx1_estado"];
				t1.tx1_frecuencia = results["tx1_frecuencia"];
				t1.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t1.tx1_potenciareflejada = results["tx1_potenciareflejada"];
				t1.tx1_temperatura = results["tx1_temperatura"];

				t1.tx2_alarmaaudio = results["tx2_alarmaaudio"];
				t1.tx2_alarmarf = results["tx2_alarmarf"];
				t1.tx2_encendido = results["tx2_encendido"];
				t1.tx2_estado = results["tx2_estado"];
				t1.tx2_frecuencia = results["tx2_frecuencia"];
				t1.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t1.tx2_potenciareflejada = results["tx2_potenciareflejada"];
				t1.tx2_temperatura = results["tx2_temperatura"];

				t1.tx3_alarmaaudio = results["tx3_alarmaaudio"];
				t1.tx3_alarmarf = results["tx3_alarmarf"];
				t1.tx3_encendido = results["tx3_encendido"];
				t1.tx3_estado = results["tx3_estado"];
				t1.tx3_frecuencia = results["tx3_frecuencia"];
				t1.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t1.tx3_potenciareflejada = results["tx3_potenciareflejada"];
				t1.tx3_temperatura = results["tx3_temperatura"];

				t1.tx4_alarmaaudio = results["tx4_alarmaaudio"];
				t1.tx4_alarmarf = results["tx4_alarmarf"];
				t1.tx4_encendido = results["tx4_encendido"];
				t1.tx4_estado = results["tx4_estado"];
				t1.tx4_frecuencia = results["tx4_frecuencia"];
				t1.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t1.tx4_potenciareflejada = results["tx4_potenciareflejada"];
				t1.tx4_temperatura = results["tx4_temperatura"];

				t1.txr_estado = results["txr_estado"];
				t1.txr_frecuencia = results["txr_frecuencia"];
				t1.txr_potenciadirecta = results["txr_potenciadirecta"];
				t1.txr_potenciareflejada = results["txr_potenciareflejada"];
				t1.txr_temperatura = results["txr_temperatura"];
				t1.txr_alarmarf = results["txr_alarmarf"];
				t1.txr_alarmaaudio = results["txr_alarmaaudio"];
				t1.txr_reservaon = results["txr_reservaon"];

				#endregion

				#region Receptor

				t1.receptor_bitrate = results["receptor_bitrate"];
				t1.receptor_cn = results["receptor_cn"];
				t1.receptor_estado = results["receptor_estado"];
				t1.receptor_locked = results["receptor_locked"];
				t1.receptor_portadora = results["receptor_portadora"];
				t1.receptor_rf = results["receptor_rf"];
				t1.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				#region UCA

				t1.uca_alarmageneral = results["uca_alarmageneral"];
				t1.uca_alarmarf = results["uca_alarmarf"];
				t1.uca_audio_tx1 = results["uca_audio_tx1"];
				t1.uca_audio_tx2 = results["uca_audio_tx2"];
				t1.uca_audio_tx3 = results["uca_audio_tx3"];
				t1.uca_audio_tx4 = results["uca_audio_tx4"];
				t1.uca_audio_txr = results["uca_audio_txr"];
				t1.uca_estado = results["uca_estado"];
				t1.uca_fallored = results["uca_fallored"];
				t1.uca_fallo_tx1 = results["uca_fallo_tx1"];
				t1.uca_fallo_tx2 = results["uca_fallo_tx2"];
				t1.uca_fallo_tx3 = results["uca_fallo_tx3"];
				t1.uca_fallo_tx4 = results["uca_fallo_tx4"];
				t1.uca_fallo_txr = results["uca_fallo_txr"];
				t1.uca_modo = results["uca_modo"];
				t1.uca_timeout = results["uca_timeout"];
				t1.uca_txreservaon = results["uca_txreservaon"];

				#endregion


				if (nivelCentro) // Solo actualiza la UI a nivel centro si estamos en ese nivel
				{
					UIReferences.Instance.UpdateBody(t1, tipo);
				}

				break;

			case "bbdd_2":
				
				TorreFM2 t2 = transform.Find("/Torres").Find("Torres FM2").Find(num_centro.ToString()).GetComponent<TorreFM2>();
				tipo = TipoTorre.FM2;

				t2.dia = DateTime.Parse(results["dia"]);
				t2.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t2.modem_estado = results["modem_estado"];
				t2.modem_nombre = results["modem_nombre"];
				t2.modem_rssi = results["modem_rssi"];
				t2.modem_datos_in = results["modem_datos_in"];
				t2.modem_datos_out = results["modem_datos_out"];
				t2.modem_consumo_in = results["modem_consumo_in"];
				t2.modem_consumo_out = results["modem_consumo_out"];
				t2.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t2.audio1_alarma = results["audio1_alarma"];
				t2.audio1_estado = results["audio1_estado"];
				t2.audio1_estado_RDS = results["audio1_estado_RDS"];
				t2.audio1_PID = results["audio1_PID"];
				

				t2.audio2_alarma = results["audio2_alarma"];
				t2.audio2_estado = results["audio2_estado"];
				t2.audio2_estado_RDS = results["audio2_estado_RDS"];
				t2.audio2_PID = results["audio2_PID"];

				t2.audio3_alarma = results["audio3_alarma"];
				t2.audio3_estado = results["audio3_estado"];
				t2.audio3_estado_RDS = results["audio3_estado_RDS"];
				t2.audio3_PID = results["audio3_PID"];

				t2.audio4_alarma = results["audio4_alarma"];
				t2.audio4_estado = results["audio4_estado"];
				t2.audio4_estado_RDS = results["audio4_estado_RDS"];
				t2.audio4_PID = results["audio4_PID"];

				#endregion

				#region Transmisores

				t2.tx1_alarmaaudio = results["tx1_alarmaaudio"];
				t2.tx1_alarmageneral = results["tx1_alarmageneral"];
				t2.tx1_alarmapotencia = results["tx1_alarmapotencia"];
				t2.tx1_entrada = results["tx1_entrada"];
				t2.tx1_estado = results["tx1_estado"];
				t2.tx1_frecuencia = results["tx1_frecuencia"];
				t2.tx1_modulacion = results["tx1_modulacion"];
				t2.tx1_modulacionL = results["tx1_modulacionL"];
				t2.tx1_modulacionR = results["tx1_modulacionR"];
				t2.tx1_nombre = results["tx1_nombre"];
				t2.tx1_potencia = results["tx1_potencia"];
				t2.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t2.tx1_potenciareflejada = results["tx1_potenciareflejada"];
				t2.tx1_remoto = results["tx1_remoto"];
				t2.tx1_salida = results["tx1_salida"];
				t2.tx1_temperaturaambiente = results["tx1_temperaturaambiente"];
				t2.tx1_temperaturatx = results["tx1_temperaturatx"];
				t2.tx1_tipo = results["tx1_tipo"];

				t2.tx2_alarmaaudio = results["tx2_alarmaaudio"];
				t2.tx2_alarmageneral = results["tx2_alarmageneral"];
				t2.tx2_alarmapotencia = results["tx2_alarmapotencia"];
				t2.tx2_entrada = results["tx2_entrada"];
				t2.tx2_estado = results["tx2_estado"];
				t2.tx2_frecuencia = results["tx2_frecuencia"];
				t2.tx2_modulacion = results["tx2_modulacion"];
				t2.tx2_modulacionL = results["tx2_modulacionL"];
				t2.tx2_modulacionR = results["tx2_modulacionR"];
				t2.tx2_nombre = results["tx2_nombre"];
				t2.tx2_potencia = results["tx2_potencia"];
				t2.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t2.tx2_potenciareflejada = results["tx2_potenciareflejada"];
				t2.tx2_remoto = results["tx2_remoto"];
				t2.tx2_salida = results["tx2_salida"];
				t2.tx2_temperaturaambiente = results["tx2_temperaturaambiente"];
				t2.tx2_temperaturatx = results["tx2_temperaturatx"];
				t2.tx2_tipo = results["tx2_tipo"];

				t2.tx3_alarmaaudio = results["tx3_alarmaaudio"];
				t2.tx3_alarmageneral = results["tx3_alarmageneral"];
				t2.tx3_alarmapotencia = results["tx3_alarmapotencia"];
				t2.tx3_entrada = results["tx3_entrada"];
				t2.tx3_estado = results["tx3_estado"];
				t2.tx3_frecuencia = results["tx3_frecuencia"];
				t2.tx3_modulacion = results["tx3_modulacion"];
				t2.tx3_modulacionL = results["tx3_modulacionL"];
				t2.tx3_modulacionR = results["tx3_modulacionR"];
				t2.tx3_nombre = results["tx3_nombre"];
				t2.tx3_potencia = results["tx3_potencia"];
				t2.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t2.tx3_potenciareflejada = results["tx3_potenciareflejada"];
				t2.tx3_remoto = results["tx3_remoto"];
				t2.tx3_salida = results["tx3_salida"];
				t2.tx3_temperaturaambiente = results["tx3_temperaturaambiente"];
				t2.tx3_temperaturatx = results["tx3_temperaturatx"];
				t2.tx3_tipo = results["tx3_tipo"];

				t2.tx4_alarmaaudio = results["tx4_alarmaaudio"];
				t2.tx4_alarmageneral = results["tx4_alarmageneral"];
				t2.tx4_alarmapotencia = results["tx4_alarmapotencia"];
				t2.tx4_entrada = results["tx4_entrada"];
				t2.tx4_estado = results["tx4_estado"];
				t2.tx4_frecuencia = results["tx4_frecuencia"];
				t2.tx4_modulacion = results["tx4_modulacion"];
				t2.tx4_modulacionL = results["tx4_modulacionL"];
				t2.tx4_modulacionR = results["tx4_modulacionR"];
				t2.tx4_nombre = results["tx4_nombre"];
				t2.tx4_potencia = results["tx4_potencia"];
				t2.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t2.tx4_potenciareflejada = results["tx4_potenciareflejada"];
				t2.tx4_remoto = results["tx4_remoto"];
				t2.tx4_salida = results["tx4_salida"];
				t2.tx4_temperaturaambiente = results["tx4_temperaturaambiente"];
				t2.tx4_temperaturatx = results["tx4_temperaturatx"];
				t2.tx4_tipo = results["tx4_tipo"];

				t2.txr_alarmaaudio = results["txr_alarmaaudio"];
				t2.txr_alarmageneral = results["txr_alarmageneral"];
				t2.txr_alarmapotencia = results["txr_alarmapotencia"];
				t2.txr_entrada = results["txr_entrada"];
				t2.txr_estado = results["txr_estado"];
				t2.txr_frecuencia = results["txr_frecuencia"];
				t2.txr_modulacion = results["txr_modulacion"];
				t2.txr_modulacionL = results["txr_modulacionL"];
				t2.txr_modulacionR = results["txr_modulacionR"];
				t2.txr_nombre = results["txr_nombre"];
				t2.txr_potencia = results["txr_potencia"];
				t2.txr_potenciadirecta = results["txr_potenciadirecta"];
				t2.txr_potenciareflejada = results["txr_potenciareflejada"];
				t2.txr_remoto = results["txr_remoto"];
				t2.txr_salida = results["txr_salida"];
				t2.txr_temperaturaambiente = results["txr_temperaturaambiente"];
				t2.txr_temperaturatx = results["txr_temperaturatx"];
				t2.txr_tipo = results["txr_tipo"];


				#endregion

				#region Receptor

				t2.receptor_bitrate = results["receptor_bitrate"];
				t2.receptor_cn = results["receptor_cn"];
				t2.receptor_estado = results["receptor_estado"];
				t2.receptor_locked = results["receptor_locked"];
				t2.receptor_portadora = results["receptor_portadora"];
				t2.receptor_rf = results["receptor_rf"];
				t2.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				#region UCA

				t2.uca_comunicaciones = results["uca_comunicaciones"];
				t2.uca_estado = results["uca_estado"];
				t2.uca_estado_tx1 = results["uca_estado_tx1"];
				t2.uca_estado_tx2 = results["uca_estado_tx2"];
				t2.uca_estado_tx3 = results["uca_estado_tx3"];
				t2.uca_estado_tx4 = results["uca_estado_tx4"];
				t2.uca_estado_txr = results["uca_estado_txr"];
				t2.uca_fallo_tx1 = results["uca_fallo_tx1"];
				t2.uca_fallo_tx2 = results["uca_fallo_tx2"];
				t2.uca_fallo_tx3 = results["uca_fallo_tx3"];
				t2.uca_fallo_tx4 = results["uca_fallo_tx4"];
				t2.uca_fallo_txr = results["uca_fallo_txr"];
				t2.uca_modofuncionamiento = results["uca_modofuncionamiento"];
				t2.uca_modoremoto = results["uca_modoremoto"];
				t2.uca_numero_tx = results["uca_numero_tx"];
				t2.uca_radiante = results["uca_radiante"];
				t2.uca_ventilacion = results["uca_ventilacion"];


				#endregion

				if (nivelCentro) // Solo actualiza la UI a nivel centro si estamos en ese nivel
				{
					UIReferences.Instance.UpdateBody(t2, tipo);
				}

				break;

			case "bbdd_3":

				TorreFM3 t3 = transform.Find("/Torres").Find("Torres FM3").Find(num_centro.ToString()).GetComponent<TorreFM3>();
				tipo = TipoTorre.FM3;

				t3.dia = DateTime.Parse(results["dia"]);
				t3.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t3.modem_estado = results["modem_estado"];
				t3.modem_nombre = results["modem_nombre"];
				t3.modem_rssi = results["modem_rssi"];
				t3.modem_datos_in = results["modem_datos_in"];
				t3.modem_datos_out = results["modem_datos_out"];
				t3.modem_consumo_in = results["modem_consumo_in"];
				t3.modem_consumo_out = results["modem_consumo_out"];
				t3.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t3.audio1_alarma = results["audio1_alarma"];
				t3.audio1_estado = results["audio1_estado"];
				t3.audio1_estado_RDS = results["audio1_estado_RDS"];
				t3.audio1_PID = results["audio1_PID"];


				t3.audio2_alarma = results["audio2_alarma"];
				t3.audio2_estado = results["audio2_estado"];
				t3.audio2_estado_RDS = results["audio2_estado_RDS"];
				t3.audio2_PID = results["audio2_PID"];

				t3.audio3_alarma = results["audio3_alarma"];
				t3.audio3_estado = results["audio3_estado"];
				t3.audio3_estado_RDS = results["audio3_estado_RDS"];
				t3.audio3_PID = results["audio3_PID"];

				t3.audio4_alarma = results["audio4_alarma"];
				t3.audio4_estado = results["audio4_estado"];
				t3.audio4_estado_RDS = results["audio4_estado_RDS"];
				t3.audio4_PID = results["audio4_PID"];


				#endregion

				#region Transmisores

	
				t3.tx1_alarmageneral = results["tx1_alarmageneral"];
				t3.tx1_alarmapotencia = results["tx1_alarmapotencia"];
				t3.tx1_estado = results["tx1_estado"];
				t3.tx1_frecuencia = results["tx1_frecuencia"];
				t3.tx1_modulacion = results["tx1_modulacion"];
				t3.tx1_nombre = results["tx1_nombre"];
				t3.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t3.tx1_potenciareflejada = results["tx1_potenciareflejada"];
				t3.tx1_temperaturaambiente = results["tx1_temperaturaambiente"];
				t3.tx1_temperaturatx = results["tx1_temperaturatx"];

				t3.tx2_alarmageneral = results["tx2_alarmageneral"];
				t3.tx2_alarmapotencia = results["tx2_alarmapotencia"];
				t3.tx2_estado = results["tx2_estado"];
				t3.tx2_frecuencia = results["tx2_frecuencia"];
				t3.tx2_modulacion = results["tx2_modulacion"];
				t3.tx2_nombre = results["tx2_nombre"];
				t3.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t3.tx2_potenciareflejada = results["tx2_potenciareflejada"];
				t3.tx2_temperaturaambiente = results["tx2_temperaturaambiente"];
				t3.tx2_temperaturatx = results["tx2_temperaturatx"];

				t3.tx3_alarmageneral = results["tx3_alarmageneral"];
				t3.tx3_alarmapotencia = results["tx3_alarmapotencia"];
				t3.tx3_estado = results["tx3_estado"];
				t3.tx3_frecuencia = results["tx3_frecuencia"];
				t3.tx3_modulacion = results["tx3_modulacion"];
				t3.tx3_nombre = results["tx3_nombre"];
				t3.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t3.tx3_potenciareflejada = results["tx3_potenciareflejada"];
				t3.tx3_temperaturaambiente = results["tx3_temperaturaambiente"];
				t3.tx3_temperaturatx = results["tx3_temperaturatx"];

				t3.tx4_alarmageneral = results["tx4_alarmageneral"];
				t3.tx4_alarmapotencia = results["tx4_alarmapotencia"];
				t3.tx4_estado = results["tx4_estado"];
				t3.tx4_frecuencia = results["tx4_frecuencia"];
				t3.tx4_modulacion = results["tx4_modulacion"];
				t3.tx4_nombre = results["tx4_nombre"];
				t3.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t3.tx4_potenciareflejada = results["tx4_potenciareflejada"];
				t3.tx4_temperaturaambiente = results["tx4_temperaturaambiente"];
				t3.tx4_temperaturatx = results["tx4_temperaturatx"];

				t3.txr_alarmageneral = results["txr_alarmageneral"];
				t3.txr_alarmapotencia = results["txr_alarmapotencia"];
				t3.txr_estado = results["txr_estado"];
				t3.txr_frecuencia = results["txr_frecuencia"];
				t3.txr_modulacion = results["txr_modulacion"];
				t3.txr_nombre = results["txr_nombre"];
				t3.txr_potenciadirecta = results["txr_potenciadirecta"];
				t3.txr_potenciareflejada = results["txr_potenciareflejada"];
				t3.txr_temperaturaambiente = results["txr_temperaturaambiente"];
				t3.txr_temperaturatx = results["txr_temperaturatx"];

				#endregion

				#region Receptor

				t3.receptor_bitrate = results["receptor_bitrate"];
				t3.receptor_cn = results["receptor_cn"];
				t3.receptor_estado = results["receptor_estado"];
				t3.receptor_locked = results["receptor_locked"];
				t3.receptor_portadora = results["receptor_portadora"];
				t3.receptor_rf = results["receptor_rf"];
				t3.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				if (nivelCentro) // Solo actualiza la UI a nivel centro si estamos en ese nivel
				{
					UIReferences.Instance.UpdateBody(t3, tipo);
				}

				break;

			case "bbdd_4":

				TorreFM4 t4 = transform.Find("/Torres").Find("Torres FM4").Find(num_centro.ToString()).GetComponent<TorreFM4>();
				tipo = TipoTorre.FM4;

				t4.dia = DateTime.Parse(results["dia"]);
				t4.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t4.modem_estado = results["modem_estado"];
				t4.modem_nombre = results["modem_nombre"];
				t4.modem_rssi = results["modem_rssi"];
				t4.modem_datos_in = results["modem_datos_in"];
				t4.modem_datos_out = results["modem_datos_out"];
				t4.modem_consumo_in = results["modem_consumo_in"];
				t4.modem_consumo_out = results["modem_consumo_out"];
				t4.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t4.audio1_alarma = results["audio1_alarma"];
				t4.audio1_estado = results["audio1_estado"];
				t4.audio1_estado_RDS = results["audio1_estado_RDS"];
				t4.audio1_PID = results["audio1_PID"];


				t4.audio2_alarma = results["audio2_alarma"];
				t4.audio2_estado = results["audio2_estado"];
				t4.audio2_estado_RDS = results["audio2_estado_RDS"];
				t4.audio2_PID = results["audio2_PID"];

				t4.audio3_alarma = results["audio3_alarma"];
				t4.audio3_estado = results["audio3_estado"];
				t4.audio3_estado_RDS = results["audio3_estado_RDS"];
				t4.audio3_PID = results["audio3_PID"];

				t4.audio4_alarma = results["audio4_alarma"];
				t4.audio4_estado = results["audio4_estado"];
				t4.audio4_estado_RDS = results["audio4_estado_RDS"];
				t4.audio4_PID = results["audio4_PID"];


				#endregion

				#region Transmisores

				t4.tx1_estado = results["tx1_estado"];
				t4.tx1_nombre = results["tx1_nombre"];
				t4.tx1_frecuencia = results["tx1_frecuencia"];
				t4.tx1_potencia = results["tx1_potencia"];
				t4.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t4.tx1_potenciareflejada = results["tx1_potenciareflejada"];
				t4.tx1_modulacion = results["tx1_modulacion"];
				t4.tx1_estadotransmisor = results["tx1_estadotransmisor"];
				t4.tx1_alarmatransmisor = results["tx1_alarmatransmisor"];
				t4.tx1_atenuacion = results["tx1_atenuacion"];
				t4.tx1_alarmaatenuacion = results["tx1_alarmaatenuacion"];
				t4.tx1_estadoamplificador = results["tx1_estadoamplificador"];
				t4.tx1_temperaturatx = results["tx1_temperaturatx"];
				t4.tx1_modofuncionamiento = results["tx1_modofuncionamiento"];

				t4.tx2_estado = results["tx2_estado"];
				t4.tx2_nombre = results["tx2_nombre"];
				t4.tx2_frecuencia = results["tx2_frecuencia"];
				t4.tx2_potencia = results["tx2_potencia"];
				t4.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t4.tx2_potenciareflejada = results["tx2_potenciareflejada"];
				t4.tx2_modulacion = results["tx2_modulacion"];
				t4.tx2_estadotransmisor = results["tx2_estadotransmisor"];
				t4.tx2_alarmatransmisor = results["tx2_alarmatransmisor"];
				t4.tx2_atenuacion = results["tx2_atenuacion"];
				t4.tx2_alarmaatenuacion = results["tx2_alarmaatenuacion"];
				t4.tx2_estadoamplificador = results["tx2_estadoamplificador"];
				t4.tx2_temperaturatx = results["tx2_temperaturatx"];
				t4.tx2_modofuncionamiento = results["tx2_modofuncionamiento"];

				t4.tx3_estado = results["tx3_estado"];
				t4.tx3_nombre = results["tx3_nombre"];
				t4.tx3_frecuencia = results["tx3_frecuencia"];
				t4.tx3_potencia = results["tx3_potencia"];
				t4.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t4.tx3_potenciareflejada = results["tx3_potenciareflejada"];
				t4.tx3_modulacion = results["tx3_modulacion"];
				t4.tx3_estadotransmisor = results["tx3_estadotransmisor"];
				t4.tx3_alarmatransmisor = results["tx3_alarmatransmisor"];
				t4.tx3_atenuacion = results["tx3_atenuacion"];
				t4.tx3_alarmaatenuacion = results["tx3_alarmaatenuacion"];
				t4.tx3_estadoamplificador = results["tx3_estadoamplificador"];
				t4.tx3_temperaturatx = results["tx3_temperaturatx"];
				t4.tx3_modofuncionamiento = results["tx3_modofuncionamiento"];

				t4.tx4_estado = results["tx4_estado"];
				t4.tx4_nombre = results["tx4_nombre"];
				t4.tx4_frecuencia = results["tx4_frecuencia"];
				t4.tx4_potencia = results["tx4_potencia"];
				t4.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t4.tx4_potenciareflejada = results["tx4_potenciareflejada"];
				t4.tx4_modulacion = results["tx4_modulacion"];
				t4.tx4_estadotransmisor = results["tx4_estadotransmisor"];
				t4.tx4_alarmatransmisor = results["tx4_alarmatransmisor"];
				t4.tx4_atenuacion = results["tx4_atenuacion"];
				t4.tx4_alarmaatenuacion = results["tx4_alarmaatenuacion"];
				t4.tx4_estadoamplificador = results["tx4_estadoamplificador"];
				t4.tx4_temperaturatx = results["tx4_temperaturatx"];
				t4.tx4_modofuncionamiento = results["tx4_modofuncionamiento"];

				#endregion

				#region Receptor

				t4.receptor_bitrate = results["receptor_bitrate"];
				t4.receptor_cn = results["receptor_cn"];
				t4.receptor_estado = results["receptor_estado"];
				t4.receptor_locked = results["receptor_locked"];
				t4.receptor_portadora = results["receptor_portadora"];
				t4.receptor_rf = results["receptor_rf"];
				t4.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				if (nivelCentro)
				{
					UIReferences.Instance.UpdateBody(t4, tipo);
				}

				break;

			case "bbdd_5":

				TorreFM5 t5 = transform.Find("/Torres").Find("Torres FM5").Find(num_centro.ToString()).GetComponent<TorreFM5>();
				tipo = TipoTorre.FM5;

				t5.dia = DateTime.Parse(results["dia"]);
				t5.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t5.modem_estado = results["modem_estado"];
				t5.modem_nombre = results["modem_nombre"];
				t5.modem_rssi = results["modem_rssi"];
				t5.modem_datos_in = results["modem_datos_in"];
				t5.modem_datos_out = results["modem_datos_out"];
				t5.modem_consumo_in = results["modem_consumo_in"];
				t5.modem_consumo_out = results["modem_consumo_out"];
				t5.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t5.audio1_alarma = results["audio1_alarma"];
				t5.audio1_estado = results["audio1_estado"];
				t5.audio1_estado_RDS = results["audio1_estado_RDS"];
				t5.audio1_PID = results["audio1_PID"];


				t5.audio2_alarma = results["audio2_alarma"];
				t5.audio2_estado = results["audio2_estado"];
				t5.audio2_estado_RDS = results["audio2_estado_RDS"];
				t5.audio2_PID = results["audio2_PID"];

				t5.audio3_alarma = results["audio3_alarma"];
				t5.audio3_estado = results["audio3_estado"];
				t5.audio3_estado_RDS = results["audio3_estado_RDS"];
				t5.audio3_PID = results["audio3_PID"];

				t5.audio4_alarma = results["audio4_alarma"];
				t5.audio4_estado = results["audio4_estado"];
				t5.audio4_estado_RDS = results["audio4_estado_RDS"];
				t5.audio4_PID = results["audio4_PID"];

				#endregion

				#region Transmisores
				
				t5.tx1_estado = results["tx1_estado"];
				t5.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t5.tx1_potenciareflejada = results["tx1_potenciareflejada"]; ;
				t5.tx1_transmisorencendido = results["tx1_transmisorencendido"];
				t5.tx1_selecexcitador = results["tx1_selecexcitador"];
				t5.tx1_frecuenciaex1 = results["tx1_frecuenciaex1"]; ;
				t5.tx1_potenciadirectaexc1 = results["tx1_potenciadirectaexc1"];
				t5.tx1_potenciareflejadaexc1 = results["tx1_potenciareflejadaexc1"];
				t5.tx1_frecuenciaex2 = results["tx1_frecuenciaex2"];
				t5.tx1_potenciadirectaexc2 = results["tx1_potenciadirectaexc2"];
				t5.tx1_potenciareflejadaexc2 = results["tx1_potenciareflejadaexc2"];
				t5.tx1_temperatura = results["tx1_temperatura"];
				t5.tx1_estadotx = results["tx1_estadotx"];

				t5.tx2_estado = results["tx2_estado"];
				t5.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t5.tx2_potenciareflejada = results["tx2_potenciareflejada"]; ;
				t5.tx2_transmisorencendido = results["tx2_transmisorencendido"];
				t5.tx2_selecexcitador = results["tx2_selecexcitador"];
				t5.tx2_frecuenciaex1 = results["tx2_frecuenciaex1"]; ;
				t5.tx2_potenciadirectaexc1 = results["tx2_potenciadirectaexc1"];
				t5.tx2_potenciareflejadaexc1 = results["tx2_potenciareflejadaexc1"];
				t5.tx2_frecuenciaex2 = results["tx2_frecuenciaex2"];
				t5.tx2_potenciadirectaexc2 = results["tx2_potenciadirectaexc2"];
				t5.tx2_potenciareflejadaexc2 = results["tx2_potenciareflejadaexc2"];
				t5.tx2_temperatura = results["tx2_temperatura"];
				t5.tx2_estadotx = results["tx2_estadotx"];

				t5.tx3_estado = results["tx3_estado"];
				t5.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t5.tx3_potenciareflejada = results["tx3_potenciareflejada"]; ;
				t5.tx3_transmisorencendido = results["tx3_transmisorencendido"];
				t5.tx3_selecexcitador = results["tx3_selecexcitador"];
				t5.tx3_frecuenciaex1 = results["tx3_frecuenciaex1"]; ;
				t5.tx3_potenciadirectaexc1 = results["tx3_potenciadirectaexc1"];
				t5.tx3_potenciareflejadaexc1 = results["tx3_potenciareflejadaexc1"];
				t5.tx3_frecuenciaex2 = results["tx3_frecuenciaex2"];
				t5.tx3_potenciadirectaexc2 = results["tx3_potenciadirectaexc2"];
				t5.tx3_potenciareflejadaexc2 = results["tx3_potenciareflejadaexc2"];
				t5.tx3_temperatura = results["tx3_temperatura"];
				t5.tx3_estadotx = results["tx3_estadotx"];

				t5.tx4_estado = results["tx4_estado"];
				t5.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t5.tx4_potenciareflejada = results["tx4_potenciareflejada"]; ;
				t5.tx4_transmisorencendido = results["tx4_transmisorencendido"];
				t5.tx4_selecexcitador = results["tx4_selecexcitador"];
				t5.tx4_frecuenciaex1 = results["tx4_frecuenciaex1"]; ;
				t5.tx4_potenciadirectaexc1 = results["tx4_potenciadirectaexc1"];
				t5.tx4_potenciareflejadaexc1 = results["tx4_potenciareflejadaexc1"];
				t5.tx4_frecuenciaex2 = results["tx4_frecuenciaex2"];
				t5.tx4_potenciadirectaexc2 = results["tx4_potenciadirectaexc2"];
				t5.tx4_potenciareflejadaexc2 = results["tx4_potenciareflejadaexc2"];
				t5.tx4_temperatura = results["tx4_temperatura"];
				t5.tx4_estadotx = results["tx4_estadotx"];

				t5.txr_estado = results["txr_estado"];
				t5.txr_potenciadirecta = results["txr_potenciadirecta"];
				t5.txr_potenciareflejada = results["txr_potenciareflejada"]; ;
				t5.txr_transmisorencendido = results["txr_transmisorencendido"];
				t5.txr_selecexcitador = results["txr_selecexcitador"];
				t5.txr_frecuenciaex1 = results["txr_frecuenciaex1"]; ;
				t5.txr_potenciadirectaexc1 = results["txr_potenciadirectaexc1"];
				t5.txr_potenciareflejadaexc1 = results["txr_potenciareflejadaexc1"];
				t5.txr_frecuenciaex2 = results["txr_frecuenciaex2"];
				t5.txr_potenciadirectaexc2 = results["txr_potenciadirectaexc2"];
				t5.txr_potenciareflejadaexc2 = results["txr_potenciareflejadaexc2"];
				t5.txr_temperatura = results["txr_temperatura"];
				t5.txr_estadotx = results["txr_estadotx"];


				#endregion

				#region Receptor

				t5.receptor_bitrate = results["receptor_bitrate"];
				t5.receptor_cn = results["receptor_cn"];
				t5.receptor_estado = results["receptor_estado"];
				t5.receptor_locked = results["receptor_locked"];
				t5.receptor_portadora = results["receptor_portadora"];
				t5.receptor_rf = results["receptor_rf"];
				t5.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				#region UCA

				t5.uca_estado = results["uca_estado"];

				t5.uca_estadopotenciatx1 = results["uca_estadopotenciatx1"];
				t5.uca_tipopotenciatx1 = results["uca_tipopotenciatx1"];
				t5.uca_estadotx1 = results["uca_estadotx1"];

				t5.uca_estadopotenciatx2 = results["uca_estadopotenciatx2"];
				t5.uca_tipopotenciatx2 = results["uca_tipopotenciatx2"];
				t5.uca_estadotx2 = results["uca_tipopotenciatx2"];

				t5.uca_estadopotenciatx3 = results["uca_tipopotenciatx3"];
				t5.uca_tipopotenciatx3 = results["uca_tipopotenciatx3"];
				t5.uca_estadotx3 = results["uca_estadotx3"];

				t5.uca_estadopotenciatx4 = results["uca_estadopotenciatx4"];
				t5.uca_tipopotenciatx4 = results["uca_tipopotenciatx4"];
				t5.uca_estadotx4 = results["uca_estadotx4"];

				t5.uca_estadopotenciatxr = results["uca_estadopotenciatxr"];
				t5.uca_tipopotenciatxr = results["uca_tipopotenciatxr"];
				t5.uca_estadotxr = results["uca_estadotxr"];

				t5.uca_estadosistema = results["uca_estadosistema"];
				t5.uca_estadocarga = results["uca_estadocarga"];
				t5.uca_estadouca = results["uca_estadouca"];

				#endregion

				if (nivelCentro)
				{
					UIReferences.Instance.UpdateBody(t5, tipo);
				}

				break;

			case "bbdd_6":

				TorreFM6 t6 = transform.Find("/Torres").Find("Torres FM6").Find(num_centro.ToString()).GetComponent<TorreFM6>();
				tipo = TipoTorre.FM6;

				t6.dia = DateTime.Parse(results["dia"]);
				t6.hora = DateTime.Parse(results["hora"]);

				#region Modem 

				t6.modem_estado = results["modem_estado"];
				t6.modem_nombre = results["modem_nombre"];
				t6.modem_rssi = results["modem_rssi"];
				t6.modem_datos_in = results["modem_datos_in"];
				t6.modem_datos_out = results["modem_datos_out"];
				t6.modem_consumo_in = results["modem_consumo_in"];
				t6.modem_consumo_out = results["modem_consumo_out"];
				t6.texto_alarma = results["texto_alarma"];

				#endregion

				#region Audio

				t6.audio1_alarma = results["audio1_alarma"];
				t6.audio1_estado = results["audio1_estado"];
				t6.audio1_estado_RDS = results["audio1_estado_RDS"];
				t6.audio1_PID = results["audio1_PID"];


				t6.audio2_alarma = results["audio2_alarma"];
				t6.audio2_estado = results["audio2_estado"];
				t6.audio2_estado_RDS = results["audio2_estado_RDS"];
				t6.audio2_PID = results["audio2_PID"];

				t6.audio3_alarma = results["audio3_alarma"];
				t6.audio3_estado = results["audio3_estado"];
				t6.audio3_estado_RDS = results["audio3_estado_RDS"];
				t6.audio3_PID = results["audio3_PID"];

				t6.audio4_alarma = results["audio4_alarma"];
				t6.audio4_estado = results["audio4_estado"];
				t6.audio4_estado_RDS = results["audio4_estado_RDS"];
				t6.audio4_PID = results["audio4_PID"];

				#endregion

				#region Receptor

				t6.receptor_bitrate = results["receptor_bitrate"];
				t6.receptor_cn = results["receptor_cn"];
				t6.receptor_estado = results["receptor_estado"];
				t6.receptor_locked = results["receptor_locked"];
				t6.receptor_portadora = results["receptor_portadora"];
				t6.receptor_rf = results["receptor_rf"];
				t6.receptor_temperatura = results["receptor_temperatura"];

				#endregion

				#region Transmisores

				t6.tx1_estado = results["tx1_estado"];
				t6.tx1_power = results["tx1_power"];
				t6.tx1_frecuencia = results["tx1_frecuencia"];
				t6.tx1_potenciadirecta = results["tx1_potenciadirecta"];
				t6.tx1_potenciareflejada = results["tx1_potenciareflejada"];
				t6.tx1_desviacion = results["tx1_desviacion"];
				t6.tx1_canalizquierdo = results["tx1_canalizquierdo"];
				t6.tx1_canalderecho = results["tx1_canalderecho"];
				t6.tx1_entradaaudio = results["tx1_entradaaudio"];
				t6.tx1_muteaudio = results["tx1_muteaudio"];
				t6.tx1_estadopll = results["tx1_estadopll"];
				t6.tx1_foldback = results["tx1_foldback"];
				t6.tx1_tensionvpa = results["tx1_tensionvpa"];
				t6.tx1_corrienteipa = results["tx1_corrienteipa"];
				t6.tx1_eficiencia = results["tx1_eficiencia"];
				t6.tx1_temperatura = results["tx1_temperatura"];

				t6.tx2_estado = results["tx2_estado"];
				t6.tx2_power = results["tx2_power"];
				t6.tx2_frecuencia = results["tx2_frecuencia"];
				t6.tx2_potenciadirecta = results["tx2_potenciadirecta"];
				t6.tx2_potenciareflejada = results["tx2_potenciareflejada"];
				t6.tx2_desviacion = results["tx2_desviacion"];
				t6.tx2_canalizquierdo = results["tx2_canalizquierdo"];
				t6.tx2_canalderecho = results["tx2_canalderecho"];
				t6.tx2_entradaaudio = results["tx2_entradaaudio"];
				t6.tx2_muteaudio = results["tx2_muteaudio"];
				t6.tx2_estadopll = results["tx2_estadopll"];
				t6.tx2_foldback = results["tx2_foldback"];
				t6.tx2_tensionvpa = results["tx2_tensionvpa"];
				t6.tx2_corrienteipa = results["tx2_corrienteipa"];
				t6.tx2_eficiencia = results["tx2_eficiencia"];
				t6.tx2_temperatura = results["tx2_temperatura"];

				t6.tx3_estado = results["tx3_estado"];
				t6.tx3_power = results["tx3_power"];
				t6.tx3_frecuencia = results["tx3_frecuencia"];
				t6.tx3_potenciadirecta = results["tx3_potenciadirecta"];
				t6.tx3_potenciareflejada = results["tx3_potenciareflejada"];
				t6.tx3_desviacion = results["tx3_desviacion"];
				t6.tx3_canalizquierdo = results["tx3_canalizquierdo"];
				t6.tx3_canalderecho = results["tx3_canalderecho"];
				t6.tx3_entradaaudio = results["tx3_entradaaudio"];
				t6.tx3_muteaudio = results["tx3_muteaudio"];
				t6.tx3_estadopll = results["tx3_estadopll"];
				t6.tx3_foldback = results["tx3_foldback"];
				t6.tx3_tensionvpa = results["tx3_tensionvpa"];
				t6.tx3_corrienteipa = results["tx3_corrienteipa"];
				t6.tx3_eficiencia = results["tx3_eficiencia"];
				t6.tx3_temperatura = results["tx3_temperatura"];

				t6.tx4_estado = results["tx4_estado"];
				t6.tx4_power = results["tx4_power"];
				t6.tx4_frecuencia = results["tx4_frecuencia"];
				t6.tx4_potenciadirecta = results["tx4_potenciadirecta"];
				t6.tx4_potenciareflejada = results["tx4_potenciareflejada"];
				t6.tx4_desviacion = results["tx4_desviacion"];
				t6.tx4_canalizquierdo = results["tx4_canalizquierdo"];
				t6.tx4_canalderecho = results["tx4_canalderecho"];
				t6.tx4_entradaaudio = results["tx4_entradaaudio"];
				t6.tx4_muteaudio = results["tx4_muteaudio"];
				t6.tx4_estadopll = results["tx4_estadopll"];
				t6.tx4_foldback = results["tx4_foldback"];
				t6.tx4_tensionvpa = results["tx4_tensionvpa"];
				t6.tx4_corrienteipa = results["tx4_corrienteipa"];
				t6.tx4_eficiencia = results["tx4_eficiencia"];
				t6.tx4_temperatura = results["tx4_temperatura"];

				t6.txr_estado = results["txr_estado"];
				t6.txr_power = results["txr_power"];
				t6.txr_frecuencia = results["txr_frecuencia"];
				t6.txr_potenciadirecta = results["txr_potenciadirecta"];
				t6.txr_potenciareflejada = results["txr_potenciareflejada"];
				t6.txr_desviacion = results["txr_desviacion"];
				t6.txr_canalizquierdo = results["txr_canalizquierdo"];
				t6.txr_canalderecho = results["txr_canalderecho"];
				t6.txr_entradaaudio = results["txr_entradaaudio"];
				t6.txr_muteaudio = results["txr_muteaudio"];
				t6.txr_estadopll = results["txr_estadopll"];
				t6.txr_foldback = results["txr_foldback"];
				t6.txr_tensionvpa = results["txr_tensionvpa"];
				t6.txr_corrienteipa = results["txr_corrienteipa"];
				t6.txr_eficiencia = results["txr_eficiencia"];
				t6.txr_temperatura = results["txr_temperatura"];

				#endregion

				#region UCA

				t6.uca_estado = results["uca_estado"];

				t6.uca_estadopotenciatx1 = results["uca_estadopotenciatx1"];
				t6.uca_tipopotenciatx1 = results["uca_tipopotenciatx1"];
				t6.uca_estadotx1 = results["uca_estadotx1"];

				t6.uca_estadopotenciatx2 = results["uca_estadopotenciatx2"];
				t6.uca_tipopotenciatx2 = results["uca_tipopotenciatx2"];
				t6.uca_estadotx2 = results["uca_tipopotenciatx2"];

				t6.uca_estadopotenciatx3 = results["uca_tipopotenciatx3"];
				t6.uca_tipopotenciatx3 = results["uca_tipopotenciatx3"];
				t6.uca_estadotx3 = results["uca_estadotx3"];

				t6.uca_estadopotenciatx4 = results["uca_estadopotenciatx4"];
				t6.uca_tipopotenciatx4 = results["uca_tipopotenciatx4"];
				t6.uca_estadotx4 = results["uca_estadotx4"];

				t6.uca_estadopotenciatxr = results["uca_estadopotenciatxr"];
				t6.uca_tipopotenciatxr = results["uca_tipopotenciatxr"];
				t6.uca_estadotxr = results["uca_estadotxr"];

				t6.uca_estadosistema = results["uca_estadosistema"];
				t6.uca_estadocarga = results["uca_estadocarga"];
				t6.uca_estadouca = results["uca_estadouca"];

				#endregion

				if (nivelCentro)
				{
					UIReferences.Instance.UpdateBody(t6, tipo);
				}

				break;

			default:
				break;
				
		}

		systemTime = DateTime.Now;

		//if (!UIReferences.Instance.UINivelCentro.activeSelf)
		if (GlobalController.Instance.SelectedTower == null)
		{
			//Debug.Log("break");
			nivelCentroSynced = false;
			yield break;
		}

		if (!nivelCentroSynced)
		{

			lastUpdate = DateTime.Parse(results["hora"]);

			// Para la proxima actualizacion cogemos la hora actual solo con los segundos de la ultima actualización + 30 segundos.

			nextUpdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, lastUpdate.Second).AddSeconds(32);
			

			TimeSpan span = nextUpdate - systemTime;
			wait = span.Seconds;

			if (GlobalController.Instance.platform == Platform.PC)
			{
				if(timerGO.TryGetComponent(out DataTimer timer))
				{
					timerGO.SetActive(true);
					timer.SetTimer(wait + 1);
				}
			
			}

			yield return new WaitForSeconds(wait+1);

			nivelCentroSynced = true;
			
			StartCoroutine(GetData(num_centro, nivelCentro));
		}
		else
		{
			wait = 30f;

			if (GlobalController.Instance.platform == Platform.PC)
			{
				if (timerGO.TryGetComponent(out DataTimer timer))
				{
					timerGO.SetActive(true);
					timer.SetTimer(wait);
				}
			}

			yield return new WaitForSeconds(wait);
			StartCoroutine(GetData(num_centro, nivelCentro));
		}	

	}

	public void GetEstados()
	{
		StartCoroutine(GetEstadoCentros());
	}

	public IEnumerator GetEstadoCentros()
    {
        if (!isAuth)
            yield return new WaitUntil(() => isAuth);

		isAssigning = true;

		float wait = 0f;

		string url = "http://centrosemisores.rtve.es:7600/api/get-centros-comunidad/00";

        UnityWebRequest www = new UnityWebRequest(url);
        www.SetRequestHeader("access-token", token);
        www.downloadHandler = new DownloadHandlerBuffer(); //Obtenemos la informacion del request

        

        yield return www.SendWebRequest(); //Devolvemos la web request

		if (www.error == null) // Si no devuelve error
		{
			var jsonParsed = JSON.Parse(www.downloadHandler.text); // Parseamos el JSON
			var results = jsonParsed["results"]; // Cogemos solo el result

			string estadoCentros = results.ToString(); // Lo almacenamos en una var como string
													   //Debug.Log(estadoCentros);
			AssignEstados(estadoCentros);


			systemTime = DateTime.Now;

			if (!datosSynced)
			{
				lastUpdate = DateTime.Parse(results[0]["hora"]);
				nextUpdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, lastUpdate.Second);
				nextUpdate = nextUpdate.AddSeconds(31);

				TimeSpan span = nextUpdate - systemTime;
				wait = span.Seconds;

				datosSynced = true;
			}
			else
			{
				wait = 30f;
			}
		}
		else // Si devuelve error
		{
			Debug.Log(www.error);
			//towerData = www.error.ToString();
		}

		yield return new WaitForSeconds(wait + 2);

		StartCoroutine(GetEstadoCentros());
    }

    private void AssignEstados(string estadoCentros)
    {
		JSONNode jsonParsed = JSON.Parse(estadoCentros);

		foreach (JSONNode centro in jsonParsed)
        {
            int num_centro = centro["codigo_centro"];

			if (num_centro == 57 || num_centro == 159)
            {
				//Debug.Log("num_centro");
            }

			else
            {
				//Debug.Log($"{num_centro}: {centro["estado_centro"]}");
				string tipoCentro = GlobalController.Instance.GetCentro(num_centro).bbdd;

				switch (tipoCentro)
				{
					case "bbdd":

						//TorreOM t0 = transform.Find("/Torres").Find("Torres OM").Find(num_centro.ToString()).GetComponent<TorreOM>();

						if(transform.Find("/Torres").Find("Torres OM").Find(num_centro.ToString()).TryGetComponent(out TorreOM t0))
						{
							t0.estado = centro["estado_centro"];
						}

						

						break;

					case "bbdd_1":

						

						if(transform.Find("/Torres").Find("Torres FM1").Find(num_centro.ToString()).TryGetComponent(out TorreFM1 t1))
						{
							t1.estado = centro["estado_centro"];
							t1.estado_transmisor1 = centro["estado_tx1"];
							t1.estado_transmisor2 = centro["estado_tx2"];
							t1.estado_transmisor3 = centro["estado_tx3"];
							t1.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					case "bbdd_2":

						if (transform.Find("/Torres").Find("Torres FM2").Find(num_centro.ToString()).TryGetComponent(out TorreFM1 t2))
						{
							t2.estado = centro["estado_centro"];
							t2.estado_transmisor1 = centro["estado_tx1"];
							t2.estado_transmisor2 = centro["estado_tx2"];
							t2.estado_transmisor3 = centro["estado_tx3"];
							t2.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					case "bbdd_3":

						if(transform.Find("/Torres").Find("Torres FM3").Find(num_centro.ToString()).TryGetComponent(out TorreFM3 t3))
						{
							t3.estado = centro["estado_centro"];
							t3.estado_transmisor1 = centro["estado_tx1"];
							t3.estado_transmisor2 = centro["estado_tx2"];
							t3.estado_transmisor3 = centro["estado_tx3"];
							t3.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					case "bbdd_4":

						if(transform.Find("/Torres").Find("Torres FM4").Find(num_centro.ToString()).TryGetComponent(out TorreFM4 t4))
						{
							t4.estado = centro["estado_centro"];
							t4.estado_transmisor1 = centro["estado_tx1"];
							t4.estado_transmisor2 = centro["estado_tx2"];
							t4.estado_transmisor3 = centro["estado_tx3"];
							t4.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					case "bbdd_5":

						if(transform.Find("/Torres").Find("Torres FM5").Find(num_centro.ToString()).TryGetComponent(out TorreFM5 t5))
						{
							t5.estado = centro["estado_centro"];
							t5.estado_transmisor1 = centro["estado_tx1"];
							t5.estado_transmisor2 = centro["estado_tx2"];
							t5.estado_transmisor3 = centro["estado_tx3"];
							t5.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					case "bbdd_6":

						if(transform.Find("/Torres").Find("Torres FM6").Find(num_centro.ToString()).TryGetComponent(out TorreFM6 t6))
						{
							t6.estado = centro["estado_centro"];
							t6.estado_transmisor1 = centro["estado_tx1"];
							t6.estado_transmisor2 = centro["estado_tx2"];
							t6.estado_transmisor3 = centro["estado_tx3"];
							t6.estado_transmisor4 = centro["estado_tx4"];
						}

						break;

					default:
						break;

				}
			}
            //TorreBase tC = transform.Find("/Torres").Find("Torres FM1").Find(num_centro).GetComponent<TorreBase>();
        }
		isAssigning = false;
    }

    public string GetEstadoCentrosAsString()
    {
        return estadoCentros.text;
    }


	public void QuitAPP()
	{
		token = null;
		isAuth = false;
		Instance.isEndLogin = false;
		usuario = null;
		password = null;
		nivel = null;
		apellido = null;

		Application.Quit();
	}

	public void Logout()
	{
		token = null;
		isAuth = false;
		Instance.isEndLogin = false;
		usuario = null;
		password = null;
		nivel = null;
		apellido = null;

		SceneManager.LoadScene(0);
	}
}