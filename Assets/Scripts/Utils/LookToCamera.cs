﻿using Microsoft.Maps.Unity.Search;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LookToCamera : MonoBehaviour
{
	
	Transform camTransform; // Referencia a la camara


	private void Awake()
	{
		camTransform = Camera.main.transform; // Asignamos referencia
	}

	private void LateUpdate() // Se ejecuta despues de Update()
	{
		transform.LookAt(transform.position + camTransform.transform.rotation * Vector3.forward, camTransform.transform.rotation * Vector3.up);
	}
}
