﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SimpleJSON;
using System.IO;
using UnityEngine.UI;
using System.Linq;

public class TreeView : MonoBehaviour
{
    string path;
    string jsonString;
    // Se declaran los 3 prefabs que se van a usar en este script
    public GameObject ComunidadTemplate = null;
    public GameObject ProvinciaTemplate = null;
    public GameObject TorreTemplate = null;

    void Start()
    {
        // se lee el json, se pasa a string y se parsea
        path = Application.streamingAssetsPath + "/rtve_data.json"; // el json está hecho a medida a partir de los datos de RTVE, tiene un formato de [{"nombre": "comunidad", "provincias": [{"nombre": "provincia", "centros": [{"num_centro": 61, "nombre": "torrespaña"}]}]}]
        jsonString = File.ReadAllText(path);
        JSONNode jsonParsed = JSON.Parse(jsonString);
		// itera por las comunidades

		//rtveAPI apiRequest = GameObject.Find("APIReaders").GetComponent<rtveAPI>();
        //apiRequest.Login();
        //apiRequest.GetData(76);

        foreach (var com in jsonParsed)
        {
            SpawnComunidad(com.Value["nombre"]); // se pasa el parámetro de Key ya que se necesita el nombre de la comunidad para usarlo como texto en la función
            //// itera por los elementos que hay dentro de cada comunidad
            foreach (var pro in com.Value["provincias"]) // se usa Value porque se quiere iterar porque el valor de cada comunidad es un objeto con key: nombre de la provincia (string), value: array de torres (list<string>)
            {
                SpawnProvincia(pro.Value["nombre"]); // parámetro el string del nombre de la provincia
                SpawnTorre(pro.Value["centros"]); // parámetro el array de nombres de torres
            }
        }
    }

    private void SpawnComunidad(string nombre)
    {
        GameObject go;
        go = Instantiate(ComunidadTemplate);
        go.transform.SetParent(transform, false); // coloca el prefab dentro del gameobject "Content"
        go.transform.localScale = new Vector3(1, 1, 1); // Escala el prefab a 1,1,1 por alguna razón me salía más grande del normal

        var comunidad = gameObject.transform.GetChild(gameObject.transform.childCount - 1).GetChild(0).GetChild(2).gameObject; // selecciona el gameobject de ComunidadTemplate

        TextMeshProUGUI cText = comunidad.GetComponent<TextMeshProUGUI>(); // selecciona el texto de la comunidad

        gameObject.transform.GetChild(gameObject.transform.childCount - 1).name = nombre; // cambia el nombre del objeto, no es obligatorio, pero ayuda a tener ordenado el indice
        cText.text = nombre.ToString(); // cambia el nombre del texto
    }
    private void SpawnProvincia(string nombre)
    {
        GameObject go;
        go = Instantiate(ProvinciaTemplate);
        go.transform.SetParent(transform.GetChild(gameObject.transform.childCount - 1), false); // coloca el prefab dentro del gameobject "Content/Comunidad"
        go.transform.localScale = new Vector3(1, 1, 1); // Escala el prefab a 1,1,1 por alguna razón me salía más grande del normal

        var comunidad = gameObject.transform.GetChild(gameObject.transform.childCount - 1); // selecciona el gameobject de ComunidadTemplate

        TextMeshProUGUI cText = comunidad.GetChild(comunidad.childCount - 1).GetChild(0).GetChild(2).gameObject.GetComponent<TextMeshProUGUI>(); // selecciona el texto de la última provincia creada
        cText.text = nombre.ToString();

        Button expandButton = gameObject.transform.GetChild(gameObject.transform.childCount - 1).GetChild(0).GetChild(0).gameObject.GetComponent<Button>(); // selecciona el botón expand de la última comunidad creada
        Button shrinkButton = gameObject.transform.GetChild(gameObject.transform.childCount - 1).GetChild(0).GetChild(1).gameObject.GetComponent<Button>(); // selecciona el botón shrink de la última comunidad creada

        GameObject provincia = comunidad.GetChild(comunidad.childCount - 1).gameObject;
        // Selecciona el objeto de la provincia que se acaba de crear para posteriormente mostrarla u ocultarla
        provincia.name = nombre;

        expandButton.onClick.AddListener(() => ShowProvincia(provincia));
        shrinkButton.onClick.AddListener(() => HideProvincia(provincia));
    }
    private void SpawnTorre(JSONNode torres)
    {
        var comunidad = transform.GetChild(gameObject.transform.childCount - 1);

        Button expandButton = comunidad.GetChild(comunidad.childCount - 1).GetChild(0).GetChild(0).gameObject.GetComponent<Button>(); // selecciona el botón expand de la última comunidad creada
        Button shrinkButton = comunidad.GetChild(comunidad.childCount - 1).GetChild(0).GetChild(1).gameObject.GetComponent<Button>(); // selecciona el botón shrink de la última comunidad creada

        foreach (JSONNode torre in torres)
        {
            GameObject go;
            go = Instantiate(TorreTemplate); // spawnea el prefab
            go.transform.SetParent(comunidad.GetChild(comunidad.childCount - 1), false); // coloca el prefab dentro del gameobject "Content/Comunidad/Provincia"
            go.transform.localScale = new Vector3(1, 1, 1); // Escala el prefab a 1,1,1 por alguna razón me salía más grande del normal

            var nombre = torre["nombre"];
            var num_centro = torre["num_centro"];
            var bbdd = torre["bbdd"];

            TextMeshProUGUI cText = go.transform.parent.GetChild(go.transform.parent.childCount - 1).GetChild(0).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>(); // selecciona el elemento de texto del objeto
            cText.text = nombre.ToString().Trim('"'); // cambia el texto de la torre

            Button torreButton = go.transform.parent.GetChild(go.transform.parent.childCount - 1).GetChild(0).GetChild(1).gameObject.GetComponent<Button>();

            GameObject torreObjeto = go.transform.parent.GetChild(go.transform.parent.childCount - 1).gameObject; // Selecciona el objeto de la torreTemplate
            torreObjeto.name = nombre.ToString(); // cambia el nombre del objeto, no es obligatorio, pero ayuda a tener ordenado el indice

            expandButton.onClick.AddListener(() => ShowProvincia(torreObjeto)); // Establece qué elemento debe mostrar al clickar
            shrinkButton.onClick.AddListener(() => HideProvincia(torreObjeto)); // Establece qué elemento debe ocultar al clickar
        }
    }

    void ShowProvincia(GameObject provincia)
    {
        provincia.SetActive(true); // muestra el objeto provincia
    }
    void HideProvincia(GameObject provincia)
    {
        provincia.SetActive(false); // oculta el objeto provincia
    }
    //void jsonToClass(var torreCase){
    //    torreCase.codigo_centro = num_centro;
    //    torreCase.bbdd = bbdd;
    //    torreCase.lat = lat;
    //    torreCase.lon = lon;
    //    torreCase.propietario = propietario;
    //}
}