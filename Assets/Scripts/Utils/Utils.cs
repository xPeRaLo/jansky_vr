﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Utils
{
	namespace Enums
	{
		public enum AnimacionUI
		{
			Open_pais,
			Close_Pais,
			Open_Comunidad,
			Close_Comunidad,
			Open_Centro,
			Close_Centro,
			ComunidadCentro

		}

		public enum AnimacionComunidad
		{
			Andalucia,
			Aragon,
			Asturias,
			Baleares,
			Cantabria,
			Canarias,
			CastillaLeon,
			CastillaLaMancha,
			Catalunya,
			Valencia,
			Extremadura,
			Galicia,
			Madrid,
			Murcia,
			Navarra,
			PaisVasco,
			LaRioja
		}

		public enum Platform
		{
			PC,
			VR
		}
	}

	namespace FileSystem
	{
		public class FileSystem
		{
			

			public static void ExportData<Data>(Data data,string fileName = "export", string extension = "m1")
			{
				string destination = Application.persistentDataPath + "/" + fileName + "." + extension;
				FileStream file;

				file = File.Create(destination);

				/*UserData data = new UserData()
				{
					usuario = rtveAPI.usuario,
					password = "",
					nombre = "",
					nivel = "",
					isAuth = rtveAPI.isAuth,
					token = rtveAPI.token
				};

				if (rememberPassword)
				{
					data.password = rtveAPI.password;
					data.nombre = rtveAPI.nombre;
					data.nivel = rtveAPI.nivel;
				}*/



				BinaryFormatter bf = new BinaryFormatter();
				bf.Serialize(file, data);

				file.Close();
			}

			public static Data ImportData<Data>(string fileName = "export", string extension = "m1")
			{
				string destination = Application.persistentDataPath + "/" + fileName + "." + extension; ;
				FileStream file;


				if (File.Exists(destination))
					file = File.OpenRead(destination);
				else
					return default;
				

				BinaryFormatter bf = new BinaryFormatter();
				Data data = (Data)bf.Deserialize(file);

				file.Close();

				/*rtveAPI.usuario = data.usuario;
				rtveAPI.password = data.password;
				rtveAPI.nivel = data.nivel;
				rtveAPI.nombre = data.nombre;*/

				return data;
			}

			public static void DeleteUserData()
			{
				string destination = Application.persistentDataPath + "/export.m1";

				if (File.Exists(destination))
					File.Delete(destination);
			}
		}
	}
}



