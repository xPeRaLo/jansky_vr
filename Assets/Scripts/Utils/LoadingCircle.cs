﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingCircle : MonoBehaviour
{
	private RectTransform rectComponent;
	private float rotateSpeed = 325f;

	bool loading = false;

	const float MAX_TIMER = 15f;
	float timer = MAX_TIMER;

	AudioSource source;

	private void Awake()
	{
		source = GlobalController.Instance.GetRadio().GetComponent<AudioSource>();
	}

	private void Start()
	{
		rectComponent = GetComponent<RectTransform>();
	}

	private void Update()
	{
		rectComponent.Rotate(0f, 0f, -rotateSpeed * Time.deltaTime);

		if (loading)
		{

			if (source.clip == null)
			{

				timer -= Time.deltaTime;

				if (timer <= 0)
				{
					GlobalController.Instance.StopRadio();
					UIReferences.Instance.btnPlay.gameObject.SetActive(true);

					loading = false;
					timer = MAX_TIMER;

					transform.parent.gameObject.SetActive(false);
				}
			}
			else
			{
				loading = false;
				timer = MAX_TIMER;

				UIReferences.Instance.btnPlay.gameObject.SetActive(false);
				UIReferences.Instance.btnStop.gameObject.SetActive(true);

				transform.parent.gameObject.SetActive(false);
			}
		}
	}

	private void OnEnable()
	{
		loading = true;

	}

	private void OnDisable()
	{
		loading = false;
		timer = MAX_TIMER;
	}

}
