﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_InputField))]
public class pwdInputFieldUtils : MonoBehaviour
{
	TMP_InputField field;

	private void Awake()
	{
		field = GetComponent<TMP_InputField>();
	}

	public void Hide()
	{
		field.inputType = TMP_InputField.InputType.Password;
		field.ForceLabelUpdate();
	}

	public void Show()
	{
		field.inputType = TMP_InputField.InputType.Standard;
		field.ForceLabelUpdate();
	}
}
