﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SimpleJSON;
using System.IO;
using UnityEngine.UI;
using System;

public class UserInfo : MonoBehaviour
{
    public GameObject nombre;
    public GameObject nivel;

    public string nombreTexto;
    public string nivelTexto;

    public Button logoutButton;
    public GameObject loginWindow;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(UpdateUserInfo());

        //logoutButton = transform.Find("BTN_LOGOUT").GetComponent<Button>();

        //logoutButton.onClick.AddListener(() => rtveAPI.Instance.Logout());
    }

    private IEnumerator UpdateUserInfo()
    {
        if (!rtveAPI.isAuth)
            yield return new WaitUntil(() => rtveAPI.isAuth);

        nombre = transform.Find("NOMBRE_USUARIO").gameObject;
        nombreTexto = rtveAPI.nombre + " " + rtveAPI.apellido;
        nombre.GetComponent<TextMeshProUGUI>().text = nombreTexto;

        //nivel = transform.GetChild(1).gameObject;
        //nivelTexto = GlobalController.Instance.TraducirComunidad(rtveAPI.Instance.nivel);
        //nivel.gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = nivelTexto;
    }
}
