﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using SimpleJSON;
using UnityEngine.SceneManagement;
using Utils.FileSystem;

public class Login : MonoBehaviour
{

    void Awake()
    {
		UIReferences.Instance.LoginButton.onClick.AddListener(() =>
		{
			StartCoroutine(rtveAPI.Instance.Login(UIReferences.Instance.UsuarioInput.text, UIReferences.Instance.PasswordInput.text));
		});


        UIReferences.Instance.LoginButton.onClick.AddListener(() => ShowLogin());
    }

	private void Start()
	{
		UserData data = FileSystem.ImportData<UserData>();

		if (data != null)
		{
			UIReferences.Instance.UsuarioInput.text = data.usuario;
			UIReferences.Instance.PasswordInput.text = data.password;

			if (data.password != "")
				UIReferences.Instance.passwordToggle.isOn = true;
			else
				UIReferences.Instance.passwordToggle.isOn = false;
		}
	}

	private void Update()
	{
		/*if (Input.GetKeyDown(KeyCode.G))
		{
			UIReferences.Instance.LoginButton.onClick.Invoke();
		}

        if (Input.GetKeyDown(KeyCode.H))
        {
            UIReferences.Instance.DismissButton.onClick.Invoke();
        }*/
    }

	public void ShowLogin()
    {
		StartCoroutine(FillWarningText());
    }

    private IEnumerator FillWarningText()
    {
		yield return new WaitUntil(() => rtveAPI.Instance.isEndLogin);

        var jsonParsed = JSON.Parse(rtveAPI.Instance.downloadHandler);

        string mensaje = jsonParsed["mensaje"];

        if (rtveAPI.token == null || rtveAPI.token == "")
        {
            TextMeshProUGUI texto = UIReferences.Instance.WarningText.GetComponentInChildren<TextMeshProUGUI>();
            texto.text = "Usuario o contraseña incorrecta.";

			#region Text Animation 

			if (!texto.transform.parent.gameObject.activeSelf)
			{

				texto.transform.parent.gameObject.SetActive(true);
				texto.color = new Color(texto.color.r, texto.color.g, texto.color.b, 0);
				
				for(float i = 0; i < 1; i+= Time.deltaTime * 1.5f)
				{
					texto.color = new Color(texto.color.r, texto.color.g, texto.color.b, i);
					yield return null;
				}

				texto.color = new Color(texto.color.r, texto.color.g, texto.color.b, 1);

				for (float i = 1; i > 0; i -= Time.deltaTime * 1.5f)
				{
					texto.color = new Color(texto.color.r, texto.color.g, texto.color.b, i);
					yield return null;
				}

				texto.color = new Color(texto.color.r, texto.color.g, texto.color.b, 0);
				texto.transform.parent.gameObject.SetActive(false);
			}

			#endregion

		}
		else
        {
            TextMeshProUGUI texto = UIReferences.Instance.SuccessText.GetComponentInChildren<TextMeshProUGUI>();
            texto.text = mensaje;

			UIReferences.Instance.UsuarioInput.interactable = false;
			UIReferences.Instance.PasswordInput.interactable = false;
			UIReferences.Instance.LoginButton.interactable = false;
			UIReferences.Instance.SuccessText.SetActive(true);
			UIReferences.Instance.WarningText.SetActive(false);
			UIReferences.Instance.passwordToggle.interactable = false;
			UIReferences.Instance.LoginButton.interactable = false;

			Load();
		}
    }

	void Load()
	{
		
		if (GlobalController.Instance.platform == Utils.Enums.Platform.PC)
			StartCoroutine(LoadYourAsyncScene(1));
		else
			LoadScene(1);
		
	}

    public IEnumerator LoadYourAsyncScene(int index)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

		while (!asyncLoad.isDone)
        {
			//float progress = Mathf.Clamp01(asyncLoad.progress / .9f);
			//UIReferences.Instance.progressBar.currentPercent = asyncLoad.progress;
			//Debug.Log(asyncLoad.progress);
			//UIReferences.Instance.progressBar.textPercent.text = progress * 100f + "%";

			yield return null;
		}
    }

	void LoadScene(int index)
	{
		SceneManager.LoadScene(index);
	}
}