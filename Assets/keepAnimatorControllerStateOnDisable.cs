﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keepAnimatorControllerStateOnDisable : MonoBehaviour
{
    [SerializeField] Animator btnOk;
    [SerializeField] Animator btnWarning;
    [SerializeField] Animator btnFault;
    [SerializeField] Animator btnAlarm;
    [SerializeField] Animator btnSC;
    [SerializeField] Animator btnRtve;
    [SerializeField] Animator btnCellnex;
    [SerializeField] Animator btnOm;
    [SerializeField] Animator btnFm;

    

    // Start is called before the first frame update
    void Start()
    {
        List<Animator> animList = new List<Animator> { btnOk, btnWarning, btnFault, btnAlarm, btnSC, btnRtve, btnCellnex, btnOm, btnFm };

        foreach (var anim in animList)
        {
            anim.keepAnimatorControllerStateOnDisable = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
