﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContentFiller : MonoBehaviour
{
    [SerializeField] GameObject frecuencias;
    [SerializeField] GameObject rssi;
    // Start is called before the first frame update
    void Start()
    {
        float rssi = -69f;

        GameObject rssi_flecha = this.rssi.transform.Find("Grafica_RSSI").Find("bg").Find("FLECHA").gameObject;
        GameObject rssi_dato = this.rssi.transform.Find("Grafica_RSSI").Find("bg").Find("RSSI_DATO").gameObject;

        rssi_dato.GetComponent<TextMeshProUGUI>().text = rssi.ToString();

        float scaledRssi = InterpolateValue(rssi, -180, 180, 90, -90);

        rssi_flecha.transform.rotation = Quaternion.Euler(0, 0, scaledRssi);

        float fr = 94.7f;

        GameObject fr1 = frecuencias.transform.Find("FR1").gameObject;
        GameObject fr1_text = fr1.transform.Find("FRECUENCIA").gameObject;

        fr1_text.GetComponent<TextMeshProUGUI>().text = fr.ToString();

        float scaledFR1 = InterpolateValue(fr, 85, 110, -283, 279);

        fr1.transform.localPosition = new Vector3(scaledFR1, fr1.transform.localPosition.y, fr1.transform.localPosition.z);
    }

    private float InterpolateValue(float value, int minValue, int maxValue, float minDim, float maxDim)
    {
        return Mathf.Lerp(minDim, maxDim, (value - minValue) / (maxValue - minValue));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
