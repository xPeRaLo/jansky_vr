﻿using UnityEngine;

namespace Crosstales.Radio.RhythmVisualizator
{
   /// <summary>Shows the details for Rhythm Visualizator.</summary>
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_rhythm_visualizator_1_1_show_more.html")]
   public class ShowMore : MonoBehaviour
   {
      #region Public methods

      public void Show()
      {
         Util.Helper.OpenURL(Util.Constants.ASSET_3P_RHYTHM_VISUALIZATOR);
      }

      #endregion
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)