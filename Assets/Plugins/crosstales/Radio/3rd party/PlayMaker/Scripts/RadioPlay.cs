﻿#if PLAYMAKER
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
   /// <summary>Play-action for PlayMaker.</summary>
   [ActionCategory("Crosstales.Radio")]
   [HelpUrl("https://www.crosstales.com/media/data/assets/radio/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_radio_play.html")]
   public class RadioPlay : BaseRadioAction
   {
      /// <summary>Name of the radio station.</summary>
      [Header("Station Settings")] [Tooltip("Name of the radio station.")] public FsmString RadioName = string.Empty;

      /// <summary>Streaming-URL of the station.</summary>
      [Tooltip("Streaming-URL of the station.")] [RequiredField] public FsmString Url = string.Empty;

      /// <summary>Name of the station.</summary>
      [Tooltip("Name of the station.")] public FsmString Station = string.Empty;


      /// <summary>Bitrate in kbit/s (default: 128).</summary>
      [Header("Stream Settings")] [Tooltip("Bitrate in kbit/s (default: 128).")] public FsmInt Bitrate = Crosstales.Radio.Util.Config.DEFAULT_BITRATE;

      /// <summary>Size of the streaming-chunk in KB (default: 32).</summary>
      [Tooltip("Size of the streaming-chunk in KB (default: 32).")] [RequiredField] public FsmInt ChunkSize = Crosstales.Radio.Util.Config.DEFAULT_CHUNKSIZE;

      /// <summary>Size of the local buffer in KB (default: 48).</summary>
      [Tooltip("Size of the local buffer in KB (default: 48).")] [RequiredField] public FsmInt BufferSize = Crosstales.Radio.Util.Config.DEFAULT_BUFFERSIZE;


      /// <summary>Size of cache stream in KB (default: 1024).</summary>
      [Header("Memory Settings")] [Tooltip("Size of cache stream in KB (default: 1024, max: 16384).")] [RequiredField] public FsmInt CacheStreamSize = Crosstales.Radio.Util.Config.DEFAULT_CACHESTREAMSIZE;

      /// <summary>Add a RadioPlayer (default: random player in scene).</summary>
      [Tooltip("Add a RadioPlayer (default: random player in scene).")] [RequiredField] public Crosstales.Radio.RadioPlayer RadioPlayer;

      public override void OnEnter()
      {
         if (RadioPlayer == null)
         {
            RadioPlayer = GameObject.Find("RadioPlayer").GetComponent<Crosstales.Radio.RadioPlayer>();
         }

         if (RadioPlayer != null)
         {
            if (string.IsNullOrEmpty(RadioName.Value))
            {
               RadioName.Value = RadioPlayer.Station.Name;
            }
            else
            {
               RadioPlayer.Station.Name = RadioName.Value;
            }

            if (string.IsNullOrEmpty(Url.Value))
            {
               Url.Value = RadioPlayer.Station.Url;
            }
            else
            {
               RadioPlayer.Station.Url = Url.Value;
            }

            if (string.IsNullOrEmpty(Station.Value))
            {
               Station.Value = RadioPlayer.Station.Station;
            }
            else
            {
               RadioPlayer.Station.Station = Station.Value;
            }

            if (Bitrate.Value != 0)
               RadioPlayer.Station.Bitrate = Bitrate.Value;

            if (ChunkSize.Value != 0)
               RadioPlayer.Station.ChunkSize = ChunkSize.Value;

            if (BufferSize.Value != 0)
               RadioPlayer.Station.BufferSize = BufferSize.Value;

            if (CacheStreamSize.Value != 0)
               RadioPlayer.CacheStreamSize = CacheStreamSize.Value;

            RadioPlayer.Play();
         }
         else
         {
            Debug.LogError("'RadioPlayer' is null! Please add a valid player object!");
         }

         Fsm.Event(sendEvent);

         Finish();
      }
   }
}
#endif
// © 2016-2021 crosstales LLC (https://www.crosstales.com)