﻿#if PLAYMAKER
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
   /// <summary>PlayNext-action for PlayMaker.</summary>
   [ActionCategory("Crosstales.Radio")]
   [HelpUrl("https://www.crosstales.com/media/data/assets/radio/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_radio_manager_play_next.html")]
   public class RadioManagerPlayNext : BaseRadioAction
   {
      /// <summary>Add a RadioManager (default: first object in scene).</summary>
      [Tooltip("Add a RadioManager (default: first object in scene).")] [RequiredField] public Crosstales.Radio.RadioManager RadioManager;

      /// <summary>Play next radio station in random order (default: false).</summary>
      [Tooltip("Play next radio station in random order (default: false).")] [RequiredField] public FsmBool PlayRandom = false;

      public override void OnEnter()
      {
         if (RadioManager == null)
         {
            RadioManager = GameObject.Find("RadioManager").GetComponent<Crosstales.Radio.RadioManager>();
         }

         if (RadioManager != null)
         {
            RadioManager.Next(PlayRandom.Value);
         }

         Fsm.Event(sendEvent);

         Finish();
      }
   }
}
#endif
// © 2016-2021 crosstales LLC (https://www.crosstales.com)