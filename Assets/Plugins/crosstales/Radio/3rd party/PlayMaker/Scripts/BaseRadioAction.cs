﻿#if PLAYMAKER
namespace HutongGames.PlayMaker.Actions
{
   /// <summary>Base class for Radio-actions in PlayMaker.</summary>
   public abstract class BaseRadioAction : FsmStateAction
   {
      #region Variables

      public FsmEvent sendEvent;

      #endregion
   }
}
#endif
// © 2017-2021 crosstales LLC (https://www.crosstales.com)