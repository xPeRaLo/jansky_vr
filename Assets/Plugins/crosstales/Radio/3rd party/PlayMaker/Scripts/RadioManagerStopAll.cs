﻿#if PLAYMAKER
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
   /// <summary>StopAll-action for PlayMaker.</summary>
   [ActionCategory("Crosstales.Radio")]
   [HelpUrl("https://www.crosstales.com/media/data/assets/radio/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_radio_manager_stop_all.html")]
   public class RadioManagerStopAll : BaseRadioAction
   {
      /// <summary>Add a RadioManager (default: random manager in scene).</summary>
      [Tooltip("Add a RadioManager (default: random manager in scene).")] [RequiredField] public Crosstales.Radio.RadioManager RadioManager;

      public override void OnEnter()
      {
         if (RadioManager == null)
         {
            RadioManager = GameObject.Find("RadioManager").GetComponent<Crosstales.Radio.RadioManager>();
         }
      }

      public override void OnUpdate()
      {
         if (RadioManager != null && RadioManager.isPlayback)
         {
            RadioManager.StopAll();
         }
         else
         {
            Fsm.Event(sendEvent);

            Finish();
         }
      }
   }
}
#endif
// © 2016-2021 crosstales LLC (https://www.crosstales.com)