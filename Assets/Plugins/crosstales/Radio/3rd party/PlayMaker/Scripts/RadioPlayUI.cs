﻿#if PLAYMAKER
using UnityEngine;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
   /// <summary>PlayUI-action for PlayMaker.</summary>
   [ActionCategory("Crosstales.Radio")]
   [HelpUrl("https://www.crosstales.com/media/data/assets/radio/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_radio_play_u_i.html")]
   public class RadioPlayUI : BaseRadioAction
   {
      /// <summary>Name of the radio station.</summary>
      [Header("Station Settings")] [Tooltip("Name of the radio station.")] public InputField RadioName;

      /// <summary>Streaming-URL of the station.</summary>
      [Tooltip("Streaming-URL of the station.")] [RequiredField] public InputField Url;

      /// <summary>Name of the station.</summary>
      [Tooltip("Name of the station.")] public InputField Station;

      /// <summary>Add a RadioPlayer (default: random player in scene).</summary>
      [Tooltip("Add a RadioPlayer (default: random player in scene).")] [RequiredField] public Crosstales.Radio.RadioPlayer RadioPlayer;

      private bool started;

      public override void OnEnter()
      {
         if (RadioPlayer == null)
         {
            RadioPlayer = GameObject.Find("RadioPlayer").GetComponent<Crosstales.Radio.RadioPlayer>();
         }

         if (RadioPlayer != null)
         {
            started = false;

            if (string.IsNullOrEmpty(RadioName.text))
            {
               RadioName.text = RadioPlayer.Station.Name;
            }
            else
            {
               RadioPlayer.Station.Name = RadioName.text;
            }

            if (string.IsNullOrEmpty(Url.text))
            {
               Url.text = RadioPlayer.Station.Url;
            }
            else
            {
               RadioPlayer.Station.Url = Url.text;
            }

            if (string.IsNullOrEmpty(Station.text))
            {
               Station.text = RadioPlayer.Station.Station;
            }
            else
            {
               RadioPlayer.Station.Station = Station.text;
            }
         }
         else
         {
            Debug.LogWarning("'RadioPlayer' is null! Please add a valid player object!");
         }
      }

      public override void OnUpdate()
      {
         if (RadioPlayer != null && !started)
         {
            RadioPlayer.Play();
            started = true;
         }
         else
         {
            Fsm.Event(sendEvent);

            Finish();
         }
      }
   }
}
#endif
// © 2016-2021 crosstales LLC (https://www.crosstales.com)