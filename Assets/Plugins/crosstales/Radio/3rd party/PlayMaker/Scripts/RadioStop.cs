﻿#if PLAYMAKER
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
   /// <summary>Stop-action for PlayMaker.</summary>
   [ActionCategory("Crosstales.Radio")]
   [HelpUrl("https://www.crosstales.com/media/data/assets/radio/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_radio_stop.html")]
   public class RadioStop : BaseRadioAction
   {
      /// <summary>Add a RadioPlayer (default: random player in scene).</summary>
      [Tooltip("Add a RadioPlayer (default: random player in scene).")] [RequiredField] public Crosstales.Radio.RadioPlayer RadioPlayer;

      public override void OnEnter()
      {
         if (RadioPlayer == null)
         {
            RadioPlayer = GameObject.Find("RadioPlayer").GetComponent<Crosstales.Radio.RadioPlayer>();
         }
      }

      public override void OnUpdate()
      {
         if (RadioPlayer != null && RadioPlayer.isPlayback /*!stopped*/)
         {
            RadioPlayer.Stop();
         }
         else
         {
            Fsm.Event(sendEvent);

            Finish();
         }
      }
   }
}
#endif
// © 2016-2021 crosstales LLC (https://www.crosstales.com)